<?php


namespace Baldwin\MedipimConnector\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Medipim\Api\V3\MedipimApiV3Error;

/**
 * Class ConnectorTest
 * @package Baldwin\MedipimConnector\Test\Unit\Model
 */
class ConnectorTest extends TestCase
{

    protected $medipimConnector;

    /**
     * Scope config mock
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfigMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManagerHelper;

    protected function setUp()
    {
        $this->objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
    }

    public function testConnectorWithoutCredentials()
    {
        $className = \Baldwin\MedipimConnector\Model\Connector::class;

        $arguments = $this->objectManagerHelper->getConstructArguments($className);

        $scopeConfig = $arguments['configInterface'];

        $this->scopeConfigMock = $scopeConfig;

        $this->expectException(MedipimApiV3Error::class);

        $this->medipimConnector = $this->objectManagerHelper->getObject($className, $arguments);

        $this->scopeConfigMock->expects($this->once())
            ->method('getApiClient')
            ->willReturn(null);

        $this->scopeConfigMock->expects($this->once())
            ->method('getApiSecret')
            ->willReturn(null);

        $this->medipimConnector->connect();
    }

    public function testConnectorWithCredentials()
    {
        $className = \Baldwin\MedipimConnector\Model\Connector::class;

        $arguments = $this->objectManagerHelper->getConstructArguments($className);

        $scopeConfig = $arguments['configInterface'];

        $this->scopeConfigMock = $scopeConfig;

        $this->medipimConnector = $this->objectManagerHelper->getObject($className, $arguments);

        $this->scopeConfigMock->expects($this->once())
            ->method('getApiClient')
            ->willReturn("dumm-client");

        $this->scopeConfigMock->expects($this->once())
            ->method('getApiSecret')
            ->willReturn('dummysecret');

        $this->medipimConnector->connect();
    }
}
