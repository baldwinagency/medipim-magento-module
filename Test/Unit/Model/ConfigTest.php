<?php


namespace Baldwin\MedipimConnector\Test\Unit\Model;

use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 * @package Baldwin\MedipimConnector\Test\Unit\Model
 */
class ConfigTest extends TestCase
{

    /**
     * @var Medipim Config mock
     */
    protected $medipimConfigMock;

    /**
     * Scope config mock
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfigMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManagerHelper;

    protected function setUp()
    {
        $this->objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $className = \Baldwin\MedipimConnector\Model\Config::class;
        $arguments = $this->objectManagerHelper->getConstructArguments($className);

        $scopeConfig = $arguments['scopeConfig'];

        $this->scopeConfigMock = $scopeConfig;
        $this->medipimConfigMock = $this->objectManagerHelper->getObject($className, $arguments);
    }

    public function testIsStaging()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn('staging');

        $this->assertEquals("staging", $this->medipimConfigMock->getEnvironment());
    }

    public function testIsProduction()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn('production');

        $this->assertEquals("production", $this->medipimConfigMock->getEnvironment());
    }
}
