<?php


namespace Baldwin\MedipimConnector\Test\Unit\Model;

use PHPUnit\Framework\TestCase;

/**
 * Class OneTest
 * @package Baldwin\MedipimConnector\Test\Unit\Model
 */
class OneTest extends TestCase
{
    public function testOneIsOne()
    {
        $this->assertEquals("1", "1");
    }
}
