<?php


namespace Baldwin\MedipimConnector\Test\Unit\Helper\Import;

use PHPUnit\Framework\TestCase;

/**
 * Class AttributeTest
 * @package Baldwin\MedipimConnector\Test\Unit\Helper\Import
 */
class AttributeTest extends TestCase
{
    /**
     * @var Helper mock
     */
    protected $helperMock;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManagerHelper;

    protected function setUp()
    {
        $this->objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $className = \Baldwin\MedipimConnector\Helper\Import\Attribute::class;
        $arguments = $this->objectManagerHelper->getConstructArguments($className);
        $this->helperMock = $this->objectManagerHelper->getObject($className, $arguments);
    }

    public function testStringToCamelCase()
    {
        $testCamelCase = "thisIsCamelCase";

        $result = $this->helperMock->camelCaseToString($testCamelCase);

        $this->assertEquals($result, "this Is Camel Case");
    }

    public function testCamelCaseToString()
    {
        $testCamelCase = "This Is Camel Case";

        $result = $this->helperMock->stringToCamelCase($testCamelCase);

        $this->assertEquals($result, "thisIsCamelCase");
    }

    public function testIntToFloat()
    {
        $price = 1255;

        $resulted = $this->helperMock->intPriceToFloat($price);

        $this->assertEquals($resulted, 12.55);
    }
}
