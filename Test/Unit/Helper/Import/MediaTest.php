<?php


namespace Baldwin\MedipimConnector\Test\Unit\Helper\Import;

use Baldwin\MedipimConnector\Helper\Import\Media;
use Magento\Framework\Filesystem\File\ReadInterface;
use PHPUnit\Framework\TestCase;
use \Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Zend\Filter\Dir;

/**
 * Class MediaTest
 * @package Baldwin\MedipimConnector\Test\Unit\Helper\Import
 */
class MediaTest extends TestCase
{
    /**
     * @var Media mock
     */
    protected $helperMock;

    /**
     * @var Filesystem $mock;
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $objectManagerHelper;

    protected $mediaDirectory;

    protected function setUp()
    {
        $this->objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);

        $className = \Baldwin\MedipimConnector\Helper\Import\Media::class;
        $arguments = $this->objectManagerHelper->getConstructArguments($className);
        $this->helperMock = $this->objectManagerHelper->getObject($className, $arguments);

        $this->filesystem = $this->getMockBuilder(Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->baseDirectory = $this->getMockBuilder(ReadInterface::class)
            ->getMockForAbstractClass();

        $path = '/var/www/html/magento2ce/pub/media/';

        $this->mediaDirectory = $this->getMockBuilder(\Magento\Framework\Filesystem\Directory\Write::class)
            ->disableOriginalConstructor()
            ->setMethods(['create', 'isFile', 'isExist', 'getAbsolutePath'])
            ->getMock();

        $this->filesystem->expects($this->once())
            ->method('getDirectoryRead')
            ->willReturn($this->mediaDirectory);

    }

    public function testDownloadImage()
    {
        $remotePath = 'https://www.baldwin.be/app/themes/baldwinv4/dist/images/baldwin_logo_new_2x.png';

        $localPath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();

        $localPath .= DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . "test.png";


        $result = $this->helperMock->downloadImage($remotePath, $localPath);

        $this->assertFileExists($result);
    }

    public function testImageFileNameOfUrl()
    {
        $remotePath = 'https://www.baldwin.be/app/themes/baldwinv4/dist/images/baldwin_logo_new_2x.png';

        $name = $this->helperMock->getImageFileNameOfUrl($remotePath);

        $this->assertEquals("baldwin_logo_new_2x.jpg", $name);
    }
}
