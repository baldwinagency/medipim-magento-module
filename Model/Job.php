<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Magento\Framework\Model\AbstractModel;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;

/**
 * Class Job
 * @package Baldwin\MedipimConnector\Model
 */
class Job extends AbstractModel implements JobInterface
{
    const CACHE_TAG = 'medi_job';

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Job::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getProfileId()
    {
        return $this->getData(self::PROFILE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledAt()
    {
        return $this->getData(self::SCHEDULED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function getExecutedAt()
    {
        return $this->getData(self::EXECUTED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function getFinishedAt()
    {
        return $this->getData(self::FINISHED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsPublished(): bool
    {
        return $this->getData(self::IS_PUBLISHED);
    }

    /**
     * {@inheritdoc}
     */
    public function setProfileId($profileId)
    {
        return $this->setData(self::PROFILE_ID, $profileId);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * {@inheritdoc}
     */
    public function setScheduledAt($scheduledAt)
    {
        return $this->setData(self::SCHEDULED_AT, $scheduledAt);
    }

    /**
     * {@inheritdoc}
     */
    public function setExecutedAt($executedAt)
    {
        return $this->setData(self::EXECUTED_AT, $executedAt);
    }

    /**
     * {@inheritdoc}
     */
    public function setFinishedAt($finishedAt)
    {
        return $this->setData(self::FINISHED_AT, $finishedAt);
    }

    /**
     * {@inheritdoc}
     */
    public function setIsPublished(bool $published)
    {
        return $this->setData(self::IS_PUBLISHED, $published);
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_SYNCING => __('Syncing'),
            self::STATUS_READY_FOR_IMPORT => __('Ready for Import'),
            self::STATUS_NOTHING_TO_IMPORT => __('Nothing to Import'),
            self::STATUS_IMPORTING => __('Importing'),
            self::STATUS_ERROR => __('Error'),
            self::STATUS_SUCCESS => __('Success'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getBatchGroupId()
    {
        return $this->getData(self::BATCH_GROUP_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setBatchGroupId($batchGroupId)
    {
        return $this->setData(self::BATCH_GROUP_ID, $batchGroupId);
    }
}
