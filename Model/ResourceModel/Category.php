<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Catalog\Api\Data\CategoryInterface as MagentoCategoryInterface;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class Category
 * @package Baldwin\MedipimConnector\Model\ResourceModel
 */
class Category extends AbstractDb
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    private $metadataPool;

    private $cachedMagentoCategoryIds;

    public function __construct(
        Context $context,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        $this->entityManager = $entityManager;
        $this->metadataPool  = $metadataPool;
        $this->cachedMagentoCategoryIds = [];
        parent::__construct($context, $connectionName);
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('medipim_category_entity', 'entity_id');
    }

    /**
     * Save an object.
     *
     * @param AbstractModel $object
     * @return $this
     * @throws \Exception
     */
    public function save(AbstractModel $object)
    {
        $this->entityManager->save($object);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function delete(AbstractModel $object)
    {
        $this->entityManager->delete($object);
        return $this;
    }

    /**
     * @param $categoryId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function lookupMagentoCategoryIds($categoryId)
    {
        if (!array_key_exists($categoryId, $this->cachedMagentoCategoryIds)) {

            $connection = $this->getConnection();
            $tableName = 'medipim_category_entity';

            $select = $connection->select()
                ->from($tableName)
                ->where("medipim_id = ?", $categoryId);

            $data = $connection->fetchAll($select);
            $data = reset($data);
            $this->cachedMagentoCategoryIds[$categoryId] = [$data['catalog_category_entity_id']];
        }

        return $this->cachedMagentoCategoryIds[$categoryId];
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function lookupChildren($categoryId)
    {
        $connection = $this->getConnection();
        $tableName = 'medipim_category_entity';

        $sql = $connection->select()
            ->from($tableName)
            ->where("medipim_parent_id = ?", $categoryId);

        return $connection->fetchAll($sql);
    }
}
