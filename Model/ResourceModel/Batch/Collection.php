<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Batch;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Class Collection
 * @package Baldwin\MedipimConnector\Model\ResourceModel\Batch
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\Batch::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\Batch::class
        );
    }
}
