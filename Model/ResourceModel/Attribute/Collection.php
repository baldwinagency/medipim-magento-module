<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Baldwin\MedipimConnector\Model\ResourceModel\Attribute;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * CMS page collection
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\Attribute::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\Attribute::class
        );
    }
}
