<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Import Profile collection
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'profile_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\ImportProfile::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile::class
        );
    }
}
