<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Category;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Category collection
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\Category::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\Category::class
        );
    }
}
