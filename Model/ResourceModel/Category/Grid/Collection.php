<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Category\Grid;

use Magento\Customer\Ui\Component\DataProvider\Document;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Baldwin\MedipimConnector\Model\ResourceModel\Category as ResourceCategory;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Catalog\Model\Category;
use Magento\Eav\Api\AttributeRepositoryInterface as EavAttributeRepositoryInterface;

class Collection extends SearchResult
{
    /**
     * @inheritdoc
     */
    protected $document = Document::class;

    /**
     * @inheritdoc
     */
    protected $_map = ['fields' => ['entity_id' => 'main_table.entity_id']];

    private $categoryEntityType;

    private $eavAttributeRepository;
    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'catalog_category_entity',
        $resourceModel = ResourceCategory::class,
        EavConfig $eavConfig,
        EavAttributeRepositoryInterface $attributeRepository
    ) {
        $this->categoryEntityType = $eavConfig->getEntityType(Category::ENTITY);
        $this->eavAttributeRepository = $attributeRepository;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    /**
     * adding email to customer name column
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()
            ->where('entity_id != ?', '1')
            ->where('entity_id != ?', '2');

        return $this;
    }
}
