<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Job\Grid;

use Magento\Customer\Ui\Component\DataProvider\Document;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Baldwin\MedipimConnector\Model\ResourceModel\Job;
class Collection extends SearchResult
{
    /**
     * @inheritdoc
     */
    protected $document = Document::class;

    /**
     * @inheritdoc
     */
    protected $_map = ['fields' => ['entity_id' => 'main_table.id']];

    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'medipim_import_jobs',
        $resourceModel = Job::class
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }


    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()
            ->joinLeft(
                ['mp' => $this->getTable('medipim_import_profile')],
                'main_table.profile_id = mp.profile_id',
                ['identifier']
            )->joinLeft(
                ['mb' => $this->getTable('medipim_sync_batches')],
                'main_table.batch_group_id = mb.batch_group_id',
                [
                    'batches' => 'COUNT(mb.id)',
                    'entities' => 'IFNULL(SUM(mb.entities), 0)'
                ]
            )->group('main_table.batch_group_id');

        $this->getSelect()->columns('mp.identifier as profile');


        return $this;
    }
}
