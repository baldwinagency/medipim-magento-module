<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Job;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;

/**
 * Job collection
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\Job::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\Job::class
        );
    }
}
