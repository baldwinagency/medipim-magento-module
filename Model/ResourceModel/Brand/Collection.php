<?php

namespace Baldwin\MedipimConnector\Model\ResourceModel\Brand;

use Magento\Catalog\Model\ResourceModel\AbstractCollection;

/**
 * Supplier collection
 */
class Collection extends AbstractCollection
{
    const MAIN_TABLE_ALIAS = 'e';

    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Baldwin\MedipimConnector\Model\Brand::class,
            \Baldwin\MedipimConnector\Model\ResourceModel\Brand::class
        );
    }
}
