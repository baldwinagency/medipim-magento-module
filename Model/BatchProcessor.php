<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\BatchProcessorInterface;
use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Model\Import\Product\ImporterFactory;
use Magento\Framework\MessageQueue\PublisherInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\Batch\CollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class BatchProcessor implements BatchProcessorInterface
{
    private $importerFactory;
    private $publisher;
    private $collectionFactory;
    private $importProfileRepository;
    private $dateTime;

    const TOPIC_NAME = 'medipim.batch.import';

    public function __construct(
        ImporterFactory $importerFactory,
        PublisherInterface $publisher,
        CollectionFactory $collectionFactory,
        DateTime $dateTime
    )
    {
        $this->importerFactory         = $importerFactory;
        $this->publisher               = $publisher;
        $this->collectionFactory       = $collectionFactory;
        $this->dateTime                = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function publishAllBatches($status = [Batch::SYNC_STATUS_PENDING], JobInterface $job)
    {
        $importer = $this->importerFactory->create();
        $batches = $this->collectionFactory->create()
            ->addFieldToFilter("batch_group_id", ['eq' => $job->getBatchGroupId()]);

        if ($batches->getSize() > 0) {
            foreach ($batches as $batch) {
                $this->publishBatch($batch->getId());
            }
        }
    }
    
    public function publishBatch(string $batchId): bool
    {
        $this->publisher->publish(self::TOPIC_NAME, $batchId);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function executeSingleBatch(string $batchId): bool
    {
        if ($batchId !== null) {
            $importer = $this->importerFactory->create();
            return $importer->singleBatchImport($batchId);
        }
        return false;
    }
}
