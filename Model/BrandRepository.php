<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\BrandRepositoryInterface;
use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Model\ResourceModel\Brand as ResourceBrand;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Baldwin\MedipimConnector\Model\BrandFactory;

/**
 * Class CategoryRepository
 * @package Baldwin\MedipimConnector\Model
 */
class BrandRepository implements BrandRepositoryInterface
{
    /**
     * @var ResourceBrand
     */
    protected $resource;

    /**
     * @var BrandFactory
     */
    protected $brandFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    public function __construct(
        ResourceBrand $resource,
        BrandFactory $brandFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource                   = $resource;
        $this->brandFactory               = $brandFactory;
        $this->collectionProcessor        = $collectionProcessor;
    }

    /**
     * @param Data\BrandInterface $brand
     * @return Data\BrandInterface
     * @throws CouldNotSaveException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\BrandInterface $brand)
    {
        try {
            $this->resource->save($brand);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the brand: %1', $exception->getMessage()),
                $exception
            );
        }
        return $brand;
    }

    /**
     * @param int $id
     * @return Data\BrandInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $brand = $this->brandFactory->create();
        $brand->load($id);
        if (!$brand->getId()) {
            throw new NoSuchEntityException(__('The brand with the "%1" ID doesn\'t exist.', $id));
        }
        return $brand;
    }
}
