<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Api\AttributeRepositoryInterface;
use Baldwin\MedipimConnector\Model\AttributeFactory;
use Baldwin\MedipimConnector\Model\ResourceModel\Attribute as ResourceAttribute;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Baldwin\MedipimConnector\Model\ResourceModel\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * Class AttributeRepository
 * @package Baldwin\MedipimConnector\Model
 */
class AttributeRepository implements AttributeRepositoryInterface
{
    /**
     * @var ResourceAttribute
     */
    protected $resource;

    /**
     * @var \Baldwin\MedipimConnector\Model\AttributeFactory
     */
    protected $attributeFactory;

    /**
     * @var AttributeCollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var Data\AttributeSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * AttributeRepository constructor.
     * @param ResourceAttribute $resource
     * @param \Baldwin\MedipimConnector\Model\AttributeFactory $attributeFactory
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\AttributeSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ResourceAttribute $resource,
        AttributeFactory $attributeFactory,
        AttributeCollectionFactory $attributeCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\AttributeSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource                   = $resource;
        $this->attributeFactory           = $attributeFactory;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->collectionProcessor        = $collectionProcessor;
        $this->searchResultsFactory       = $searchResultsFactory;
    }

    /**
     * Save Attribute.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\AttributeInterface $attribute
     * @return Attribute
     * @throws CouldNotSaveException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\AttributeInterface $attribute)
    {
        try {
            $this->resource->save($attribute);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the attribute: %1', $exception->getMessage()),
                $exception
            );
        }
        return $attribute;
    }

    /**
     * Retrieve Attribute.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $attribute = $this->attributeFactory->create();
        $attribute->load($id);
        if (!$attribute->getId()) {
            throw new NoSuchEntityException(__('The medipim attribute with the "%1" ID doesn\'t exist.', $id));
        }
        return $attribute;
    }

    /**
     * Retrieve Attribute.
     *
     * @param int $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByMedipimId($medipimId)
    {
        $attribute = $this->attributeFactory->create()
            ->getCollection()
            ->addFieldToFilter('medipim_id', $medipimId)
            ->getFirstItem();

        if (!$attribute->getId()) {
            throw new NoSuchEntityException(__('The medipim attribute with the "%1" Medipim ID doesn\'t exist.', $medipimId));
        }
        return $attribute;
    }

    /**
     * Retrieve Attribute.
     *
     * @param int $eavAttribute
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByEavAttributeId($eavAttributeId)
    {
        $attribute = $this->attributeFactory->create()
            ->getCollection()
            ->addFieldToFilter('attribute_id', $eavAttributeId)
            ->getFirstItem();

        if (!$attribute->getId()) {
            throw new NoSuchEntityException(__('The medipim attribute with the "%1" EAV Attribute ID doesn\'t exist.', $eavAttributeId));
        }
        return $attribute;
    }

    /**
     * Load Attribute data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Baldwin\MedipimConnector\Api\Data\PageSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->attributeCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\AttributeSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
