<?php

namespace Baldwin\MedipimConnector\Model\ImportProfile;

use Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->prepareData($this->loadedData);
        }
        $items = $this->collection->getItems();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $block) {
            $this->loadedData[$block->getId()] = $this->prepareData($block->getData());
        }

        $data = $this->dataPersistor->get('medipim_connector_importprofile');
        $data = $this->prepareData($data);
        if (!empty($data)) {
            $block = $this->collection->getNewEmptyItem();
            $block->setData($data);
            $this->loadedData[$block->getId()] = $block->getData();

            $this->dataPersistor->clear('medipim_connector_importprofile');
        }

        return $this->loadedData;
    }

    private function prepareData($data) {
        if (isset($data['brand_ids']) && is_string($data['brand_ids'])) {
            $brandIds = $data['brand_ids'];
            $brandIds = explode(",", $brandIds);

            $data["brand_ids"] = $brandIds;
        }

        if (isset($data['supplier_ids']) && is_string($data['supplier_ids'])) {
            $supplierIds = $data['supplier_ids'];
            $supplierIds = explode(",", $supplierIds);


            $data["supplier_ids"] = $supplierIds;
        }

        if (isset($data['category_ids']) && is_string($data['category_ids'])) {
            $categoryIds = $data['category_ids'];
            $categoryIds = explode(",", $categoryIds);


            $data["category_ids"] = $categoryIds;
        }

        return $data;
    }
}
