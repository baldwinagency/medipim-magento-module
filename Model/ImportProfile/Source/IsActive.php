<?php

namespace Baldwin\MedipimConnector\Model\ImportProfile\Source;

use Magento\Framework\Data\OptionSourceInterface;

use Baldwin\MedipimConnector\Model\ImportProfile;
/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var ImportProfile
     */
    protected $importProfile;

    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(ImportProfile $importProfile)
    {
        $this->importProfile = $importProfile;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->importProfile->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
