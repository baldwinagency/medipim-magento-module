<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data\SupplierInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Brand
 * @package Baldwin\MedipimConnector\Model
 */
class Supplier extends AbstractModel implements SupplierInterface
{
    const CACHE_TAG = 'medi_supplier';

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Supplier::class);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::SUPPLIER_NAME);
    }

    /**
     * @param $name
     * @return \Baldwin\MedipimConnector\Api\Data\SupplierInterface
     */
    public function setName($name)
    {
        return $this->setData(self::SUPPLIER_NAME, $name);
    }
}
