<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\BatchProcessorInterface;
use Baldwin\MedipimConnector\Api\BatchRepositoryInterface;
use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Model\ResourceModel\Batch as ResourceBatch;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Baldwin\MedipimConnector\Model\BatchFactory;
use Baldwin\MedipimConnector\Model\ResourceModel\Batch\CollectionFactory as BatchCollectionFactory;

/**
 * Class CategoryRepository
 * @package Baldwin\MedipimConnector\Model
 */
class BatchRepository implements BatchRepositoryInterface
{
    /**
     * @var ResourceBatch
     */
    protected $resource;

    /**
     * @var BatchFactory
     */
    protected $batchFactory;

    /**
     * @var BatchCollectionFactory
     */
    protected $batchCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    protected $batchProcessor;

    /**
     * BatchRepository constructor.
     * @param ResourceBatch $resource
     * @param BatchFactory $batchFactory
     * @param BatchCollectionFactory $batchCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceBatch $resource,
        BatchFactory $batchFactory,
        BatchCollectionFactory $batchCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        BatchProcessorInterface $batchProcessor
    ) {
        $this->resource                   = $resource;
        $this->batchCollectionFactory     = $batchCollectionFactory;
        $this->batchFactory               = $batchFactory;
        $this->collectionProcessor        = $collectionProcessor;
        $this->batchProcessor             = $batchProcessor;
    }

    /**
     * @param Data\BatchInterface $batch
     * @return Data\BatchInterface
     * @throws CouldNotSaveException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\BatchInterface $batch)
    {
        try {
            $this->resource->save($batch);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the batch: %1', $exception->getMessage()),
                $exception
            );
        }
        return $batch;
    }

    /**
     * @param int $id
     * @return Data\BatchInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $batch = $this->batchFactory->create();
        $batch->load($id);
        if (!$batch->getId()) {
            throw new NoSuchEntityException(__('The category with the "%1" ID doesn\'t exist.', $id));
        }
        return $batch;
    }

    public function getByBatchGroupId($batchGroupId)
    {
        $batches = $this->batchCollectionFactory->create()
            ->addAttributeToSelect("*")
            ->addFieldToFilter(Batch::BATCH_GROUP_ID, $batchGroupId);

        return $batches;
    }

    /**
     * @param string $status
     * @return Data\BatchInterface|ResourceBatch\Collection
     */
    public function getByStatus($status)
    {
        $batches = $this->batchCollectionFactory->create()
            ->addAttributeToSelect("*")
            ->addFieldToFilter(Batch::SYNC_STATUS, $status);

        return $batches;
    }

    public function deleteByBatchGroupId(string $batchGroupId)
    {
        $batches = $this->getByBatchGroupId($batchGroupId);
        foreach ($batches as $batch) {
            $this->delete($batch);
        }
    }

    public function delete(Data\BatchInterface $batch)
    {
        try {
            $this->resource->delete($batch);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not delete the job: %1', $exception->getMessage()),
                $exception
            );
        }
        return $batch;
    }

    public function reschedule(Data\BatchInterface $batch)
    {
        $batch->setStatus(Batch::SYNC_STATUS_PENDING);
        $batch->clearSyncTimestamps();

        $this->save($batch);

        $this->batchProcessor->publishBatch($batch->getId());
    }
}
