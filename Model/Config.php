<?php

namespace Baldwin\MedipimConnector\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class Config
 * @package Baldwin\MedipimConnector\Model
 */
class Config implements ConfigInterface
{

    /** Configuration path for api client */
    const XML_PATH_API_CLIENT = 'medipim/api/api_client';

    /** Configuration path for api secret */
    const XML_PATH_API_SECRET = 'medipim/api/api_secret';

    /** Configuration path for api environment */
    const XML_PATH_API_ENVIRONMENT = 'medipim/api/api_environment';

    /** Configuration path to specify to product import batch size */
    const XML_PATH_PRODUCT_IMPORT_BATCH_SIZE = 'medipim/product_import/batch_size';

    /** Configuration path to specify number of failed batchtes within one import */
    const XML_PATH_PRODUCT_IMPORT_MAX_BATCH_FAILS = 'medipim/product_import/max_batch_fails';

    /** Configuration path to specify number of failed batchtes within one import */
    const XML_PATH_PRODUCT_IMPORT_MAX_BATCH_EXECUTED = 'medipim/product_import/max_batch_executed';

    /** Configuration path to specify whether images should be imported or not */
    const XML_PATH_PRODUCT_IMPORT_IMPORT_IMAGES = 'medipim/product_import/import_images';

    /** Configuration path to specify whether to use hard image import or Cdn */
    const XML_PATH_PRODUCT_IMPORT_IMPORT_IMAGE_TYPE = 'medipim/product_import/import_image_type';

    /** Configuration path to specif what size / type should be used for the frontals */
    const XML_PATH_PRODUCT_IMPORT_FRONTAL_IMAGE_TYPE = 'medipim/product_import/frontal_image_type';

    const XML_PATH_PRODUCT_IMPORT_PRODUCT_LIST_IMAGE_TYPE = 'medipim/product_import/product_list_image_type';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor
    ) {
        $this->scopeConfig  = $scopeConfig;
        $this->encryptor    = $encryptor;
    }

    /**
     * @return string
     */
    public function getApiClient(): string
    {
        $value = $this->getEncryptedValue(self::XML_PATH_API_CLIENT);
        return $value;
    }

    /**
     * @return string
     */
    public function getApiSecret(): string
    {
        $value = $this->getEncryptedValue(self::XML_PATH_API_SECRET);
        return $value;
    }

    /**
     * @return string
     */
    public function getEnvironment(): string
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_API_ENVIRONMENT);
        return $value;
    }

    /**
     * @return int
     */
    public function getBatchSize(): int
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_BATCH_SIZE);
        return $value;
    }

    /**
     * @return int
     */
    public function getMaxBatchFails(): int
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_MAX_BATCH_FAILS);
        return $value;
    }

    public function getMaxBatchExecuted(): int
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_MAX_BATCH_FAILS);
        return $value;
    }

    public function importImages(): bool
    {
        $value = (bool) $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_IMPORT_IMAGES);
        return $value;
    }

    /**
     * @param $configPath
     * @return string
     */
    private function getEncryptedValue($configPath): string
    {
        $value = $this->scopeConfig->getValue($configPath);

        $value = $this->encryptor->decrypt($value);

        return $value;
    }

    //Todo should be configurable.
    public function getPreferredLanguage(): string
    {
        return 'nl';
    }

    public function getImportImageType(): string
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_IMPORT_IMAGE_TYPE);

        return $value;
    }

    public function getFrontalImageType(): string
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_FRONTAL_IMAGE_TYPE);

        return $value;
    }

    public function getProductListImageType(): string
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_PRODUCT_IMPORT_PRODUCT_LIST_IMAGE_TYPE);

        return $value;
    }
}
