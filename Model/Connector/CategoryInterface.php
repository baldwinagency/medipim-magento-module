<?php

namespace Baldwin\MedipimConnector\Model\Connector;

/**
 * Interface CategoryInterface
 * @package Baldwin\MedipimConnector\Model\Connector
 */
interface CategoryInterface
{
    const MEDIPIM_API_CATEGORY_ID           = 'id';
    const MEDIPIM_API_CATEGORY_NAME         = 'name';
    const MEDIPIM_API_CATEGORY_PARENT       = 'parent';

    const MEDIPIM_API_META                  = 'meta';

    //These variables are inside the meta array.
    const MEDIPIM_API_CATEGORY_CREATED_AT   = 'createdAt';
    const MEDIPIM_API_CATEGORY_UPDATED_AT   = 'updatedAt';
}
