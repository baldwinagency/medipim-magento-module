<?php

namespace Baldwin\MedipimConnector\Model\Connector;

interface AttributeInterface
{
    const MEDIPIM_API_ATTRIBUTE_ID           = 'code';
    const MEDIPIM_API_ATTRIBUTE_TYPE         = 'type';
    const MEDIPIM_API_ATTRIBUTE_GROUP        = 'attributeGroup';
    const MEDIPIM_API_ATTRIBUTE_IS_MULTI     = 'isMulti';
    const MEDIPIM_API_ATTRIBUTE_IS_LOCALIZED = 'isLocalized';

    const ATTRIBUTE_TYPE_INTEGER             = 'integer';
    const ATTRIBUTE_TYPE_STRING              = 'string';
    const ATTRIBUTE_TYPE_BOOLEAN             = 'boolean';

    const ATTRIBUTE_ATC                      = 'atc';
    const ATTRIBUTE_APB_CATEGORY             = 'apbCategory';
    const ATTRIBUTE_VAT                      = 'vat';
    const ATTRIBUTE_STATUS                   = 'status';
    const ATTRIBUTE_NAME                     = 'name';
    const ATTRIBUTE_CNK                      = 'cnk';
    const ATTRIBUTE_WEIGHT                   = 'weight';
    const ATTRIBUTE_PUBLIC_PRICE             = 'publicPrice';
    const ATTRIBUTE_PRESCRIPTION             = 'prescription';
    const WRITTEN_REQUEST_BY_PATIENT         = 'writtenRequestByPatient';
    const ATTRIBUTE_PUBLIC_CATEGORIES        = 'publicCategories';
    const ATTRIBUTE_SHORT_DESCRIPTION        = 'shortDescription';
    const ATTRIBUTE_BCFI_CATEGORY            = 'bcfiCategory';
    const ATTRIBUTE_ACTIVE_INGREDIENTS       = 'activeIngredients';
    const ATTRIBUTE_DESCRIPTIONS             = 'descriptions';
    const ATTRIBUTE_ATTRIBUTES               = 'attributes';
    const ATTRIBUTE_PACKAGE_QTY              = 'packageQuantity';
    const ATTRIBUTE_MEDIA                    = 'media';
    const ATTRIBUTE_PHOTOS                   = 'photos';
    const ATTRIBUTE_FRONTALS                 = 'frontals';
    const ATTRIBUTE_LINKS                    = 'links';
    const ATTRIBUTE_META                     = 'meta';
    const ATTRIBUTE_REFUND_VALUE_WITH_OMNIO         = 'refundValueWithOmnio';
    const ATTRIBUTE_REFUND_VALUE_WITHOUT_OMNIO      = 'refundValueWithoutOmnio';
    const ATTRIBUTE_CTI_EXTENDED                    = 'ctiExtended';
    const ATTRIBUTE_UDI                             = 'udi';
    const ATTRIBUTE_TRADE_IN_REFUND_VALUE           = 'tradeInRefundValue';
    const ATTRIBUTE_TRADE_IN_MONTHS_BEFORE_EXPIRY   = 'tradeInMonthsBeforeExpiry';
    const ATTRIBUTE_TRADE_IN_MONTHS_AFTER_EXPIRY    = 'tradeInMonthsAfterExpiry';

   //these are multiselects
    const ATTRIBUTE_EAN                      = 'ean';
    const ATTRIBUTE_ORGANIZATIONS            = 'organizations';
    const ATTRIBUTE_GTIN                     = 'gtin';
    const ATTRIBUTE_LEAFLETS                 = 'leaflets';
    const ATTRIBUTE_BRANDS                   = 'brands';

    const SKIP_ATTRIBUTES_DURING_IMPORT = [
        self::ATTRIBUTE_STATUS,
        self::ATTRIBUTE_WEIGHT,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_PUBLIC_PRICE,
        self::ATTRIBUTE_PUBLIC_CATEGORIES,
        self::ATTRIBUTE_ATTRIBUTES,
        self::ATTRIBUTE_PHOTOS,
        self::ATTRIBUTE_FRONTALS,
        self::ATTRIBUTE_DESCRIPTIONS,
        self::ATTRIBUTE_SHORT_DESCRIPTION,
        self::ATTRIBUTE_CTI_EXTENDED,
        self::ATTRIBUTE_ATC,
        self::ATTRIBUTE_UDI,
        self::ATTRIBUTE_TRADE_IN_REFUND_VALUE,
        self::ATTRIBUTE_TRADE_IN_MONTHS_BEFORE_EXPIRY,
        self::ATTRIBUTE_TRADE_IN_MONTHS_AFTER_EXPIRY
    ];

    const SKIP_ATTRIBUTES_IN_MAPPING = [
        self::ATTRIBUTE_PUBLIC_CATEGORIES,
        self::ATTRIBUTE_ATTRIBUTES,
        self::ATTRIBUTE_CTI_EXTENDED,
        self::ATTRIBUTE_ATC,
        self::ATTRIBUTE_UDI,
        self::ATTRIBUTE_MEDIA,
        self::ATTRIBUTE_TRADE_IN_REFUND_VALUE,
        self::ATTRIBUTE_TRADE_IN_MONTHS_BEFORE_EXPIRY,
        self::ATTRIBUTE_TRADE_IN_MONTHS_AFTER_EXPIRY
    ];

    const DEFAULT_MAGENTO_ATTRIBUTE_MAPPING = [
        self::ATTRIBUTE_STATUS => 'status',
        self::ATTRIBUTE_NAME => 'name',
        self::ATTRIBUTE_WEIGHT => 'weight',
        self::ATTRIBUTE_PUBLIC_PRICE => 'price',
        self::ATTRIBUTE_DESCRIPTIONS => 'description',
        self::ATTRIBUTE_SHORT_DESCRIPTION => 'short_description'
    ];

    const DEFAULT_FILTERABLE_ATTRIBUTES = [
        'price',
        'prescription',
        'writtenRequestByPatient',
        'brands',
        'organizations'
    ];
}
