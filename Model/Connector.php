<?php

namespace Baldwin\MedipimConnector\Model;

use Medipim\Api\V3\MedipimApiV3Client;
use Medipim\Api\V3\MedipimApiV3Error;

/**
 * Class Connector
 * @package Baldwin\MedipimConnector\Model
 */
class Connector implements ConnectorInterface
{

    /**
     * Staging base Url
     */
    const STAGING_API_BASE_URL               = 'https://api.dev.medipim.be/v3';
    const PRODUCTION_API_BASE_URL            = 'https://api.medipim.be/v3';

    const API_PATH_ATTRIBUTES_ALL            = '/products/fields/all';
    const API_PATH_PUBLIC_CATEGORIES_ALL     = '/public-categories/all';
    const API_PATH_PRODUCTS_GET              = '/products/get';
    const API_PATH_PRODUCTS_SEARCH           = '/products/search'; //max 10.000 products, with pagination
    const API_PATH_PRODUCTS_EXPORT           = '/products/export'; //gives all the results.

    const API_PATH_ORGANIZATIONS_ALL         = '/organizations/all';
    const API_PATH_BRANDS_ALL                = '/brands/all';

    const PRODUCT_PER_PAGE                   = 100;
    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var MedipimApiV3Client
     */
    private $client;

    /**
     * Connector constructor.
     * @param ConfigInterface $configInterface
     */
    public function __construct(
        ConfigInterface $configInterface
    ) {
        $this->configInterface = $configInterface;
    }

    /**
     * @return $this
     */
    public function connect()
    {
        if (!$this->client == null) {
            return $this;
        }

        $apiClient = $this->configInterface->getApiClient();
        $apiSecret = $this->configInterface->getApiSecret();
        $apiEnv    = $this->configInterface->getEnvironment();

        if (empty($apiClient) || empty($apiSecret)) {
            throw new MedipimApiV3Error("Both the Api Client and Secret should be defined");
        }

        $this->client = new MedipimApiV3Client($apiClient, $apiSecret);

        //change the base url if staging is configured.
        $baseUrl = null;
        switch ($apiEnv) {
            case ConfigInterface::API_ENVIRONMENT_STAGING:
                $baseUrl = self::STAGING_API_BASE_URL;
                break;
            case ConfigInterface::API_ENVIRONMENT_PRODUCTION:
                $baseUrl = self::PRODUCTION_API_BASE_URL;
                break;
        }

        $this->client->setBaseUrl($baseUrl);

        return $this;
    }

    /**
     * Get Medipim product attributes
     * @return array
     */
    public function getProductAttributes(): array
    {
        $attributes = $this->get(self::API_PATH_ATTRIBUTES_ALL);

        $attributes = reset($attributes);

        return $attributes;
    }

    /**
     * Get Medipim Public Categories
     * @return array
     */
    public function getPublicCategories(): array
    {
        $categories =  $this->get(self::API_PATH_PUBLIC_CATEGORIES_ALL);

        $categories = end($categories);

        return $categories;
    }

    /**
     * @param $id
     * @return array
     */
    public function getProductById($id): array
    {
        $query = ['id' => $id];
        return $this->getProduct($query);
    }

    /**
     * @param $cnk
     * @return array
     */
    public function getProductByCnk($cnk): array
    {
        $query = ['cnk' => $cnk];
        return $this->getProduct($query);
    }

    public function searchProducts($query)
    {
        $products = $this->get(self::API_PATH_PRODUCTS_SEARCH, $query);
        return $products;
    }

    /**
     * @return array
     */
    public function getBrands(): array
    {
        $brands =  $this->get(self::API_PATH_BRANDS_ALL);

        $brands = end($brands);

        return $brands;
    }

    /**
     * @return array
     */
    public function getOrganizations(): array
    {
        $organizations =  $this->get(self::API_PATH_ORGANIZATIONS_ALL);

        $organizations = end($organizations);

        return $organizations;
    }

    /**
     * @param $query
     * @return array
     */
    private function getProduct($query): array
    {
        $product = $this->get(self::API_PATH_PRODUCTS_GET, $query);
        return $product;
    }

    /**
     * @param $path
     * @param $query
     */
    private function get($path, $query = null)
    {
        return $this->client->get($path, $query);
    }

    /**
     * @param array $query
     * @param callable $callback
     * @return mixed|void
     */
    public function getProductsExport(array $query, callable $callback)
    {
        $this->client->stream(
            self::API_PATH_PRODUCTS_EXPORT,
            $query,
            $callback
        );
    }
}
