<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Api\Data\ImportProfileInterface;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile as ResourceImportProfile;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile\CollectionFactory as ImportProfileCollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Baldwin\MedipimConnector\Api\JobRepositoryInterface as JobRepository;
use Baldwin\MedipimConnector\Model\JobFactory;

/**
 * Class CategoryRepository
 * @package Baldwin\MedipimConnector\Model
 */
class ImportProfileRepository implements ImportProfileRepositoryInterface
{
    /**
     * @var ResourceImportProfile
     */
    protected $resource;

    /**
     * @var ImportProfileFactory
     */
    protected $importProfileFactory;

    /**
     * @var ImportProfileCollectionFactory
     */
    protected $importProfileCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    protected $dateTime;

    /**
     * @var JobRepository
     */
    protected $jobRepository;

    protected $jobFactory;

    /**
     * ImportProfileRepository constructor.
     * @param ResourceImportProfile $resource
     * @param \Baldwin\MedipimConnector\Model\ImportProfileFactory $importProfileFactory
     * @param ImportProfileCollectionFactory $importProfileCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceImportProfile $resource,
        ImportProfileFactory $importProfileFactory,
        ImportProfileCollectionFactory $importProfileCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        DateTime $dateTime,
        JobRepository $jobRepository,
        JobFactory $jobFactory
    ) {
        $this->resource                          = $resource;
        $this->importProfileCollectionFactory    = $importProfileCollectionFactory;
        $this->importProfileFactory              = $importProfileFactory;
        $this->collectionProcessor               = $collectionProcessor;
        $this->dateTime                          = $dateTime;
        $this->jobRepository                     = $jobRepository;
        $this->jobFactory                        = $jobFactory;
    }

    /**
     * @param Data\ImportProfileInterface $importProfile
     * @return Data\ImportProfileInterface
     * @throws CouldNotSaveException
     */
    public function save(ImportProfileInterface $importProfile)
    {
        try {
            $this->resource->save($importProfile);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the import profile: %1', $exception->getMessage()),
                $exception
            );
        }
        return $importProfile;
    }

    /**
     * @param int $id
     * @return ImportProfile
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $importProfile = $this->importProfileFactory->create();
        $importProfile->load($id);
        if (!$importProfile->getId()) {
            throw new NoSuchEntityException(__('The import profile with the "%1" ID doesn\'t exist.', $id));
        }
        return $importProfile;
    }

    public function delete(ImportProfileInterface $importProfile)
    {
        try {
            $this->resource->delete($importProfile);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not delete the import profile: %1', $exception->getMessage()),
                $exception
            );
        }
        return $importProfile;
    }

    public function schedule(ImportProfileInterface $importProfile)
    {
        try {
            $createdAtTime = $this->dateTime->gmtTimestamp();
            $date = new \DateTime();
            $date->setTimestamp($createdAtTime);
            $datetimeFormat = 'Y-m-d H:i:s';

            /* @var Job $job */
            $job = $this->jobFactory->create();
            $job->setScheduledAt($date->format($datetimeFormat))
                ->setCreatedAt($date->format($datetimeFormat))
                ->setProfileId($importProfile->getId())
                ->setStatus(Job::STATUS_PENDING)
                ->setIsPublished(job::STATUS_NOT_PUBLISHED);

            $this->jobRepository->save($job);

            return $job->getId();

        } catch(\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not schedule import profile: %1', $exception->getMessage()),
                $exception
            );
        }
    }
}
