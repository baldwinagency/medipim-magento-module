<?php

namespace Baldwin\MedipimConnector\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Baldwin\MedipimConnector\Api\Data\AttributeInterface;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface as MedipimAttributeInteface;

/**
 * Class Attribute
 * @package Baldwin\MedipimConnector\Model
 */
class Attribute extends AbstractModel implements AttributeInterface, IdentityInterface, MedipimAttributeInteface
{
    const CACHE_TAG = 'medi_attr';

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Attribute::class);
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ID);
    }

    /**
     * Get Medipim Id
     * @return string|null
     */
    public function getMedipimId()
    {
        return $this->getData(self::MEDIPIM_ID);
    }

    /**
     * Get Eav Attribute Id
     * @return string|null
     */
    public function getAttributeId()
    {
        return $this->getData(self::ATTRIBUTE_ID);
    }

    /**
     * Get Attribute Type
     * @return string|null
     */
    public function getType()
    {
        return $this->getData(self::ATTRIBUTE_TYPE);
    }

    /**
     * Is attribute a multi select
     * @return boolean|null
     */
    public function getIsMulti()
    {
        return $this->getData(self::IS_MULTISELECT);
    }

    /**
     * Is attribute localized
     * @return boolean|null
     */
    public function getIsLocalized()
    {
        return $this->getData(self::IS_LOCALIZED);
    }

    /**
     * Get Created At
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get Updated At
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set Medipim Id
     * @param $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setMedipimId($medipimId)
    {
        return $this->setData(self::MEDIPIM_ID, $medipimId);
    }

    /**
     * Set Eav Attribute Id
     * @param $attributeId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setAttributeId($attributeId)
    {
        return $this->setData(self::ATTRIBUTE_ID, $attributeId);
    }

    /**
     * get Attribute type
     * @param $type
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setType($type)
    {
        return $this->setData(self::ATTRIBUTE_TYPE, $type);
    }

    /**
     * Set attribute as multi select
     * @param $isMulti
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setIsMulti($isMulti)
    {
        return $this->setData(self::IS_MULTISELECT, $isMulti);
    }

    /**
     * Set attribute is localized
     * @param $isLocalized
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setIsLocalized($isLocalized)
    {
        return $this->setData(self::IS_LOCALIZED, $isLocalized);
    }

    /**
     * Set Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Set Data from Medipim Api
     * @param $data
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setDataFromApi($data)
    {
        if (array_key_exists(self::MEDIPIM_API_ATTRIBUTE_ID, $data)) {
            $this->setMedipimId($data[self::MEDIPIM_API_ATTRIBUTE_ID]);
        }

        if (array_key_exists(self::MEDIPIM_API_ATTRIBUTE_TYPE, $data)) {
            $this->setType($data[self::MEDIPIM_API_ATTRIBUTE_TYPE]);
        }

        if (array_key_exists(self::MEDIPIM_API_ATTRIBUTE_IS_MULTI, $data)) {
            $this->setIsMulti($data[self::MEDIPIM_API_ATTRIBUTE_IS_MULTI]);
        }

        if (array_key_exists(self::MEDIPIM_API_ATTRIBUTE_IS_LOCALIZED, $data)) {
            $this->setIsLocalized($data[self::MEDIPIM_API_ATTRIBUTE_IS_LOCALIZED]);
        }
    }
}
