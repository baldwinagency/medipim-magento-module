<?php

namespace Baldwin\MedipimConnector\Model\Import\Brand;

use Baldwin\MedipimConnector\Api\BrandRepositoryInterface;
use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\BrandFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\Attribute\Importer as AttributeImporter;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface;

/**
 * Class Importer
 * @package Baldwin\MedipimConnector\Model\Import\Brand
 */
class Importer
{
    private $connector;

    private $brandRepository;

    private $brandFactory;

    private $logger;

    private $attributeImporter;

    private $productAttributeRepository;

    /**
     * Importer constructor.
     * @param ConnectorInterface $connector
     * @param BrandRepositoryInterface $brandRepository
     * @param BrandFactory $brandFactory
     * @param Logger $logger
     */
    public function __construct(
        ConnectorInterface $connector,
        BrandRepositoryInterface $brandRepository,
        BrandFactory $brandFactory,
        Logger $logger,
        AttributeImporter $attributeImporter,
        ProductAttributeRepositoryInterface $productAttributeRepository
    )
    {
        $this->connector = $connector;
        $this->brandRepository = $brandRepository;
        $this->brandFactory = $brandFactory;
        $this->logger = $logger;
        $this->attributeImporter = $attributeImporter;
        $this->productAttributeRepository = $productAttributeRepository;
    }

    public function execute()
    {
        $this->connector->connect();

        $this->logger->addMessage("Start syncing brands");
        $brands = $this->connector->getBrands();
        $this->insertBrands($brands);

        if ($brands !== null) {
            $countBrands = count($brands);
            $this->logger->addMessage("Finished syncing " . $countBrands . " brands");
        } else {
            $this->logger->addError("Could not fetch brands from API");
        }
    }

    public function insertBrands(array $brands) {
        $attributeOptions = [];
        $attribute = 'medipim_brands';

        foreach ($brands as $brand) {
            try {
                $brandModel = $this->brandRepository->getById((int)$brand['id']);
            }catch(NoSuchEntityException $ex) {
                $this->logger->addMessage($ex->getMessage());
                $brandModel = $this->brandFactory->create();
                $brandModel->setId((int)$brand['id']);
            }
            try{
                $brandModel->setName($brand['name']['en']);
                $this->brandRepository->save($brandModel);
            } catch(\Exception $exception) {
                $this->logger->addCritical("Something went wrong while saving the brands: ". $exception->getMessage());
                break;
            }
        }

        $brandAttribute = $this->productAttributeRepository->get(AttributeInterface::ATTRIBUTE_BRANDS);
        $this->attributeImporter->manageAttributeOptions($brandAttribute, $brands);
    }

}
