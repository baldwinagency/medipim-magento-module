<?php

namespace Baldwin\MedipimConnector\Model\Import;

use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Helper\Logger;
use FireGento\FastSimpleImport\Model\ImporterFactory;

/**
 * Class AbstractImporter
 * @package Baldwin\MedipimConnector\Model\Importer
 */
class AbstractImporter
{
    /**
     * @var ConnectorInterface
     */
    protected $connector;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ImporterFactory
     */
    protected $importerFactory;

    /**
     * AbstractImporter constructor.
     * @param ConnectorInterface $connector
     * @param Logger $logger
     * @param ImporterFactory $importerFactory
     */
    public function __construct(
        ConnectorInterface $connector,
        Logger $logger,
        ImporterFactory $importerFactory
    ) {
        $this->connector       = $connector;
        $this->logger          = $logger;
        $this->importerFactory = $importerFactory;
    }

    public function runImport()
    {
        //TODO: extend from this function
    }
}
