<?php

namespace Baldwin\MedipimConnector\Model\Import\Product;

use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Baldwin\MedipimConnector\Helper\Catalog\Image;
use Baldwin\MedipimConnector\Helper\Import\Category as CategoryHelper;
use Baldwin\MedipimConnector\Helper\Import\Media;
use Baldwin\MedipimConnector\Model\Config\Source\ImportImageType;
use Baldwin\MedipimConnector\Model\ConfigInterface;
use Baldwin\MedipimConnector\Model\Job;
use Baldwin\MedipimConnector\Model\ResourceModel\Batch\CollectionFactory as BatchCollectionFactory;
use Baldwin\MedipimConnector\Api\BatchRepositoryInterface;
use Baldwin\MedipimConnector\Model\Batch;
use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\AbstractImporter;
use Baldwin\MedipimConnector\Api\AttributeRepositoryInterface as MedipimAttributeRepositoryInterface;
use Baldwin\MedipimConnector\Model\Attribute;
use Baldwin\MedipimConnector\Helper\Import\Attribute as AttributeHelper;
use Baldwin\MedipimConnector\Model\ResourceModel\Category as ResourceCategory;
use FireGento\FastSimpleImport\Model\ImporterFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface as MagentoCategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Eav\Api\AttributeRepositoryInterface as EavAttributeRepositoryInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use Baldwin\MedipimConnector\Model\Flag\AttributeMappingChanged;
use Baldwin\MedipimConnector\Helper\Lock as LockHelper;
use Baldwin\MedipimConnector\Model\Import\Supplier\Importer as SupplierImporter;

/**
 * Class Importer
 * @package Baldwin\MedipimConnector\Model\Import\Product
 */
class Importer extends AbstractImporter
{
    const TIME_FORMAT = 'Y-m-d h:i:s';
    const IMPORT_PROFILE = 'catalog_product';
    const DEFAULT_LANGUAGE = 'nl';
    const MULTIPLE_VALUE_SEPERATOR = "||";
    const MULTISELECT_VALUE_SEPERATOR = '|';

    /**
     * @var BatchCollectionFactory
     */
    private $batchCollectionFactory;

    /**
     * @var BatchRepositoryInterface
     */
    private $batchRepository;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var AttributeRepositoryInterface
     */
    private $medipimAttributeRepository;

    /**
     * @var EavAttributeRepositoryInterface
     */
    private $eavAttributeRepositoryInterface;

    /**
     * @var array
     */
    private $eavAttributes;

    /**
     * @var array
     */
    private $medipimAttributes;

    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    private $productEntityType;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var ResourceCategory
     */
    private $resourceCategory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var Media
     */
    private $mediaHelper;

    /**
     * @var CategoryHelper
     */
    private $categoryHelper;

    /**
     * @var MagentoCategoryRepositoryInterface
     */
    private $magentoCategoryRepository;

    /**
     * @var array
     */
    private $cachedMagentoCats;

    /**
     * @var AttributeMappingChanged
     */
    private $attributeMappingChanged;

    /**
     * @var array
     */
    private $hardImageGallery;

    /**
     * @var array
     */
    private $cdnMediaGallery;

    /**
     * @var LockHelper
     */
    private $lockHelper;

    private $importProfileRepository;

    private $jobRepository;

    public function __construct(
        ConnectorInterface $connector,
        Logger $logger,
        ImporterFactory $importerFactory,
        BatchCollectionFactory $batchCollectionFactory,
        BatchRepositoryInterface $batchRepository,
        DateTime $dateTime,
        MedipimAttributeRepositoryInterface $medipimAttributeRepository,
        EavConfig $eavConfig,
        EavAttributeRepositoryInterface $eavAttributeRepository,
        AttributeHelper $attributeHelper,
        ResourceCategory $resourceCategory,
        StoreManagerInterface $storeManager,
        ConfigInterface $configInterface,
        Media $mediaHelper,
        CategoryHelper $categoryHelper,
        MagentoCategoryRepositoryInterface $magentoCategoryRepository,
        AttributeMappingChanged $attributeMappingChanged,
        LockHelper $lockHelper,
        JobRepositoryInterface $jobRepository,
        ImportProfileRepositoryInterface $importProfileRepository
    )
    {
        $this->batchCollectionFactory          = $batchCollectionFactory;
        $this->batchRepository                 = $batchRepository;
        $this->dateTime                        = $dateTime;
        $this->medipimAttributeRepository      = $medipimAttributeRepository;
        $this->eavAttributeRepositoryInterface = $eavAttributeRepository;
        $this->attributeHelper                 = $attributeHelper;
        $this->resourceCategory                = $resourceCategory;
        $this->storeManager                    = $storeManager;
        $this->configInterface                 = $configInterface;
        $this->mediaHelper                     = $mediaHelper;
        $this->categoryHelper                  = $categoryHelper;
        $this->magentoCategoryRepository       = $magentoCategoryRepository;
        $this->attributeMappingChanged         = $attributeMappingChanged;
        $this->lockHelper                      = $lockHelper;
        $this->jobRepository                   = $jobRepository;
        $this->importProfileRepository         = $importProfileRepository;
        $this->productEntityType               = $eavConfig->getEntityType(Product::ENTITY);
        $this->cachedMagentoCats               = [];
        $this->cdnMediaGallery                 = [];
        $this->hardImageGallery                = [];
        $this->eavAttributes                   = [];
        $this->medipimAttributes               = [];

        parent::__construct($connector, $logger, $importerFactory);
    }

    public function singleBatchImport($batchId): bool
    {
        $result = true;
        $batch = $this->batchRepository->getById($batchId);


        $job = $this->jobRepository->getByBatchGroupId($batch->getBatchGroupId());

        try {
            $this->lockHelper->createAndCheckLockFile();

            $this->logger->addMessage("Started Single Batch Import with batch id: " . $batchId);

            if ($job->getStatus() !== Job::STATUS_ERROR) {
                $job->setStatus(Job::STATUS_IMPORTING);
                $this->jobRepository->save($job);
            }

            $storeIds = [];

            $importProfile = $this->importProfileRepository->getById($job->getProfileId());
            if ($importProfile) {
                $storeIds = $importProfile->getStoreIds();
            }

            $this->startImport($batch, $storeIds);

            //if this is the last batch of the job, we set the date on the import profile
            $pendingBatches = $this->batchCollectionFactory->create()
                ->addAttributeToSelect("*")
                ->addFieldToFilter("batch_group_id", ['eq' => $batch->getBatchGroupId()])
                ->addFieldToFilter("status", ['eq' => Batch::SYNC_STATUS_PENDING])
                ->getSize();

            if ($pendingBatches < 1) {
                $endTime = $this->dateTime->gmtTimestamp();

                $job->setStatus(Job::STATUS_SUCCESS);
                $job->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', $endTime));
                $this->jobRepository->save($job);
            }

        } catch(\Exception $exception) {
            $this->logger->addCritical($exception->getMessage());
            $result = false;
            $batch->setStatus(Batch::SYNC_STATUS_ERROR);
            $batch->setSyncError($exception->getMessage());
            $job->setStatus(Job::STATUS_ERROR);
            $this->batchRepository->save($batch);
            $this->jobRepository->save($job);
        }

        //release the lock
        $this->lockHelper->releaseLockFile();

        return $result;
    }

    public function fullBatchImport($status = [Batch::SYNC_STATUS_PENDING], $batchGroupId = null): bool
    {
        $this->logger->addMessage("Started Full Batch Import");
        $this->lockHelper->createAndCheckLockFile();

        $this->attributeMappingChanged = $this->attributeMappingChanged->loadSelf();

        $job = null;
        $importProfile = null;
        $storeIds = [];
        $collection = $this->batchCollectionFactory->create()
            ->addAttributeToSelect('*');

        if ($batchGroupId !== null) {
            $collection->addAttributeToFilter(Batch::BATCH_GROUP_ID, $batchGroupId);
            $job = $this->jobRepository->getByBatchGroupId($batchGroupId);
            $importProfile = $this->importProfileRepository->getById($job->getProfileId());
            $storeIds = $importProfile->getStoreIds();
            $job->setStatus(Job::STATUS_IMPORTING);
            $this->jobRepository->save($job);
        }

        $collection->addAttributeToFilter(Batch::SYNC_STATUS, array(
            'in' => $status
         ));

        /* @var Batch $batch */
        $failedBatches = [];
        $executedBatches = [];
        foreach ($collection as $batch) {
            try {
                $this->startImport($batch, $storeIds);
                $executedBatches[] = $batch->getEntityId();
            } catch (\Exception $ex) {
                //we set errors of the sync
                $this->logger->addCritical($ex);
                $batch->setSyncError($ex->getMessage());
                $batch->setStatus(Batch::SYNC_STATUS_ERROR);
                $this->batchRepository->save($batch);
                $failedBatches[] = $batch;
            }
        }

        $endTime = $this->dateTime->gmtTimestamp();

        if ($batchGroupId !== null) {

            //update the job status
            $job->setStatus(Job::STATUS_SUCCESS);
            $job->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', $endTime));
            $this->jobRepository->save($job);
        }

        //release the lock
        $this->lockHelper->releaseLockFile();

        //we set to flag to null otherwise the message of the attribute mapping won't be hidden.
        $this->attributeMappingChanged->setFlagData(null)->save();

        $this->logger->addMessage("Finished Full Batch Import with "
            . count($failedBatches) . " failed and " . count($executedBatches) . " successful"
        );

        return true;
    }

    public function startImport(Batch $batch, $storeIds = [])
    {
        //We save the start date of the sync
        $endTime = $this->dateTime->gmtTimestamp();
        $batch->setSyncStarted(strftime('%Y-%m-%d %H:%M:%S', $endTime));
        $batch->setSyncError(NULL);
        $batch->setStatus(Batch::SYNC_STATUS_PROCESSING);

        $this->batchRepository->save($batch);

        $products = json_decode($batch->getBatchData(), true);

        $importSuccess = $this->importProducts($products, $storeIds);

        if ($importSuccess === true) {
            //we save the finished time of the sync
            $endTime = $this->dateTime->gmtTimestamp();

            $batch->setSyncFinished(strftime('%Y-%m-%d %H:%M:%S', $endTime));
            $batch->setStatus(Batch::SYNC_STATUS_SUCCESS);
            $this->batchRepository->save($batch);
        }
    }

    /**
     * @param array $products
     * @return bool
     * @throws \Exception
     */
    private function importProducts(array $products, array $storeIds = [])
    {
        //Reset the arrays
        $this->eavAttributes     = [];
        $this->medipimAttributes = [];
        $this->cachedMagentoCats = [];
        $this->cdnMediaGallery   = [];
        $this->hardImageGallery  = [];

        $stores = $this->storeManager->getStores();
        $readyForImport = [];

        foreach ($products as $product) {

            $data = $product['result'];

            //default data
            $compiledData = $this->compileImportData($data, null);
            if ($compiledData !== null) {
                $readyForImport[] = $compiledData;
            }

            $defaultStoreViewOnly = false;
            //If only storeId 0 is defined, we should only import the default Storeview values.
            if (count($storeIds) === 1) {
                if ($storeIds[0] === '0') {
                    $defaultStoreViewOnly = true;
                }
            }

            if (!$defaultStoreViewOnly) {
                //storeview specific data
                foreach ($storeIds as $storeId) {
                    $store = $this->storeManager->getStore($storeId);
                    $compiledData = $this->compileImportData($data, $store);
                    if ($compiledData !== null) {
                        $readyForImport[] = $compiledData;
                    }
                }
            }
        }

        // Run importer
        $importerModel = $this->importerFactory->create();
        $importerModel->setAllowedErrorCount(99);
        $importerModel->setMultipleValueSeparator(self::MULTIPLE_VALUE_SEPERATOR);
        $importerModel->setImportImagesFileDir('pub/media/import');

        $success = $importerModel->processImport($readyForImport);

        if ($success === false) {
            $this->logger->addMessage($importerModel->getLogTrace());
            $this->logger->addMessage($importerModel->getErrorMessages());

            throw new \Exception($importerModel->getLogTrace());
        } else {
            return true;
        }
    }

    /**
     * @param $products
     */
    private function cleanup($products)
    {
        //We remove the import folder for the sku to save space.
        foreach ($products as $product) {
            $path = $this->mediaHelper->getMediaDirectoryAbsolutePath() . "import" . $product['sku'];
            $this->mediaHelper->removeDirectory($path);
        }
    }


    /**
     * Creating a firegento friendly array for the product
     * @param array $data
     * @return array
     */
    private function compileImportData(array $data, Store $store = null): ? array
    {
        $this->cdnMediaGallery = [];
        $this->hardImageGallery = [];

        $language = self::DEFAULT_LANGUAGE;
        $product = [
            'sku' => "MP-" . $data['id'],
            'product_type' => 'simple',
            'attribute_set_code' => 'Default',
            'store_view_code' => null,
            'product_websites' => 'base',
            'status' => Status::STATUS_ENABLED
        ];

        if ($store !== null) {
            $language = $this->attributeHelper->getMedipimLanguageCodeByStoreId($store->getId());
            $product['store_view_code'] = $store->getCode();
        }

        foreach ($data as $key => $value) {
            $attribute = null;
            try {
                if ($key === Attribute::ATTRIBUTE_PHOTOS && $this->configInterface->importImages() === true) {
                    $this->addProductMediaData($product, $value, $language);
                    continue;
                } elseif($key === Attribute::ATTRIBUTE_FRONTALS && $this->configInterface->importImages() === true){
                    $this->addProductFrontalData($product, $value, $language);
                    continue;
                } elseif($key === Attribute::ATTRIBUTE_PUBLIC_CATEGORIES) {
                    try {
                        $product['categories'] = $this->matchCategories($value);
                    } catch(\Exception $exception) {
                        $log = json_encode($value);
                        $this->logger->addCritical("Can't find category {$log} , skipping");
                        return null;
                    }
                    continue;
                } elseif(
                    //we are skipping the import of these attributes
                    $key === Attribute::ATTRIBUTE_META ||
                    $key === Attribute::ATTRIBUTE_LINKS ||
                    $key === "id" ||
                    $key === Attribute::ATTRIBUTE_APB_CATEGORY || //TODO this is a multiselect
                    $key === Attribute::ATTRIBUTE_EAN || //TODO this is a multiselect
                    $key === Attribute::ATTRIBUTE_GTIN || ///TODO this is a multiselect
                    $key === Attribute::ATTRIBUTE_LEAFLETS ||
                    $key === Attribute::WRITTEN_REQUEST_BY_PATIENT ||//TODO SHOULD BE FIXED
                    $key === Attribute::ATTRIBUTE_PACKAGE_QTY ||//TODO SHOULD BE FIXED
                    $key === Attribute::ATTRIBUTE_PRESCRIPTION||//TODO SHOULD BE FIXED
                    $key === Attribute::ATTRIBUTE_VAT //TODO what to do with this?
                ) {
                    continue;
                }

                if (array_key_exists($key, $this->eavAttributes)) {
                    $attribute = $this->eavAttributes[$key]['magento'];
                } else {
                    try {
                        $medipimAttr = $this->medipimAttributeRepository->getByMedipimId($key);
                        $attribute = $this->eavAttributeRepositoryInterface->get(
                            $this->productEntityType->getId(),
                            $medipimAttr->getData("attribute_id")
                        );

                        $this->eavAttributes[$key] = ['medipim' => $medipimAttr, 'magento' => $attribute];
                    } catch (\Exception $ex) {
                        continue;
                    }
                }

                $attrCode = $attribute->getAttributeCode();

                $result = $this->attributeValueCompiler($key, $value, $language, $attribute);
                $product[$attrCode] = $result;
            } catch (\Exception $ex) {
                $this->logger->addMessage($ex);
            }
        }

        switch($this->configInterface->getImportImageType()) {
            case ImportImageType::IMPORT_TYPE_CDN:
                $product['mp_cdn_image_links'] = json_encode($this->cdnMediaGallery);
                break;
            case ImportImageType::IMPORT_TYPE_HARD:
                $result = [];

                //we need to merge the frontals with the additional images.
                foreach ( $this->hardImageGallery as $key => $set) {
                    foreach ($set as $image) {
                        $result[] = $image;
                    }
                }
                $additionalImages = implode(",", $result);
                $product['additional_images'] = $additionalImages;
                break;
        }


        //we need to create the url key manually to avoid duplicate urls
        $product['url_key'] = $this->generateUrlKey($product);
        return $product;
    }

    /**
     * Some values have a wrong format and we need to fix those
     * @param string $key
     * @param string $value
     */
    private function attributeValueCompiler(string $key, $value, ? string $language, AttributeInterface $attribute)
    {
        $result = $value;
        switch ($key) {
            case Attribute::ATTRIBUTE_STATUS:
                switch ($value) {
                    case 'active':
                        $result = Status::STATUS_ENABLED;
                        break;
                    case 'disabled':
                        $result = Status::STATUS_DISABLED;
                        break;
                }
                break;
            case Attribute::ATTRIBUTE_PUBLIC_PRICE:
                if($value === null) {
                    $result = 0;
                } else {
                    $price = $this->attributeHelper->intPriceToFloat($value);
                    $result = $price;
                }
                break;
            case Attribute::ATTRIBUTE_PRESCRIPTION:
                $result = (int) $value;
                break;
            case Attribute::ATTRIBUTE_PUBLIC_CATEGORIES:
                $result = $this->matchCategories($value);
                break;
            case Attribute::ATTRIBUTE_APB_CATEGORY:
                $result = implode(",", $value);
                break;
            case Attribute::ATTRIBUTE_NAME:
            case Attribute::ATTRIBUTE_SHORT_DESCRIPTION:
                $result = $this->attributeHelper->getStoreViewValueOfAttribute($language, $value);
                break;
            case Attribute::ATTRIBUTE_BCFI_CATEGORY:
                $result = null;
                $result = $this->attributeHelper->getStoreViewValueOfAttribute($language, $value['name']);
                break;
            case Attribute::ATTRIBUTE_ACTIVE_INGREDIENTS:
                $ingredients = [];
                if(is_array($value)) {
                    foreach ($value as $ingredient) {
                        $ingredients[] = $this->attributeHelper->getStoreViewValueOfAttribute($language, $ingredient['name']);
                    }
                }
                $result = implode(",", $ingredients);
                break;
            case Attribute::ATTRIBUTE_DESCRIPTIONS:
                $descriptions = [];
                foreach ($value as $description) {
                    //We only need to fetch the descriptions with public targetGroup
                    //We wrap the descriptions in divs.
                    if (in_array("public", $description['targetGroups'])) {
                        $type = $description['type'];
                        $header = $type;
                        $content = $this->attributeHelper->getStoreViewValueOfAttribute(
                            $language, $description['content']
                        );

                        if(empty($content['html'])) {
                            continue;
                        }
                        $headerContent = $this->attributeHelper->snakeToString($header);

                        $row = ['headerId' => $header, 'headerContent' => $headerContent, 'content' => $content['html']];
                        $descriptions[] = $row;
                    }
                }
                $result = json_encode($descriptions);
                break;
            case Attribute::ATTRIBUTE_ORGANIZATIONS:
            case Attribute::ATTRIBUTE_BRANDS:
                $result = null;

                if (is_array($value) && count($value) > 0) {
                    $result = [];
                    foreach ($value as $v) {
                        if ($key === Attribute::ATTRIBUTE_ORGANIZATIONS) {

                            //some orgs we don't want to import.
                            if (in_array($v['id'], SupplierImporter::SKIPPED_SUPPLIERS)) {
                                continue;
                            }
                        }
                        if (!in_array($v['id'], $result)) {
                            $result[] = $v['id'];
                        }
                    }

                    $merged = implode(self::MULTISELECT_VALUE_SEPERATOR, $result);
                    $result = $merged;
                }

                break;

        }

        return $result;
    }

    /**
     * Function the generate the url key with the cnk code
     * @param array $product
     * @return string
     */
    private function generateUrlKey(array $product): string
    {
        $nameAttr = $this->eavAttributes[Attribute::ATTRIBUTE_NAME]['magento'];
        $cnkAttr = $this->eavAttributes[Attribute::ATTRIBUTE_CNK]['magento'];

        $title = $product[$nameAttr->getAttributeCode()];
        $cnkCode = $product[$cnkAttr->getAttributeCode()];

        $url = preg_replace('#[^0-9a-z]+#i', '-', $title);

        $urlKey = strtolower($url) . '-' . $cnkCode;

        return $urlKey;
    }

    /**
     * @param array $categories
     * @return array
     */
    private function matchCategories(array $medipimCategories): string
    {
        $result = "";
        $catIds = [];

        /* @var \Baldwin\MedipimConnector\Model\ResourceModel\Category $resourceCategory */
        foreach ($medipimCategories as $category) {
            try {
                $id = $category['id'];
                $result = $this->resourceCategory->lookupMagentoCategoryIds($id);
                if ($result !== null) {
                    array_push($catIds, $result);
                } else {
                    throw new LocalizedException("Medipim Category with id {$id} does not exist");
                }
            } catch (LocalizedException $exception) {
                $this->logger->addMessage($exception);
            }
        }
        if (!empty($catIds)) {
            $catIds = call_user_func_array('array_merge', $catIds);

            $result = [];
            foreach ($catIds as $id) {
                if (! in_array($id, $this->cachedMagentoCats)) {
                    $this->cachedMagentoCats[$id] = $this->magentoCategoryRepository->get($id);
                }

                /* @var Category $category*/
                $category = $this->cachedMagentoCats[$id];

                $result[] = $this->categoryHelper->createNamePathFromCategoryUrlPath($category->getPath());
            }
            $result = implode(self::MULTIPLE_VALUE_SEPERATOR, $result);
        }
        return $result;
    }

    /**
     * Packshots should be taken first as the base image. If no packshots are defined, we should go to the porductshots
     * @param array $product
     * @param $media
     */
    private function addProductMediaData(array $product, array $media, $language)
    {
        $importImageType = $this->configInterface->getImportImageType();

        $packshots = [];
        $productshots = [];
        $baseImage = null;
        foreach ($media as $image) {
            //check if image has the locale.
            if (in_array($language, $image['locales'])) {
                switch ($image['type']) {
                    case 'packshot':
                        $packshots[] = $image;
                        break;
                    case 'productshot':
                        $productshots[] = $image;
                        break;
                }
            }
        }

        if (!empty($packshots)) {
            $packshots = $this->getOrderedPhotos($packshots);
        }

        if (!empty($productshots)) {
            $productshots = $this->getOrderedPhotos($productshots);
        }

        switch($importImageType) {
            //This is the hard image import. So we download the images
            case ImportImageType::IMPORT_TYPE_HARD:

                //We also set the CDN data since the shopowner might change it's preference
                $this->addToCdnImageSet('packshots', $packshots);
                $this->addToCdnImageSet('productshots', $productshots);

                //we shift the base image, otherwise it's duplicated
                if (!empty($packshots)) {
                    $baseImage = array_shift($packshots);
                } elseif (!empty($productshots)) {
                    $baseImage = array_shift($productshots);
                }

                $additionalPackshots = $this->getAdditionalImages($product, $packshots);
                $additionalProductShots = $this->getAdditionalImages($product, $productshots);
                $additionalImages = array_merge($additionalPackshots, $additionalProductShots);

                if ($baseImage) {
                    $path = $this->checkImage(
                        $product['sku'],
                        $baseImage['formats']['huge'],
                        $baseImage['meta']['updatedAt']
                    );

                    $product['base_image'] = $path;
                    $product['small_image'] = $path;
                    $product['thumbnail_image'] = $path;
                }
                if (!empty($additionalImages)) {
                    $this->addToHardImageSet('additional_images', $additionalImages);
                }
                break;
            //This is the CDN import, so we only need links.
            case ImportImageType::IMPORT_TYPE_CDN:
                $this->addToCdnImageSet('packshots', $packshots);
                $this->addToCdnImageSet('productshots', $productshots);
                break;
        }
    }

    /**
     * @param array $product
     * @param array $frontals
     */
    private function addProductFrontalData(array $product, array $frontals, $language)
    {
        $frontalType = $this->configInterface->getFrontalImageType();
        $importType = $this->configInterface->getImportImageType();

        $result = [];
        foreach ($frontals as $frontal) {
            //check if the image has the correct locale
            if (in_array($language, $frontal['locales'])) {
                switch ($importType) {
                    case ImportImageType::IMPORT_TYPE_HARD:
                        $result[] = $this->checkImage(
                            $product['sku'],
                            $frontal['formats'][$frontalType],
                            $frontal['meta']['updatedAt']
                        );
                        break;
                    case ImportImageType::IMPORT_TYPE_CDN:
                        $result[] = $frontal;
                        break;
                }
            }
        }

        switch ($importType) {
            case ImportImageType::IMPORT_TYPE_HARD:
                $this->addToHardImageSet('frontals', $result);
                break;
            case ImportImageType::IMPORT_TYPE_CDN:
                $this->addToCdnImageSet('frontals', $result);
                break;
        }
    }

    /**
     * Add key value to cdn Image set
     * @param $baseImage
     * @param $additionalImages
     */
    private function addToCdnImageSet($key, $value)
    {
        if(array_key_exists($key, $this->cdnMediaGallery)) {
            array_push($this->cdnMediaGallery[$key], $value);

        } else {
            $this->cdnMediaGallery[$key] = $value;
        }
    }

    /**
     * Add key value to hard Image set
     * @param $baseImage
     * @param $additionalImages
     */
    private function addToHardImageSet($key, $value)
    {
        if(array_key_exists($key, $this->hardImageGallery)) {
            array_push($this->hardImageGallery[$key], $value);

        } else {
            $this->hardImageGallery[$key] = $value;
        }
    }

    /**
     * order photos by updatedAt
     * @param array $photos
     */
    private function getOrderedPhotos(array $photos): array
    {

        //we change the indexes.
        uasort($photos, [$this, 'compareByUpdatedAt']);

        return $photos;
    }

    private function compareByUpdatedAt($a, $b)
    {
        return $b['meta']['updatedAt'] <=> $a['meta']['updatedAt'];
    }

    private function getAdditionalImages(array $product, array $photos)
    {
        $additionalImages = [];
        foreach ($photos as $photo) {
            $path =  $this->checkImage(
                $product['sku'],
                $photo['formats']['huge'],
                $photo['meta']['updatedAt']
            );
            $additionalImages[] = $path;
        }

        return $additionalImages;
    }

    /**
     * Download image and get the path.
     * @param string $sku
     * @param $imageUrl
     * @param $updatedAt
     * @return string
     */
    private function checkImage(string $sku, $imageUrl, $updatedAt): string
    {
        $imageName = $this->mediaHelper->getImageFileNameOfUrl($imageUrl);

        $timestamp = $updatedAt;

        //We need to add the timestamp to the image so that we know when the image needs to be downloaded again.
        $imageName = $timestamp . "_" . $imageName;
        $location = $this->mediaHelper->getMediaDirectoryAbsolutePath()
            . "import" . DIRECTORY_SEPARATOR . $sku . DIRECTORY_SEPARATOR;

        $path = $this->mediaHelper->downloadImage($imageUrl, $location, $imageName);

        return $sku . DIRECTORY_SEPARATOR . $imageName;
    }
}
