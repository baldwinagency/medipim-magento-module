<?php

namespace Baldwin\MedipimConnector\Model\Import\Product;

use Baldwin\MedipimConnector\Api\BatchRepositoryInterface;
use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\Batch;
use Baldwin\MedipimConnector\Model\ConfigInterface;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\AbstractImporter;
use Baldwin\MedipimConnector\Model\BatchFactory;
use FireGento\FastSimpleImport\Model\ImporterFactory;

/**
 * Class Importer
 * @package Baldwin\MedipimConnector\Model\Import\Product
 */
class Syncer extends AbstractImporter
{
    const IMPORT_PROFILE = 'catalog_product';
    const TOPIC_NAME = 'medipim.batch.import';

    /**
     * @var BatchRepositoryInterface
     */
    private $batchRepository;

    /**
     * @var BatchFactory
     */
    private $batchFactory;

    private $products;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    public function __construct(
        ConnectorInterface $connector,
        Logger $logger,
        ImporterFactory $importerFactory,
        BatchFactory $batchFactory,
        BatchRepositoryInterface $batchRepository,
        ConfigInterface $configInterface
    )
    {
        $this->batchFactory    = $batchFactory;
        $this->batchRepository = $batchRepository;
        $this->configInterface = $configInterface;
        parent::__construct($connector, $logger, $importerFactory);
    }

    /**
     * @param array $query
     * @return null|string|void
     */
    public function fetchFromApi(array $query = [], $batchGroupId): bool
    {
        $this->numberOfBatches = 0;
        $this->total = 0;
        $this->products = [];
        $this->connector->connect();

        $this->logger->addMessage("Start syncing products from api with query: ". json_encode($query, JSON_PRETTY_PRINT));

        $maxBatchSize = $this->configInterface->getBatchSize();

        $this->connector->getProductsExport($query, function ($data) use ($batchGroupId, $maxBatchSize) {
            $total = $data['meta']['total'];
            $index = $data['meta']['index'];
            $toImport = $total - $index;
            $this->products[] = $data;
            $this->total += $total;
            if (count($this->products) === $maxBatchSize || ($toImport < $maxBatchSize && count($this->products) === $toImport)) {
                $batch = $this->products;
                $productsCount = count($batch);
                $this->createBatch($batch, $batchGroupId, $productsCount);
                $this->products = [];
                $this->numberOfBatches= +1;
            }
        });

        $this->logger->addMessage(
            sprintf(
                "finished syncing products from api. Created {$this->numberOfBatches} batches for %s products",
                $this->total
            )
        );

        if ($this->numberOfBatches > 0) {
            return true;
        }

        return false;
    }

    private function createBatch(array $batchData, string $batchGroupId, int $size)
    {
        /* @var Batch $batch */
        $batch = $this->batchFactory->create();
        $batch->setBatchData(json_encode($batchData));
        $batch->setType(Batch::BATCH_TYPE_PRODUCT);
        $batch->setStatus(Batch::SYNC_STATUS_PENDING);
        $batch->setBatchGroupId($batchGroupId);
        $batch->setEntities($size);

        try {
            $this->batchRepository->save($batch);
        } catch (\Exception $ex) {
            $this->logger->addMessage($ex->getMessage());
        }
    }
}
