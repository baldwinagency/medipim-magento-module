<?php

namespace Baldwin\MedipimConnector\Model\Import\Category;

use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\AbstractImporter;
use FireGento\FastSimpleImport\Model\ImporterFactory;
use Baldwin\MedipimConnector\Api\CategoryRepositoryInterface;
use Baldwin\MedipimConnector\Helper\Import\Category as CategoryHelper;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\Collection;
use Magento\Catalog\Api\CategoryRepositoryInterface as MagentoCategoryRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory as MagentoCategoryFactory;
use Baldwin\MedipimConnector\Model\CategoryFactory as MedipimCategoryFactory;
use Magento\Framework\Exception\LocalizedException;
use Baldwin\MedipimConnector\Model\Category;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\CollectionFactory as MedipimCategoryCollectionFactory;
use Baldwin\MedipimConnector\Model\ResourceModel\CategoryFactory as MedipimCategoryResourceModelFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as MagentoCategoryCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Importer
 * @package Baldwin\MedipimConnector\Model\Importer\Category
 */
class Importer extends AbstractImporter
{
    const FULL_IMPORT = true;
    const IMPORT_PROFILE = 'catalog_category';
    const ROOT_CATEGORY = 'Default Category';

    /**
     * @var AttributeRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var MagentoCategoryRepositoryInterface
     */
    private $magentoCategoryRepositoryInterface;

    /**
     * @var CategoryFactory
     */
    private $magentoCategoryFactory;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var MedipimCategoryCollectionFactory
     */
    private $collectionFactory;

    /**
     * @var MedipimCategoryResourceModelFactory
     */
    private $resourceModelFactory;

    /**
     * @var array
     */
    private $categories;

    /**
     * @var CategoryHelper
     */
    private $categoryHelper;

    /**
     * @var array
     */
    private $addedNodes;

    private $magentoCategoryCollectionFactory;

    private $storeManager;

    /**
     * @var \Magento\Store\Api\Data\StoreInterface[] $magentoStores
     */
    private $magentoStores;

    public function __construct(
        ConnectorInterface $connector,
        Logger $logger,
        ImporterFactory $importerFactory,
        CategoryRepositoryInterface $categoryRepository,
        MagentoCategoryRepositoryInterface $magentoCategoryRepositoryInterface,
        MagentoCategoryFactory $magentoCategoryFactory,
        MedipimCategoryFactory $categoryFactory,
        MedipimCategoryCollectionFactory $collectionFactory,
        MedipimCategoryResourceModelFactory $resourceModelFactory,
        CategoryHelper $categoryHelper,
        MagentoCategoryCollectionFactory $magentoCategoryCollectionFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->categoryRepository                 = $categoryRepository;
        $this->magentoCategoryRepositoryInterface = $magentoCategoryRepositoryInterface;
        $this->magentoCategoryFactory             = $magentoCategoryFactory;
        $this->categoryFactory                    = $categoryFactory;
        $this->collectionFactory                  = $collectionFactory;
        $this->resourceModelFactory               = $resourceModelFactory;
        $this->categories                         = [];
        $this->categoryHelper                     = $categoryHelper;
        $this->addedNodes                         = [];
        $this->magentoCategoryCollectionFactory   = $magentoCategoryCollectionFactory;
        $this->storeManager                       = $storeManager;

        parent::__construct($connector, $logger, $importerFactory);
    }

    public function runImport()
    {
        $this->magentoStores = $this->storeManager->getStores();

        $this->logger->logMessage("Fetching categories from api");

        $this->fetchFromApi();

        $this->logger->logMessage("Done fetching from api");
        $this->logger->logMessage("Generating Category tree levels");

        $categoriesCollection = $this->collectionFactory
            ->create()
            ->addAttributeToSelect("*")
            ->addAttributeToSort("medipim_id", 'asc');

        $this->calculateCategoryLevels($categoriesCollection);

        $this->logger->logMessage("Importing categories in Catalog");

        $categoriesCollection = $this->collectionFactory
            ->create()
            ->addAttributeToSort("level", "asc");

        $this->fireGentoImport($categoriesCollection);
        $this->logger->logMessage("Finished importing categories");

        $this->logger->logMessage("Running post import checks");

        $this->afterImport();

        $this->logger->logMessage("Finished importing categories");
    }

    protected function fetchFromApi()
    {
        $this->connector->connect();

        $categories = $this->connector->getPublicCategories();

        foreach ($categories as $category) {
            $categoryModel = null;

            $categoryModel = $this->getCategory($category[Category::MEDIPIM_API_CATEGORY_ID], true);

            try {
                $categoryModel->setDataFromApi($category);
                $this->categoryRepository->save($categoryModel);
                $this->categories[$categoryModel->getMedipimId()] = $categoryModel;
            } catch (LocalizedException $exception) {
                $this->logger->addMessage($exception->getMessage());
            }
        }
    }

    /**
     * We need to calculate the category tree depth
     * @param Collection $categoriesCollection
     */
    private function calculateCategoryLevels(Collection $categoriesCollection)
    {
        foreach ($categoriesCollection as $category) {

            $cat = $this->getCategory($category->getMedipimId(), true);
            $parent = $this->getCategory($category->getMedipimParentId());

            $path = [];
            $level = $this->getLevel($cat, $parent, $path);

            $cat->setLevel($level);

            try {
                $this->categoryRepository->save($cat);
            } catch (LocalizedException $exception) {
                $this->logger->addMessage($exception);
            }
        }
    }

    /**
     * @param int|null $id
     * @param bool $returnObject
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface|Category|mixed|null
     */
    private function getCategory(? int $id, $returnObject = false)
    {
        $category = null;

        if ($id == null) {
            return $category;
        }

        if (!array_key_exists($id, $this->categories)) {
            try {
                $category = $this->categoryRepository->getByMedipimId($id);
            } catch (\Exception $ex) {
                $category = null;

                if ($returnObject === true) {
                    $category = $this->categoryFactory->create();
                }
            }
            $this->categories[$id] = $category;
        } else {
            $category = $this->categories[$id];
        }

        return $category;
    }

    /**
     * @param Category $category
     * @param Category|null $parent
     * @param int $level
     * @param array $path
     * @return int
     */
    private function getLevel(Category $category, ? Category $parent, array $path): int
    {
        $level = (count($path) / 2);

        if ($parent === null) {
            return $level;
        }

        if (in_array($category->getMedipimParentId(), $path, true)) {
            $this->logger->logMessage("Error in categories with medipim ID: " . $category->getMedipimId() . " already in path " . json_encode($path));
            return $level;
        } elseif ($category->getMedipimId() === $category->getMedipimParentId()) {
            $this->logger->logMessage("Recursive match: Error in categories with medipim ID: " . $category->getMedipimId());
            return $level;
        }

        array_push($path, $parent->getMedipimId());

        if ($category->getMedipimParentId() !== null) {
            $oldParent = $parent;
            $parent = $this->getCategory($category->getMedipimParentId());
            $category = $oldParent;
        } else {
            $parent = null;
        }

        return $this->getLevel($category, $parent, $path);
    }

    private function fireGentoImport(Collection $categories)
    {
        $this->logger->logMessage("Creating Category import tree");

        $data = [];
        foreach ($categories as $category) {
            $this->addCategoryTreeNode($category, $data);
        }

        $this->logger->logMessage("Finished creating Category import tree.");


        // Run importer
        $importerModel = $this->importerFactory->create();
        $importerModel->setEntityCode(self::IMPORT_PROFILE);
        $importerModel->setAllowedErrorCount(100);


        try {
            $nodesCount = count($this->addedNodes);
            $this->logger->logMessage("Starting import tree with {$nodesCount} nodes");
            $success = $importerModel->processImport($data);
            if ($success === false) {
                $this->logger->addMessage($importerModel->getLogTrace());
                throw new \Exception($importerModel->getLogTrace());
            }

        } catch (\Exception $e) {
            $this->logger->addMessage($e->getMessage());
        }


        $this->logger->addMessage($importerModel->getLogTrace());
        $this->logger->addMessage($importerModel->getErrorMessages());

    }

    /**
     * We need to fill in the link table of medipim and magento categories.
     * @throws LocalizedException
     */
    private function afterImport()
    {
        $collection = $this->magentoCategoryCollectionFactory->create()
            ->addAttributeToSelect("medipim_ids");

        $matchCategories = [];
        foreach ($collection as $cat) {
            $catIds = [];
            if($cat->getData("medipim_ids") !== null) {
                $catIds = explode(",", $cat->getData("medipim_ids"));
            }

            foreach($catIds as $medipimId) {
                $matchCategories[$medipimId] = $cat->getId();
            }
        }

        foreach ($matchCategories as $medipimCat => $magentoCat) {
            try {
                $catModel = $this->categoryRepository->getByMedipimId($medipimCat);
                $catModel->setMagentoCategoryId($magentoCat);
                $this->categoryRepository->save($catModel);
            } catch(\Exception $exception) {
                $this->logger->addMessage(
                    sprintf("Error: MedipimCat %s MagentoCat %s: ", $medipimCat, $magentoCat)
                    .  $exception->getMessage()
                );
            }
        }

    }

    private function count_array_values($my_array, $match)
    {
        $count = 0;

        foreach ($my_array as $key => $value)
        {
            if ($value == $match)
            {
                $count++;
            }
        }

        return $count;
    }

    private function addCategoryTreeNode(Category $category, &$nodes, $parents = [], $parentIds = [], $parentId = null)
    {
        //each node should only be added once.
        $nrMagentoStores = count($this->magentoStores);
        if ($this->count_array_values($this->addedNodes, $category->getMedipimId()) < $nrMagentoStores) {

            $catName = trim($category->getName("en"));
            $parents[] = addcslashes(rtrim($catName), "/");
            $parentIds[] = $category->getMedipimId();

            $rootUrlPath = $this->categoryHelper->createUrlPathFromMedipimCategoryIds($parentIds);

            //we are saving the rootUrlPath
            $category->setPath($rootUrlPath);

            $this->categoryRepository->save($category);

            $categoryStatus = 1;


            // NOTE: _category & name should be the same (only difference is _category has escaped delimiter)
            $nodes[] = [
                '_root' => self::ROOT_CATEGORY,
                '_category' => implode('/', $parents), // delimiter in name should be escaped
                'name' => $catName, // delimiter in name should not be escaped here
                'is_active' => $categoryStatus,
                'url_key' => $this->categoryHelper->getImportCategoryUrlKeyFromName($catName),
                'url_path' => $rootUrlPath,
                'include_in_menu' => 1,
                'is_anchor' => 1,
                'node_id' => (int) $category->getMedipimId(),
                'node_root_path' => $rootUrlPath,
                'available_sort_by' => 'position',
                'default_sort_by' => 'position',
                'medipim_ids' => $category->getMedipimId()
            ];


            foreach ($this->magentoStores as $store) {
                if ($store->getCode() === 'admin') {
                    continue;
                }

                $medipimLang = $store->getData(CategoryHelper::MEDIPIM_CODE);

                //If no medipim language is found, we don't need to generate url paths/keys
                if ($medipimLang === NULL){
                    continue;
                }

                $this->addedNodes[] = $category->getMedipimId();

                $storeCatName = ucwords($category->getName($medipimLang));
                $urlKey = $this->categoryHelper->getImportCategoryUrlKeyFromName($category->getName($medipimLang));

                $urlPath = $this->categoryHelper->createUrlPathFromMedipimCategoryIds($parentIds, $medipimLang);

                $nodes[] = [
                    '_root' => self::ROOT_CATEGORY,
                    '_store' => $store->getCode(),
                    'name' => $storeCatName,
                    'url_key' => $urlKey,
                    'url_path' => $urlPath,
                    'is_active' => 1,
                    'node_id' =>  (int) $category->getMedipimId(),
                    'node_root_path' => $urlPath
                ];

                $catId = $category->getMedipimId();
                if ($parentId !== null) {
                    $catId = $parentId . '/' . $catId;
                }
            }

            $children = $this->resourceModelFactory->create()
                ->lookupChildren($category->getMedipimId());

            // Check if the category has children (and repeat the process)
            if (isset($children) && count($children) > 0) {
                foreach ($children as $child) {
                    $child = $this->getCategory($child['medipim_id']);
                    $this->addCategoryTreeNode($child, $nodes, $parents, $parentIds, $catId);
                }
            }
        }
    }
}
