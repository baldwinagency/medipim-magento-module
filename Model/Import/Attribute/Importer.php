<?php

namespace Baldwin\MedipimConnector\Model\Import\Attribute;

use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\AbstractImporter;
use Baldwin\MedipimConnector\Model\Attribute;
use Baldwin\MedipimConnector\Helper\Import\Attribute as AttributeHelper;
use Baldwin\MedipimConnector\Api\AttributeRepositoryInterface;
use Baldwin\MedipimConnector\Model\AttributeFactory as MedipimAttributeFactory;
use FireGento\FastSimpleImport\Model\ImporterFactory;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Api\AttributeRepositoryInterface as EavAttributeRepositoryInterface;
use Magento\Eav\Api\AttributeManagementInterface as EavAttributeManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory as CatalogAttributeFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory;
use Magento\Framework\View\Design\Theme\LabelFactory;
use \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttributeOptionCollectionFactory;
use Magento\Eav\Api\AttributeRepositoryInterface as MagentoAttributeRepositoryInterface;
use \Magento\Eav\Setup\EavSetupFactory;
/**
 * Class Importer
 * @package Baldwin\MedipimConnector\Model\Import\Attribute
 */
class Importer extends AbstractImporter
{
    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var MedipimAttributeFactory
     */
    private $attributeFactory;

    /**
     * @var CatalogAttributeFactory
     */
    private $catalogAttributeFactory;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    private $productEntityType;

    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    private $categoryEntityType;

    /**
     * @var EavAttributeRepositoryInterface
     */
    private $eavAttributeRepositoryInterface;

    /**
     * @var AttributeOptionInterfaceFactory
     */
    private $attributeOptionFactory;

    private $attributeOptionLabelFactory;

    private $attributeOptionManagement;

    private $eavAttributeManagement;

    private $attributeOptionCollectionFactory;

    private $magentoAttributeRepo;

    private $eavSetupFactory;

    /**
     * Importer constructor.
     * @param ConnectorInterface $connector
     * @param Logger $logger
     * @param ImporterFactory $importerFactory
     * @param AttributeRepositoryInterface $attributeRepository
     * @param MedipimAttributeFactory $attributeFactory
     * @param CatalogAttributeFactory $catalogAttributeFactory
     * @param AttributeHelper $attributeHelper
     */
    public function __construct(
        ConnectorInterface $connector,
        Logger $logger,
        ImporterFactory $importerFactory,
        AttributeRepositoryInterface $attributeRepository,
        MedipimAttributeFactory $attributeFactory,
        CatalogAttributeFactory $catalogAttributeFactory,
        AttributeHelper $attributeHelper,
        EavConfig $eavConfig,
        EavAttributeRepositoryInterface $eavAttributeRepository,
        AttributeOptionInterfaceFactory $attrtibuteOptionFactory,
        AttributeOptionLabelInterfaceFactory $attributeOptionLabelFactory,
        AttributeOptionManagementInterface $attributeOptionManagement,
        EavAttributeManagementInterface $eavAttributeManagement,
        AttributeOptionCollectionFactory $attributeOptionCollectionFactory,
        MagentoAttributeRepositoryInterface $magentoAttributeRepo,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->attributeRepository              = $attributeRepository;
        $this->attributeFactory                 = $attributeFactory;
        $this->catalogAttributeFactory          = $catalogAttributeFactory;
        $this->attributes                       = [];
        $this->attributeHelper                  = $attributeHelper;
        $this->productEntityType                = $eavConfig->getEntityType(Product::ENTITY);
        $this->categoryEntityType               = $eavConfig->getEntityType(Category::ENTITY);
        $this->eavConfig                        = $eavConfig;
        $this->eavAttributeRepositoryInterface  = $eavAttributeRepository;
        $this->attributeOptionFactory           = $attrtibuteOptionFactory;
        $this->attributeOptionLabelFactory      = $attributeOptionLabelFactory;
        $this->attributeOptionManagement        = $attributeOptionManagement;
        $this->eavAttributeManagement           = $eavAttributeManagement;
        $this->attributeOptionCollectionFactory = $attributeOptionCollectionFactory;
        $this->magentoAttributeRepo             = $magentoAttributeRepo;
        $this->eavSetupFactory = $eavSetupFactory;

        parent::__construct($connector, $logger, $importerFactory);
    }

    public function runImport()
    {
        $this->logger->logMessage("Fetching categories from api");
        $this->fetchFromApi();
        $this->logger->logMessage("Done fetching from api");
        $this->logger->logMessage("Start importing attributes");
        $this->importAttributes();
        $this->logger->logMessage("Finished importing attributes");
        $this->setCategoryAttribute();
    }

    protected function fetchFromApi()
    {
        $this->connector->connect();

        $data = $this->connector->getProductAttributes();

        foreach ($data as $attribute) {
            try {
                //TODO this is exhausting for the database. We should do 1 query.
                $attrModel = $this->attributeRepository->getByMedipimId(
                    $attribute[Attribute::MEDIPIM_API_ATTRIBUTE_ID]
                );

            } catch (LocalizedException $exception) {
                //the model doesn't exist yet so we need to create a new one.
                $attrModel = $this->attributeFactory->create();
            }

            try {
                $attrModel->setDataFromApi($attribute);
                $this->attributeRepository->save($attrModel);
                $this->attributes[] = $attrModel;
            } catch (LocalizedException $exception) {
                $this->logger->addMessage($exception->getMessage());
            }
        }
    }

    private function importAttributes()
    {
        /* @var Attribute $attribute */
        foreach ($this->attributes as $attribute) {
            $attributeCode = $attribute->getMedipimId();
            $eavAttrId = null;

            //we don't want to add attributes that are default magento attributes
            if (!in_array($attributeCode, Attribute::SKIP_ATTRIBUTES_DURING_IMPORT, true) === true) {
                $attrName = ucfirst($this->attributeHelper->camelCaseToString($attributeCode));
                $type = $attribute->getType();
                $isMultiSelect = $attribute->getIsMulti();
                $entityId = $this->productEntityType->getId();
                $attrCode = $this->attributeHelper->camelToSnake($attributeCode);
                $eavAttrId = $this->addAttribute(
                    $attrCode,
                    $attrName,
                    $type,
                    $isMultiSelect,
                    $entityId,
                    Product::ENTITY,
                    'Medipim',
                    7, //TODO remove the hardcoded value
                    4 //TODO remove the hardcoded value
                );

            } else {
                if (array_key_exists($attribute->getMedipimId(), Attribute::DEFAULT_MAGENTO_ATTRIBUTE_MAPPING)) {
                    $attrCode = Attribute::DEFAULT_MAGENTO_ATTRIBUTE_MAPPING[$attribute->getMedipimId()];
                    try {
                        $eavAttrId = $this->eavAttributeRepositoryInterface->get(
                            $this->productEntityType->getId(),
                            $attrCode
                        )->getAttributeId();
                    } catch (NoSuchEntityException $ex) {
                        $this->logger->addMessage($ex);
                    }
                } else {
                    $this->logger->addMessage(
                        "Attribute with id {$attribute->getMedipimId()} does not exist in default mapping"
                    );
                }
            }

            //we match the eav attribute with the one of Medipim.
            $attribute->setAttributeId($eavAttrId);
            $this->attributeRepository->save($attribute);
        }
    }

    private function setCategoryAttribute()
    {
        //We also need to set a new attribute on the category entity to save the medipim IDs.
        $attributeCode = "medipim_ids";
        $attrName = "Medipim IDs";
        $type = "string";
        $isMultiSelect = false;
        $entityId = $this->categoryEntityType->getId();
        $attributeGroup = 'General Information';
        $this->addAttribute(
            $attributeCode,
            $attrName,
            $type,
            $isMultiSelect,
            $entityId,
            Category::ENTITY,
            $attributeGroup,
            4, //TODO remove the hardcoded value
            3 //TODO remove the hardcoded value
        );
    }

    /**
     * @param string $attrCode
     * @param string $label
     * @param $type
     * @param bool $isMultiSelect
     * @param Attribute $medipimAttribute
     * @return mixed
     */
    private function addAttribute(
        string $attrCode,
        string $label,
        $type,
        bool $isMultiSelect,
        int $entityId,
        string $entityTypeCode,
        string $attributeGroup = null,
        int $attributeGroupId,
        int $attributeSetId
    ) {
        $attributeData = [];
        try {
            $attributeModel = $this->eavAttributeRepositoryInterface->get($this->productEntityType->getId(), $attrCode);
        } catch (\Exception $ex) {
            $attributeModel = $this->catalogAttributeFactory->create();
        }
        $sortOrder = 999;

        $attributeData = [
            'frontend_label' => $label,
            'frontend_input' => 'text',
            'is_required' => 0,
            'attribute_code' => $attrCode,
            'is_global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'is_unique' => 0,
            'is_used_in_grid' => 0,
            'is_visible_in_grid' => 0,
            'is_filterable_in_grid' => 0,
            'is_filterable' => in_array($attrCode, Attribute::DEFAULT_FILTERABLE_ATTRIBUTES, true),
            'is_important' => 0,
            'is_searchable' => 0,
            'is_comparable' => 0,
            'is_used_for_promo_rules' => 0,
            'is_html_allowed_on_front' => 1,
            'is_visible_on_front' => 0,
            'used_in_product_listing' => 0,
            'source_model' => null,
            'backend_type' => 'varchar',
            'group' => $attributeGroup
            //'position' => (int) $data['sortkey'],
        ];

        switch ($type) {
            case Attribute::ATTRIBUTE_TYPE_BOOLEAN:
                $attributeData['frontend_input'] = 'select';
                $attributeData['source_model'] = \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class;
                $attributeData['backend_type'] = 'int';
                break;
        }

        // (multi)select attribute
        if ($isMultiSelect === true) {
            $attributeData['is_global'] = \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL;
            $attributeData['frontend_input'] = 'multiselect';
            $attributeData['backend_model'] = \Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend::class;
        }

        $attributeModel->addData($attributeData);
        $attributeModel->setEntityTypeId($entityId);
        $attributeModel->setIsUserDefined(1);
        try {
            $this->eavAttributeRepositoryInterface->save($attributeModel);

            //We need to assign the attribute to a group and set
            $this->eavAttributeManagement->assign(
                $entityTypeCode,
                $attributeSetId,
                $attributeGroupId,
                $attrCode,
                $sortOrder
            );

            if($isMultiSelect === true) {
                $this->manageAttributeOptions($attributeModel);
            }
            $id = $attributeModel->getId();

            return $id;
        } catch (\Magento\Framework\Exception\AlreadyExistsException $alreadyExistsException) {
            $this->logger->logMessage($alreadyExistsException);
        } catch (\Exception $exception) {
            $this->logger->logMessage($exception->getMessage());
        }
    }

    public function manageAttributeOptions(\Magento\Catalog\Api\Data\ProductAttributeInterface $attributeModel, $values = [])
    {
        $attributeCode = $attributeModel->getAttributeCode();
        $existingValues = $attributeModel->getOptions();

        $apiValues = [];

        if (empty($values)) {
            switch ($attributeCode) {
                case 'brands':
                    $apiValues = $this->connector->getBrands();
                    break;
                case 'organizations':
                    $apiValues = $this->connector->getOrganizations();
                    break;
            };
        } else {
            $apiValues = $values;
        }

        $this->updateAttributeOptions($attributeModel, $apiValues);
    }

    public function updateAttributeOptions(AttributeInterface $attribute, $apiData)
    {
        $eavSetup = $this->eavSetupFactory->create();

        $options = $attribute->getOptions();

        $existingOptions = [];
        foreach ($options as $option) {
            $existingOptions[$option->getValue()] = $option;
        }

        $updatedOptions = [];

        foreach ($apiData as $apiOption) {
            $option = [];
            $updatedLabels = [];

            foreach ($apiOption['name'] as $language => $storeLabel) {
                $storeIds = $this->attributeHelper->getStoreIdByMedipimLanguage($language);
                foreach ($storeIds as $storeId) {
                    $updatedLabels[$storeId] = $storeLabel;
                }
                $updatedLabels[0] = $apiOption['id'];
            }

            $optionId = $this->attributeHelper->getAttributeOptionValueId($attribute, $apiOption['id'])?: 0;

            if ($optionId > 0) {
                $updatedOptions[$optionId] = $updatedLabels;
            } else {
                $data = ['value' => [0 => $updatedLabels], 'attribute_id' => $attribute->getAttributeId()];
                $eavSetup->addAttributeOption($data);
            }
        }

        $data = ['value' => $updatedOptions, 'attribute_id' => $attribute->getAttributeId()];

        $eavSetup->addAttributeOption($data);

    }
}
