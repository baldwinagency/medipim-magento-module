<?php

namespace Baldwin\MedipimConnector\Model\Import\Supplier;

use Baldwin\MedipimConnector\Api\SupplierRepositoryInterface;
use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Model\SupplierFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Model\Import\Attribute\Importer as AttributeImporter;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface;

/**
 * Class Index
 * @package Baldwin\MedipimConnector\Controller\Adminhtml\Job
 */
class Importer
{
    //some suppliers we don't want to be shown.
    const SKIPPED_SUPPLIERS = ['1035'];

    /**
     * @var ConnectorInterface
     */
    private $connector;

    /**
     * @var SupplierRepositoryInterface
     */
    private $supplierRepository;

    /**
     * @var SupplierFactory
     */
    private $supplierFactory;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        ConnectorInterface $connector,
        SupplierRepositoryInterface $supplierRepository,
        SupplierFactory $supplierFactory,
        Logger $logger,
        AttributeImporter $attributeImporter,
        ProductAttributeRepositoryInterface $productAttributeRepository
    )
    {
        $this->connector = $connector;
        $this->supplierRepository = $supplierRepository;
        $this->supplierFactory = $supplierFactory;
        $this->logger = $logger;
        $this->attributeImporter = $attributeImporter;
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $this->connector->connect();

        $this->logger->addMessage("Start syncing suppliers");
        $suppliers = $this->connector->getOrganizations();

        if($suppliers !== null) {
            $this->insertSuppliers($suppliers);
            $countSuppliers = count($suppliers);

            $this->logger->addMessage("Finished syncing " . $countSuppliers . " suppliers");
        } else {
            $this->logger->addMessage("Could not fetch suppliers from API");
        }
    }

    /**
     * @param $suppliers
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function insertSuppliers($suppliers) {
        foreach ($suppliers as $supplier) {
            if (in_array($supplier['id'], self::SKIPPED_SUPPLIERS)) {
                continue;
            }

            try {
                $supplierModel = $this->supplierRepository->getById($supplier['id']);
            } catch(NoSuchEntityException $ex) {
                $supplierModel = $this->supplierFactory->create();
                $supplierModel->setId($supplier['id']);
            }

            try {
                $supplierModel->setName($supplier['name']['en']);
                $this->supplierRepository->save($supplierModel);
            } catch(\Exception $exception) {
                $this->logger->addMessage("Something went wrong while saving the suppliers");
                break;
            }
        }

        $supplierAttribute = $this->productAttributeRepository->get(AttributeInterface::ATTRIBUTE_ORGANIZATIONS);
        $this->attributeImporter->manageAttributeOptions($supplierAttribute, $suppliers);
    }
}
