<?php

namespace Baldwin\MedipimConnector\Model\Flag;

class AttributeMappingChanged extends \Magento\Framework\Flag
{
    protected $_flagCode = 'baldwin_medipimconnector_attribute_mapping_changed';
}
