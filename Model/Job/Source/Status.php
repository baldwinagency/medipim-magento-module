<?php

namespace Baldwin\MedipimConnector\Model\Job\Source;

use Magento\Framework\Data\OptionSourceInterface;

use Baldwin\MedipimConnector\Model\Job;
/**
 * Class Status
 */
class Status implements OptionSourceInterface
{
    /**
     * @var Job
     */
    protected $job;

    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->job->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
