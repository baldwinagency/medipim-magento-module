<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data\BrandInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Brand
 * @package Baldwin\MedipimConnector\Model
 */
class Brand extends AbstractModel implements BrandInterface
{
    const CACHE_TAG = 'medi_brand';

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Brand::class);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::BRAND_NAME);
    }

    /**
     * @param $name
     * @return \Baldwin\MedipimConnector\Api\Data\BrandInterface
     */
    public function setName(string $name)
    {
        return $this->setData(self::BRAND_NAME, $name);
    }
}
