<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\CategoryRepositoryInterface;
use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Model\ResourceModel\Category as ResourceCategory;
use Baldwin\MedipimConnector\Model\CategoryFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * Class CategoryRepository
 * @package Baldwin\MedipimConnector\Model
 */
class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @var ResourceCategory
     */
    protected $resource;

    /**
     * @var \Baldwin\MedipimConnector\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * CategoryRepository constructor.
     * @param ResourceCategory $resource
     * @param \Baldwin\MedipimConnector\Model\CategoryFactory $categoryFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceCategory $resource,
        CategoryFactory $categoryFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource                   = $resource;
        $this->categoryFactory            = $categoryFactory;
        $this->categoryCollectionFactory  = $categoryCollectionFactory;
        $this->collectionProcessor        = $collectionProcessor;
    }

    /**
     * Save Category.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\CategoryInterface $category
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\CategoryInterface $category)
    {
        try {
            $this->resource->save($category);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the category: %1', $exception->getMessage()),
                $exception
            );
        }
        return $category;
    }

    /**
     * Retrieve Category.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id)
    {
        $category = $this->categoryFactory->create();
        $category->load($id);
        if (!$category->getId()) {
            throw new NoSuchEntityException(__('The category with the "%1" ID doesn\'t exist.', $id));
        }
        return $category;
    }

    /**
     * Retrieve Category by Medipim ID.
     *
     * @param int $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByMedipimId($medipimId)
    {
        $category = $this->categoryFactory->create()
            ->getCollection()
            ->addFieldToFilter('medipim_id', $medipimId)
            ->getFirstItem();

        if (!$category->getId()) {
            throw new NoSuchEntityException(__('The medipim category with the "%1" Medipim ID doesn\'t exist.', $medipimId));
        }
        return $category;
    }

    /**
     * Retrieve Category Medipim Parent ID.
     *
     * @param int $parentId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByMedipimParentId($parentId)
    {
        $category = $this->categoryFactory->create()
            ->getCollection()
            ->addFieldToFilter('medipim_parent_id', $parentId)
            ->getFirstItem();

        if (!$category->getId()) {
            throw new NoSuchEntityException(__('The medipim category with the "%1" Medipim Parent ID doesn\'t exist.', $parentId));
        }

        return $category;
    }
}
