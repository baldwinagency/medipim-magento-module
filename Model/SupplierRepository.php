<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Api\SupplierRepositoryInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\Supplier as ResourceSupplier;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Baldwin\MedipimConnector\Model\SupplierFactory;

/**
 * Class SupplierRepository
 * @package Baldwin\MedipimConnector\Model
 */
class SupplierRepository implements SupplierRepositoryInterface
{
    /**
     * @var ResourceSupplier
     */
    protected $resource;

    /**
     * @var SupplierFactory
     */
    protected $supplierFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    public function __construct(
        ResourceSupplier $resource,
        SupplierFactory $supplierFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource                   = $resource;
        $this->supplierFactory               = $supplierFactory;
        $this->collectionProcessor        = $collectionProcessor;
    }

    /**
     * @param Data\SupplierInterface $supplier
     * @return Data\SupplierInterface
     * @throws CouldNotSaveException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\SupplierInterface $supplier)
    {
        try {
            $this->resource->save($supplier);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the supplier: %1', $exception->getMessage()),
                $exception
            );
        }
        return $supplier;
    }

    /**
     * @param int $id
     * @return Data\SupplierInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $supplier = $this->supplierFactory->create();
        $supplier->load($id);
        if (!$supplier->getId()) {
            throw new NoSuchEntityException(__('The supplier with the "%1" ID doesn\'t exist.', $id));
        }
        return $supplier;
    }
}
