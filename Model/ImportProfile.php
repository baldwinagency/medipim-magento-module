<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data\ImportProfileInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class ImportProfile
 * @package Baldwin\MedipimConnector\Model
 */
class ImportProfile extends AbstractModel implements ImportProfileInterface
{
    const CACHE_TAG = 'medi_profile';

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile::class);
    }

    /**
     * Get Status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Get Frequency
     * @return string|null
     */
    public function getFrequency()
    {
        return $this->getData(self::FREQUENCY);
    }

    /**
     * Get Start time
     * @return string|null
     */
    public function getStartTime()
    {
        return $this->getData(self::START_TIME);
    }

    /**
     * Get manual confirmation
     * @return boolean|null
     */
    public function getManualConfirmation()
    {
        return (bool) $this->getData(self::MANUAL_CONFIRMATION);
    }

    /**
     * Set status
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Set frequency
     * @param $frequency
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setFrequency($frequency)
    {
        return $this->setData(self::FREQUENCY, $frequency);
    }

    /**
     * Set start time
     * @param $startTime
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStartTime($startTime)
    {
        return $this->setData(self::START_TIME, $startTime);
    }

    /**
     * Set Manual confirmation
     * @param $confirmation
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setManualConfirmation($confirmation)
    {
        return $this->setData(self::MANUAL_CONFIRMATION, $confirmation);
    }

    /**
     * Get Identifier
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * Get Minium Content Required
     * @return string|null
     */
    public function getMinContentRequired()
    {
        return $this->getData(self::MINIMUM_CONTENT_REQUIERD);
    }

    /**
     * Get Brands
     * @return array
     */
    public function getBrandIds()
    {
        $brands = $this->getData(self::BRAND_IDS);
        if (is_string($brands)) {
            $brands =  explode(',', $brands);
        }

        return $brands;
    }

    /**
     * Get Suppliers
     * @return array
     */
    public function getSupplierIds()
    {
        $suppliers = $this->getData(self::SUPPLIER_IDS);
        if (is_string($suppliers)) {
            $suppliers =  explode(',', $suppliers);
        }

        return $suppliers;
    }

    /**
     * Get Categories
     * @return array
     */
    public function getCategoryIds()
    {
        $categories = $this->getData(self::CATEGORY_IDS);

        if (is_string($categories)) {
            $categories =  explode(',', $categories);
        }

        return $categories;
    }

    /**
     * Get Attachment
     * @return string|null
     */
    public function getAttachment()
    {
        return $this->getData(self::ATTACHMENT);
    }


    /**
     * Get Identifier
     * @param $identifier
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setIdentifier($identifier)
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }

    /**
     * Set Minium Content Required
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setMinContentRequired($miniumContentRequired)
    {
        return $this->setData(self::MINIMUM_CONTENT_REQUIERD, $miniumContentRequired);
    }

    /**
     * Set Brands
     * @param $brands
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setBrandIds($brands)
    {
        if (is_array($brands)) {
            $brands = implode(",", $brands);
        }

        return $this->setData(self::BRAND_IDS, $brands);
    }

    /**
     * Set Suppliers
     * @param $suppliers
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setSupplierIds($suppliers)
    {
        if (is_array($suppliers)) {
            $suppliers = implode(",", $suppliers);
        }

        return $this->setData(self::SUPPLIER_IDS, $suppliers);
    }

    /**
     * Set Categories
     * @param $categories
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setCategoryIds($categories)
    {
        if (is_array($categories)) {
            $categories = implode(",", $categories);
        }

        return $this->setData(self::CATEGORY_IDS, $categories);
    }

    /**
     * Set Attachment
     * @param $attachment
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setAttachment($attachment)
    {
        return $this->setData(self::ATTACHMENT, $attachment);
    }

    /**
     * Get Stores
     * @return array
     */
    public function getStoreIds(): array
    {
        $stores = $this->getData(self::STORE_IDS);
        if (is_string($stores)) {
            $stores = explode(',', $stores);
        }
        return $stores;
    }

    /**
     * Set Stores
     * @param $stores
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStoreIds($stores)
    {
        if (is_array($stores)) {
            $stores = implode(",", $stores);
        }

        return $this->setData(self::ATTACHMENT, $stores);
    }

    /**
     * Get the timestamp of the last time this import profile ran
     * @return mixed
     */
    public function getUpdatedSince()
    {
        return $this->getData(self::UPDATED_SINCE);
    }

    /**
     * Set the timestamp of the last time this import profile ran
     * @param $updatedSince
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setUpdatedSince($updatedSince)
    {
        return $this->setData(self::UPDATED_SINCE, $updatedSince);
    }

    /**
     * Returns the profile data in the correct format for the API
     * @return array
     */
    public function getParsedProfileData(): array
    {
        $data = [
            'filter' => [
                'and' => [
                    ['minimumContent' => (bool)(int)$this->getMinContentRequired()],
                    ['status' => 'active'],
                    ['prescription' => false],
                    ['not' =>
                        ['apbCategory' => ["material","unknown","pesticides_agricultural","biocide","reactive"]]
                    ]
                ]
            ]
        ];

        if ($this->getUpdatedSince() !== null) {
            $data['filter']['and'][] = ['updatedSince' => strtotime($this->getUpdatedSince())]; // we need the unix timestamp
        }

        if ($this->getSupplierIds() && count($this->getSupplierIds()) > 0) {
            $data['filter']['and'][] = ['organization' => $this->getSupplierIds()];
        }

        if ($this->getBrandIds() !== null && count($this->getBrandIds()) > 0) {
            $data['filter']['and'][] = ['brand' => $this->getBrandIds()];
        }

        if ($this->getCategoryIds() && count($this->getCategoryIds()) > 0) {
            $data['filter']['and'][] = ['publicCategory' => $this->getCategoryIds()];
        }

        return $data;
    }

}
