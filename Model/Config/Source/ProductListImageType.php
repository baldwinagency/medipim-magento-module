<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

/**
 * Class Environment
 * @package Baldwin\MedipimConnector\Model\Config\Source
 */
class ProductListImageType implements \Magento\Framework\Option\ArrayInterface
{
    const PRODUCT_LIST_IMAGE_FRONTAL      = 'frontals';
    const PRODUCT_LIST_IMAGE_PACKAGE_SHOT = 'packshots';
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::PRODUCT_LIST_IMAGE_PACKAGE_SHOT, 'label' => __('Frontal Image')],
            ['value' => self::PRODUCT_LIST_IMAGE_FRONTAL, 'label' => __('Packshot Image')]
        ];
    }
}
