<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;

/**
 * Class MagentoAttributes
 * @package Baldwin\MedipimConnector\Model\Config\Source
 */
class MagentoAttributes implements \Magento\Framework\Option\ArrayInterface
{
    private $productAttributeRepository;
    private $criteriaBuilder;
    const CREATE_NEW_ATTRIBUTE_VALUE = 'create_new';
    public function __construct(
        ProductAttributeRepositoryInterface $productAttributeRepository,
        SearchCriteriaBuilder $criteriaBuilder
    )
    {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    public function toOptionArray()
    {
        $attributeArray = [];
        $attributes = $this->productAttributeRepository->getList($this->criteriaBuilder->create())->getItems();

        $attributeArray[] = [
            'value' => self::CREATE_NEW_ATTRIBUTE_VALUE,
            'label' => 'Create New'
        ];

        foreach ($attributes as $attribute) {

            $labels = $attribute->getFrontendLabels();
            $label = reset($labels);

            $attributeArray[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getAttributeCode() //$label->getLabel()
            ];
        }
        return $attributeArray;
    }
}
