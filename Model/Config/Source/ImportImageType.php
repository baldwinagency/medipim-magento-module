<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

/**
 * Class Environment
 * @package Baldwin\MedipimConnector\Model\Config\Source
 */
class ImportImageType implements \Magento\Framework\Option\ArrayInterface
{
    const IMPORT_TYPE_CDN  = 'import_cdn';
    const IMPORT_TYPE_HARD = 'import_hard';
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::IMPORT_TYPE_CDN, 'label' => __('Use Cdn')],
            ['value' => self::IMPORT_TYPE_HARD, 'label' => __('Hard Image Import')]
        ];
    }
}
