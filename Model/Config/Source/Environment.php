<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

/**
 * Class Environment
 * @package Baldwin\MedipimConnector\Model\Config\Source
 */
class Environment implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'staging', 'label' => __('Staging')],
            ['value' => 'production', 'label' => __('Production')]
        ];
    }
}
