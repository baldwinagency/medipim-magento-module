<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface;

class MedipimAttributes implements \Magento\Framework\Option\ArrayInterface
{
    protected $connector;
    private $attributeHelper;

    public function __construct(
        \Baldwin\MedipimConnector\Model\ConnectorInterface $connector,
        \Baldwin\MedipimConnector\Helper\Import\Attribute $attributeHelper
    )
    {
        $this->connector = $connector;
        $this->attributeHelper = $attributeHelper;
    }

    public function toOptionArray()
    {
        return $this->fetch();
    }

    private function fetch()
    {
        try {
            $this->connector->connect();
            $attributesArray = [];
            $attributes = $this->connector->getProductAttributes();

            foreach ($attributes as $attribute) {
                $code = $attribute[AttributeInterface::MEDIPIM_API_ATTRIBUTE_ID];
                $attributesArray[] = [
                    'value' => $code,
                    'label' => $this->attributeHelper->camelCaseToString($code)
                ];
            }

        } catch(\Exception $exception) {
            $attributesArray[] = [
                'value' => 'api error',
                'label' => "Api Error: " . $exception->getMessage()
            ];
        }
        return $attributesArray;

    }
}
