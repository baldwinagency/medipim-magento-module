<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

/**
 * Class Environment
 * @package Baldwin\MedipimConnector\Model\Config\Source
 */
class FrontalImageType implements \Magento\Framework\Option\ArrayInterface
{
    const FRONTAL_TYPE_ORIGINAL_PNG   = 'original';
    const FRONTAL_TYPE_MEDIPUM_PNG    = 'medium';
    const FRONTAL_TYPE_ORIGINAL_JPEG  = 'originalJpeg';
    const FRONTAL_TYPE_MEDIPUM_JPEG   = 'mediumJpeg';

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::FRONTAL_TYPE_ORIGINAL_PNG, 'label' => __('Original .PNG')],
            ['value' => self::FRONTAL_TYPE_ORIGINAL_JPEG, 'label' => __('Medium .PNG')],
            ['value' => self::FRONTAL_TYPE_MEDIPUM_PNG, 'label' => __('Medium .PNG (450x450)')],
            ['value' => self::FRONTAL_TYPE_MEDIPUM_JPEG, 'label' => __('Medium .JPEG (450x450)')]
        ];
    }
}
