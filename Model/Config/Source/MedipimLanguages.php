<?php

namespace Baldwin\MedipimConnector\Model\Config\Source;

class MedipimLanguages implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'nl', 'label' => 'nl'],
            ['value' => 'fr', 'label' => 'fr'],
            ['value' => 'en', 'label' => 'en'],
        ];
    }
}
