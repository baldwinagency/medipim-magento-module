<?php

namespace Baldwin\MedipimConnector\Model;

/**
 * Interface ConnectorInterface
 * @package Baldwin\MedipimConnector\Model
 */
interface ConnectorInterface
{
    /**
     * @return void
     */
    public function connect();

    /**
     * @return mixed
     */
    public function getProductAttributes();

    /**
     * @param $query
     * @return mixed
     */
    public function searchProducts($query);

    /**
     * @param $cnk
     * @return mixed
     */
    public function getProductByCnk($cnk);

    /**
     * @param $id
     * @return mixed
     */
    public function getProductById($id);

    /**
     * @return mixed
     */
    public function getPublicCategories();

    /**
     * @return mixed
     */
    public function getOrganizations();

    /**
     * @return mixed
     */
    public function getBrands();

    /**
     * @param array $query
     * @return mixed
     */
    public function getProductsExport(array $query, callable $callback);
}
