<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data;
use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\Job as ResourceJob;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Baldwin\MedipimConnector\Model\JobFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Baldwin\MedipimConnector\Api\BatchRepositoryInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\Job\CollectionFactory as JobCollectionFactory;

/**
 * Class JobRepository
 * @package Baldwin\MedipimConnector\Model
 */
class JobRepository implements JobRepositoryInterface
{
    /**
     * @var ResourceJob
     */
    protected $resource;

    /**
     * @var \Baldwin\MedipimConnector\Model\JobFactory
     */
    protected $jobFactory;

    /**
     * @var JobCollectionFactory
     */
    protected $jobCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    private $dateTime;

    private $batchRepository;

    /**
     * JobRepository constructor.
     * @param ResourceJob $resource
     * @param \Baldwin\MedipimConnector\Model\JobFactory $jobFactory
     * @param JobCollectionFactory $jobCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceJob $resource,
        JobFactory $jobFactory,
        JobCollectionFactory $jobCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        DateTime $dateTime,
        BatchRepositoryInterface $batchRepository
    ) {
        $this->resource                          = $resource;
        $this->jobCollectionFactory              = $jobCollectionFactory;
        $this->jobFactory                        = $jobFactory;
        $this->collectionProcessor               = $collectionProcessor;
        $this->dateTime                          = $dateTime;
        $this->batchRepository                   = $batchRepository;
    }

    /**
     * @param JobInterface $job
     * @return JobInterface
     * @throws CouldNotSaveException
     */
    public function save(JobInterface $job)
    {
        try {
            $this->resource->save($job);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the job: %1', $exception->getMessage()),
                $exception
            );
        }
        return $job;
    }

    /**
     * @param int $id
     * @return Data\BatchInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $job = $this->jobFactory->create();
        $job->load($id);
        if (!$job->getId()) {
            throw new NoSuchEntityException(__('The job with the "%1" ID doesn\'t exist.', $id));
        }
        return $job;
    }

    /**
     * @param int $batchGroupId
     * @return ImportProfile
     * @throws NoSuchEntityException
     */
    public function getByBatchGroupId($batchGroupId)
    {
        $job = $this->jobCollectionFactory
            ->create()
            ->addFieldToFilter('batch_group_id', $batchGroupId)
            ->getFirstItem();

        if (!$job->getId()) {
            throw new NoSuchEntityException(__('The job with the Batch group id "%1" doesn\'t exist.', $batchGroupId));
        }

        return $job;
    }


    public function delete(JobInterface $job)
    {
        try {
            if ($job->getBatchGroupId() !== null) {
                $this->batchRepository->deleteByBatchGroupId($job->getBatchGroupId());
            }

            $this->resource->delete($job);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not delete the job: %1', $exception->getMessage()),
                $exception
            );
        }
        return $job;
    }

    public function reschedule(JobInterface $job)
    {
        try {
            $createdAtTime = $this->dateTime->gmtTimestamp();
            $date = new \DateTime();
            $date->setTimestamp($createdAtTime);
            $datetimeFormat = 'Y-m-d H:i:s';

            $scheduledAtTime = $createdAtTime;

            if ($job->getBatchGroupId() !== null) {
                $this->batchRepository->deleteByBatchGroupId($job->getBatchGroupId());
            }

            /* @var Job $job */
            $job->setScheduledAt($date->format($datetimeFormat))
                ->setExecutedAt(null)
                ->setFinishedAt(null)
                ->setCreatedAt($date->format($datetimeFormat))
                ->setStatus(Job::STATUS_PENDING)
                ->setIsPublished(false);

            $this->save($job);

            return $job->getId();

        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not reschedule job: %1', $exception->getMessage()),
                $exception
            );
        }
    }
}
