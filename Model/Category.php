<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Helper\Import\AbstractHelper;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel;
use Magento\Framework\Model\AbstractModel;
use Baldwin\MedipimConnector\Api\Data\CategoryInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Registry;
use Baldwin\MedipimConnector\Model\ConfigInterface;
use Baldwin\MedipimConnector\Model\Connector\CategoryInterface as MedipimCategoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class Category
 * @package Baldwin\MedipimConnector\Model
 */
class Category extends AbstractModel implements CategoryInterface, IdentityInterface, MedipimCategoryInterface
{
    const CACHE_TAG = 'medi_cat';
    const TIME_FORMAT = 'Y-m-d h:i:s';
    /**
     * @var \Baldwin\MedipimConnector\Model\ConfigInterface
     */
    private $medipimConfig;

    /**
     * @var DateTime
     */
    private $dateTime;

    public function __construct(
        Context $context,
        Registry $registry,
        ResourceModel\AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        ConfigInterface $medipimConfig,
        DateTime $dateTime,
        array $data = []
    )
    {
        $this->medipimConfig = $medipimConfig;
        $this->dateTime = $dateTime;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Category::class);
    }

    /**
     * Get Id
     * @return string|null
     */
    public function getId()
    {
        return parent::getData(self::ID);
    }

    /**
     * Get Medipim Category Id
     * @return string|null
     */
    public function getMedipimId()
    {
        return $this->getData(self::MEDIPIM_ID);
    }

    /**
     * Get Medipim Parent Category Id
     * @return string|null
     */
    public function getMedipimParentId()
    {
        return $this->getData(self::MEDIPIM_PARENT_ID);
    }

    /**
     * Get Medipim Parent Category Id
     * @return string|null
     */
    public function getMagentoCategoryId()
    {
        return $this->getData(self::MAGENTO_CATEGORY_ID);
    }

    /**
     * Get Created At
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get Updated At
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * We fetch the language that is predefined in the Configuration.
     * @return null|string
     */
    public function getName($medipimLang = AbstractHelper::DEFAULT_MEDIPIM_LANGUAGE)
    {
        $names = json_decode($this->getData(self::NAME), true);

        $result = null;
        if (is_array($names)) {
            if (array_key_exists($medipimLang, $names)) {
                $result = $names[$medipimLang];
            }
        }

        return $result;
    }

    /**
     * Get Category Level
     * @return string|null
     */
    public function getLevel()
    {
        return $this->getData(self::LEVEL);
    }

    /**
     * Get Category Path
     * @return string|null
     */
    public function getPath()
    {
        return $this->getData(self::PATH);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set Name
     * @param $name
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set Path
     * @param $path
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setPath($path)
    {
        return $this->setData(self::PATH, $path);

    }

    /**
     * Set Medipim Id
     * @param $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimId($medipimId)
    {
        return $this->setData(self::MEDIPIM_ID, $medipimId);
    }

    /**
     * Set Medipim Parent Category Id
     * @param $parentId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimParentId($parentId)
    {
        return $this->setData(self::MEDIPIM_PARENT_ID, $parentId);
    }

    /**
     * Set Medipim Parent Category Id
     * @param $categoryId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMagentoCategoryId($categoryId)
    {
        return $this->setData(self::MAGENTO_CATEGORY_ID, $categoryId);
    }

    /**
     * Set Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setCreatedAt($createdAt)
    {
        if (is_numeric($createdAt)) {
            $createdAt = $this->dateTime->date(self::TIME_FORMAT, $createdAt);
        }

        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        if (is_numeric($updatedAt)) {
            $updatedAt = $this->dateTime->date(self::TIME_FORMAT, $updatedAt);
        }

        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Set Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimCreatedAt($createdAt)
    {
        return $this->setData(self::MEDIPIM_CREATED_AT, $createdAt);
    }

    /**
     * Set Medipim Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimUpdatedAt($updatedAt)
    {
        return $this->setData(self::MEDIPIM_UPDATED_AT, $updatedAt);
    }

    /**
     * set Category Level
     * @param $level
     * @return CategoryInterface|Category
     */
    public function setLevel($level)
    {
        return $this->setData(self::LEVEL, $level);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Set Data from Medipim Api
     * @param $data
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setDataFromApi($data)
    {
        if (array_key_exists(self::MEDIPIM_API_CATEGORY_ID, $data)) {
            $this->setMedipimId($data[self::MEDIPIM_API_CATEGORY_ID]);
        }

        //We save the name as JSON data since we will have multiple languages for the name
        if (array_key_exists(self::MEDIPIM_API_CATEGORY_NAME, $data)) {
            $name = $data[self::MEDIPIM_API_CATEGORY_NAME];
            if (is_array($name)) {
                $name = json_encode($name);
            }

            $this->setName($name);
        }

        if (array_key_exists(self::MEDIPIM_API_CATEGORY_PARENT, $data)) {
            $this->setMedipimParentId($data[self::MEDIPIM_API_CATEGORY_PARENT]);
        }

        if (array_key_exists(self::MEDIPIM_API_CATEGORY_PARENT, $data)) {
            $this->setMedipimParentId($data[self::MEDIPIM_API_CATEGORY_PARENT]);
        }

        if (array_key_exists(self::MEDIPIM_API_META, $data)) {
            $metaData = $data[self::MEDIPIM_API_META];
            $this->setMedipimCreatedAt(
                $this->dateTime->date(self::TIME_FORMAT, $metaData[self::MEDIPIM_API_CATEGORY_CREATED_AT])
            );
            $this->setMedipimUpdatedAt(
                $this->dateTime->date(self::TIME_FORMAT, $metaData[self::MEDIPIM_API_CATEGORY_UPDATED_AT])
            );
        }

        $this->setCreatedAt($this->dateTime->date(self::TIME_FORMAT));
        $this->setUpdatedAt($this->dateTime->date(self::TIME_FORMAT));
    }

    /**
     * Get if category is synced
     * @return string|null
     */
    public function getIsSynced()
    {
        return $this->getData(self::IS_SYNCED);
    }

    /**
     * Set is synced
     * @param $isSynced
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setIsSynced($isSynced)
    {
        return $this->setData(self::IS_SYNCED, $isSynced);
    }
}
