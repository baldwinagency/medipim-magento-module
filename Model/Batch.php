<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Api\Data\BatchInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Batch extends AbstractModel implements BatchInterface, IdentityInterface
{
    const BATCH_TYPE_PRODUCT  = 'product';
    const BATCH_TYPE_CATEGORY = 'category';
    const CACHE_TAG = 'medi_batch';

    protected function _construct()
    {
        $this->_init(\Baldwin\MedipimConnector\Model\ResourceModel\Batch::class);
    }

    /**
     * Get batch data
     * @return string|null
     */
    public function getBatchData()
    {
        return $this->getData(self::BATCH_DATA);
    }

    /**
     * Get sync error
     * @return string|null
     */
    public function getSyncError()
    {
        return $this->getData(self::SYNC_ERROR);
    }

    /**
     * Get sync started
     * @return string|null
     */
    public function getSyncStarted()
    {
        return $this->getData(self::SYNC_STARTED);
    }

    /**
     * Get sync finished
     * @return string|null
     */
    public function getSyncFinished()
    {
        return $this->getData(self::SYNC_FINISHED);
    }

    /**
     * Get sync status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->getData(self::SYNC_STATUS);
    }

    public function getEntities(): int
    {
        return $this->getData(self::ENTITES);
    }

    /**
     * @param $entities
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setEntities(int $entities)
    {
        return $this->setData(self::ENTITES, $entities);
    }

    /**
     * @param $batchData
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setBatchData($batchData)
    {
        return $this->setData(self::BATCH_DATA, $batchData);
    }

    /**
     * @param $syncError
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncError($syncError)
    {
        return $this->setData(self::SYNC_ERROR, $syncError);
    }

    /**
     * @param $syncStarted
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncStarted($syncStarted)
    {
        return $this->setData(self::SYNC_STARTED, $syncStarted);
    }

    /**
     * @param $syncFinished
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncFinished($syncFinished)
    {
        return $this->setData(self::SYNC_FINISHED, $syncFinished);
    }

    /**
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::SYNC_STATUS, $status);
    }

    public function clearSyncTimestamps()
    {
        $this->setSyncStarted(null);
        $this->setSyncFinished(null);
    }

    /**
     * Get Type
     * @return mixed
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @param $type
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get batch group id
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function getBatchGroupId()
    {
        return $this->getData(self::BATCH_GROUP_ID);
    }

    /**
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setBatchGroupId($groupId)
    {
        return $this->setData(self::BATCH_GROUP_ID, $groupId);
    }
}
