<?php

namespace Baldwin\MedipimConnector\Model\Adminhtml\System\Message;


use Magento\Framework\Notification\MessageInterface;
use Baldwin\MedipimConnector\Model\Flag\AttributeMappingChanged;

/**
 * Class AttributeSyncMessage
 * @package Baldwin\MedipimConnector\Model\Adminhtml\System\Message
 */
class AttributeSyncMessage implements MessageInterface
{
    const MESSAGE_IDENTITY = 'baldwin_medipimconnector_system_message';

    private $attributeMappingChanged;

    public function __construct(
        AttributeMappingChanged $attributeMappingChanged
    )
    {
        $this->attributeMappingChanged = $attributeMappingChanged->loadSelf();
    }

    /**
     * Retrieve unique system message identity
     *
     * @return string
     */
    public function getIdentity()
    {
        return self::MESSAGE_IDENTITY;
    }

    /**
     * Check whether the system message should be shown
     *
     * @return bool
     */
    public function isDisplayed()
    {
        // The message will be shown
        if((int)$this->attributeMappingChanged->getFlagData() === 1) {
            return true;
        }
        return false;
    }

    /**
     * Retrieve system message text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getText()
    {
        return __('Medipim Attribute Mapping has been changed. The values will only change when a new import is triggered');
    }

    public function getSeverity()
    {
        return self::SEVERITY_MAJOR;
    }

}
