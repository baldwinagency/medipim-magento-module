<?php

namespace Baldwin\MedipimConnector\Model\Adminhtml\System\Config;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface;
use Baldwin\MedipimConnector\Model\Config\Source\MagentoAttributes;
use Magento\Eav\Api\AttributeRepositoryInterface as EavAttributeRepositoryInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Catalog\Model\Product;
use Baldwin\MedipimConnector\Model\Flag\AttributeMappingChanged;

class AttributesManager  extends Value
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    /**
     * @var \Baldwin\MedipimConnector\Model\Config\Source\MedipimAttributes
     */
    private $medipimAttributes;

    /**
     * @var
     */
    private $attributeCollectionFactory;

    private $magentoAttributeRepository;

    private $medipimAttributeRepository;

    private $attributeMappingChanged;

    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        ?AbstractResource $resource = null,
        ?AbstractDb $resourceCollection = null,
        \Magento\Framework\Serialize\Serializer\Json $serializer,
        \Baldwin\MedipimConnector\Model\Config\Source\MedipimAttributes $medipimAttributes,
        \Baldwin\MedipimConnector\Model\ResourceModel\Attribute\CollectionFactory $attributeCollectionFactory,
        EavAttributeRepositoryInterface $magentoAttributeRepository,
        \Baldwin\MedipimConnector\Api\AttributeRepositoryInterface $medipimAttributeRepository,
        EavConfig $eavConfig,
        AttributeMappingChanged $attributeMappingChanged,
        array $data = []
    )
    {
        $this->serializer                 = $serializer;
        $this->medipimAttributes          = $medipimAttributes;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->magentoAttributeRepository = $magentoAttributeRepository;
        $this->productEntityType          = $eavConfig->getEntityType(Product::ENTITY);
        $this->medipimAttributeRepository = $medipimAttributeRepository;
        $this->attributeMappingChanged    = $attributeMappingChanged;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function beforeSave()
    {
        $data = $this->getValue();

        $this->attributeMappingChanged = $this->attributeMappingChanged->loadSelf();

        $attributeCollection = $this->attributeCollectionFactory->create()
            ->addAttributeToSelect("*");


        $result = [];

        if (!is_array($data)) {
            try {
                $data = $this->serializer->unserialize($data);
            } catch (\InvalidArgumentException $e) {
                $data = [];
            }
        }

        $setFlag = false;

        foreach ($data as $attribute) {
            if (is_array($attribute)) {
                if (array_key_exists("magento_attribute", $attribute)) {
                    $magentoAttrCode = $attribute['magento_attribute'];
                    $medipimAttr = $attribute['medipim_attribute'];

                    $medipimModel = $attributeCollection->getItemByColumnValue("medipim_id", $medipimAttr);

                    $magentoModelAttrId = $this->magentoAttributeRepository->get(
                        $this->productEntityType->getId(),
                        $magentoAttrCode
                    )->getAttributeId();

                    $medipimModel->setData("attribute_id", $magentoModelAttrId);

                    $this->medipimAttributeRepository->save($medipimModel);
                    $setFlag = true;
                }
            }
        }

        if ($setFlag === true) {
            $this->attributeMappingChanged->setFlagData(1)->save();
        }
        $this->setValue(null);

        return $this;
    }

    /**
     * Process data after load
     *
     * @return $this
     */
    public function afterLoad()
    {
        $values = [];
        $medipimAttr = $this->medipimAttributes->toOptionArray();

        $attributeCollection = $this->attributeCollectionFactory->create()
            ->addAttributeToSelect("medipim_id")
            ->addAttributeToSelect("attribute_id");

        foreach ($medipimAttr as $attribute) {
            $key = $attribute['value'];

            if (in_array($key, AttributeInterface::SKIP_ATTRIBUTES_IN_MAPPING)) {
                continue;
            }

            $attribute = $attributeCollection->getItemByColumnValue("medipim_id", $key);

            //the attribute could not be found
            if($attribute === null) {
                continue;
            }

            try {
                $attributeId = $attribute->getData("attribute_id");
                $magentoAttr = $this->magentoAttributeRepository->get(
                    $this->productEntityType->getId(),
                    $attributeId
                )->getAttributeCode();
                $values[] = ['medipim_attribute' => $key, 'magento_attribute' => $magentoAttr];

            } catch(\Magento\Framework\Exception\NoSuchEntityException $exception) {
                $values[] = ['medipim_attribute' => $key, 'magento_attribute' => MagentoAttributes::CREATE_NEW_ATTRIBUTE_VALUE];

            }
        }

        $this->setValue($values);

        return $this;
    }
}
