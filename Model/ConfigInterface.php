<?php

namespace Baldwin\MedipimConnector\Model;

/**
 * Interface ConfigInterface
 * @package Baldwin\MedipimConnector\Model
 */
interface ConfigInterface
{
    /* Api staging Environment */
    const API_ENVIRONMENT_STAGING = 'staging';

    /* Api production Environment */
    const API_ENVIRONMENT_PRODUCTION = 'production';

    /**
     * Get api.client
     *
     * @return string
     */
    public function getApiClient();

    /**
     * Get api.secret
     *
     * @return string
     */
    public function getApiSecret();

    /**
     * Get api.environment
     * @return string
     */
    public function getEnvironment();

    /**
     * Get prefered language
     * @return string
     */
    public function getPreferredLanguage();

    /**
     * Get product import batch size
     * @return mixed
     */
    public function getBatchSize();

    /**
     * get product import maximum failed batches in a single import
     * @return mixed
     */
    public function getMaxBatchFails();

    /**
     * get product import maximum executed batches in a single import
     * @return mixed
     */
    public function getMaxBatchExecuted();

    /**
     * Should import images
     * @return mixed
     */
    public function importImages();

    /**
     * Use Hard image import or Cdn
     * @return mixed
     */
    public function getImportImageType();

    /**
     * Specify frontal image size/type
     * @return mixed
     */
    public function getFrontalImageType();

    /**
     * Specify productl ist image type
     * @return mixed
     */
    public function getProductListImageType();

}
