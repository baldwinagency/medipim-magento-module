<?php

namespace Baldwin\MedipimConnector\Model;

use Baldwin\MedipimConnector\Helper\Logger;
use Baldwin\MedipimConnector\Api\BatchProcessorInterface;
use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Baldwin\MedipimConnector\Api\JobProcessorInterface;
use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Baldwin\MedipimConnector\Model\Import\Product\Syncer;
use Baldwin\MedipimConnector\Model\ImportProfile;
use Baldwin\MedipimConnector\Model\Job;
use Baldwin\MedipimConnector\Model\ResourceModel\Job\Collection;
use Baldwin\MedipimConnector\Model\Import\Product\ImporterFactory;
use Baldwin\MedipimConnector\Model\Import\Product\SyncerFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class JobProcessor
 * @package Baldwin\MedipimConnector\Model
 */
class JobProcessor implements JobProcessorInterface
{
    const FIRST_DAY_OF_WEEK = 1;
    const FIRST_DAY_OF_MONTH = 1;

    private $importerFactory;

    private $productSyncerFactory;

    private $jobRepository;

    private $importProfileRepository;

    private $logger;

    private $batchProcessor;

    private $dateTime;

    public function __construct(
        ImporterFactory $importerFactory,
        SyncerFactory $productSyncerFactory,
        JobRepositoryInterface $jobRepository,
        ImportProfileRepositoryInterface $importProfileRepository,
        BatchProcessorInterface $batchProcessor,
        Logger $logger,
        DateTime $dateTime
    ) {
        $this->importerFactory          = $importerFactory;
        $this->productSyncerFactory     = $productSyncerFactory;
        $this->jobRepository            = $jobRepository;
        $this->logger                   = $logger;
        $this->importProfileRepository  = $importProfileRepository;
        $this->batchProcessor           = $batchProcessor;
        $this->dateTime                  = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function executeJobSync(JobInterface $job): bool
    {
        $executedTime   = $this->dateTime->gmtTimestamp();

        //First we set the executed time and status of the job
        $job->setStatus(Job::STATUS_SYNCING);
        $job->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', $executedTime));

        try {
            $this->jobRepository->save($job);
        } catch (\Exception $ex) {
            $this->logger->addCritical($ex);
        }

        //now the real work
        try {
            $batchGroupId = $this->generateBatchGroupId();
            $job->setBatchGroupId($batchGroupId);
            $this->jobRepository->save($job);

            $result = $this->fetchData($job, $batchGroupId);

            if ($result === true) {
                $job->setStatus(Job::STATUS_READY_FOR_IMPORT);
                $job->setIsPublished(Job::STATUS_NOT_PUBLISHED);
            } else {
                $finishedTime = $this->dateTime->gmtTimestamp();
                $job->setStatus(Job::STATUS_NOTHING_TO_IMPORT);
                $job->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', $finishedTime));
            }
            $this->jobRepository->save($job);

            return true;
        } catch (\Exception $exception) {
            $job->setStatus(Job::STATUS_ERROR);
            $this->jobRepository->save($job);
            $this->logger->addMessage($exception);
            return false;
        }

    }

    private function generateBatchGroupId(): string
    {
        return bin2hex(random_bytes(8));
    }
    /**
     * {@inheritdoc}
     */
    public function executeJobImport(JobInterface $job)
    {
        $this->batchProcessor->publishAllBatches([Batch::SYNC_STATUS_PENDING], $job);
    }

    /**
     * @param \Baldwin\MedipimConnector\Model\Job $job
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function fetchData(JobInterface $job, string $batchGroupId): bool {
        /** @var ImportProfile $profile */
        $importProfile = $this->importProfileRepository->getById($job->getProfileId());
        $profileData = $importProfile->getParsedProfileData();

        /** @var Syncer $syncer */
        $syncer = $this->productSyncerFactory->create();

        $result = $syncer->fetchFromApi($profileData, $batchGroupId);
        $endTime = $this->dateTime->gmtTimestamp();
        $importProfile->setUpdatedSince(strftime('%Y-%m-%d %H:%M:%S', $endTime));
        $this->importProfileRepository->save($importProfile);

        return $result;
    }
}
