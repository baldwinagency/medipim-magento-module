<?php

namespace Baldwin\MedipimConnector\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface AttributeSearchResultsInterface
 * @package Baldwin\MedipimConnector\Api\Data
 */
interface AttributeSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\AttributeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
