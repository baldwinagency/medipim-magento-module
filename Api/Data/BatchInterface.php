<?php

namespace Baldwin\MedipimConnector\Api\Data;

/**
 * Interface BatchInterface
 * @package Baldwin\MedipimConnector\Api\Data
 */
interface BatchInterface
{
    const ID                = 'id';
    const TYPE              = 'type';
    const BATCH_DATA        = 'batch_data';
    const SYNC_ERROR        = 'sync_error';
    const SYNC_STARTED      = 'sync_started';
    const SYNC_FINISHED     = 'sync_finished';
    const SYNC_STATUS       = 'status';
    const BATCH_GROUP_ID    = 'batch_group_id';
    const ENTITES           = 'entities';

    const SYNC_STATUS_PENDING    = 'pending';
    const SYNC_STATUS_PROCESSING = 'processing';
    const SYNC_STATUS_SUCCESS    = 'success';
    const SYNC_STATUS_ERROR      = 'error';
    /**
     * Get Id
     * @return string|null
     */
    public function getId();

    /**
     * Get Type
     * @return mixed
     */
    public function getType();

    /**
     * Get batch data
     * @return string|null
     */
    public function getBatchData();

    /**
     * Get sync error
     * @return string|null
     */
    public function getSyncError();

    /**
     * Get sync started
     * @return string|null
     */
    public function getSyncStarted();

    /**
     * Get sync finished
     * @return string|null
     */
    public function getSyncFinished();

    /**
     * Get number of entities
     * @return int
     */
    public function getEntities(): int;

    /**
     * @param int $entities
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setEntities(int $entities);

    /**
     * Get status
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function getStatus();

    /**
     * Get batch group id
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function getBatchGroupId();

    /**
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setId($id);

    /**
     * @param $type
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setType($type);

    /**
     * @param $batchData
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setBatchData($batchData);

    /**
     * @param $syncError
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncError($syncError);

    /**
     * @param $syncStarted
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncStarted($syncStarted);

    /**
     * @param $syncFinished
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setSyncFinished($syncFinished);

    /**
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setStatus($status);

    /**
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     */
    public function setBatchGroupId($groupId);

    public function clearSyncTimestamps();
}
