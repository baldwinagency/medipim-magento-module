<?php

namespace Baldwin\MedipimConnector\Api\Data;

/**
 * Medipim Category Model
 * @package Baldwin\MedipimConnector\Api\Data
 */
interface CategoryInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                    = 'entity_id';
    const MEDIPIM_ID            = 'medipim_id';
    const MEDIPIM_PARENT_ID     = 'medipim_parent_id';
    const NAME                  = 'name';
    const IS_SYNCED             = 'is_synced';
    const CREATED_AT            = 'created_at';
    const UPDATED_AT            = 'updated_at';
    const MEDIPIM_CREATED_AT    = 'api_created_at';
    const MEDIPIM_UPDATED_AT    = 'api_updated_at';
    const LEVEL                 = 'level';
    const PATH                  = 'path';
    const MAGENTO_CATEGORY_ID   = 'catalog_category_entity_id';

    /**
     * Get Id
     * @return string|null
     */
    public function getId();

    /**
     * Get Medipim Category Id
     * @return string|null
     */
    public function getMedipimId();

    /**
     * Get Medipim Parent Category Id
     * @return string|null
     */
    public function getMedipimParentId();

    /**
     * Get Category Name
     * @return string|null
     */
    public function getName();

    /**
     * Get if category is synced
     * @return string|null
     */
    public function getIsSynced();

    /**
     * Get Created At
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get Updated At
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Get level
     * @return string|null
     */
    public function getLevel();

    /**
     * Get level
     * @return string|null
     */
    public function getPath();

    /**
     * Get Magento category ID
     * @return string|null
     */
    public function getMagentoCategoryId();

    /**
     * Set id
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setId($id);

    /**
     * Set name
     * @param $name
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setName($name);

    /**
     * Set path
     * @param $path
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setPath($path);

    /**
     * Set is synced
     * @param $isSynced
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setIsSynced($isSynced);

    /**
     * Set Medipim Id
     * @param $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimId($medipimId);

    /**
     * Set Medipim Parent Category Id
     * @param $parentId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimParentId($parentId);

    /**
     * Set Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Set Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Set Medipim Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimCreatedAt($createdAt);

    /**
     * Set Medipim Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMedipimUpdatedAt($updatedAt);

    /**
     * Set Category level
     * @param $level
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setLevel($level);


    /**
     * Set Magento category id
     * @param $level
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setMagentoCategoryId($id);


    /**
     * Set Data from Medipim Api
     * @param $data
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     */
    public function setDataFromApi($data);
}
