<?php

namespace Baldwin\MedipimConnector\Api\Data;

use Baldwin\MedipimConnector\Model\ImportProfile;

/**
 * Interface JobInterface
 * @package Baldwin\MedipimConnector\Api\Data
 */
interface JobInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                        = 'id';
    const PROFILE_ID                = 'profile_id';
    const BATCH_GROUP_ID            = 'batch_group_id';
    const STATUS                    = 'status';
    const CREATED_AT                = 'created_at';
    const SCHEDULED_AT              = 'scheduled_at';
    const EXECUTED_AT               = 'executed_at';
    const FINISHED_AT               = 'finished_at';
    const IS_PUBLISHED              = 'is_published';

    const STATUS_PENDING            = 'pending';
    const STATUS_RUNNING            = 'running';
    const STATUS_SYNCING            = 'syncing';
    const STATUS_READY_FOR_IMPORT   = 'ready_for_import';
    const STATUS_NOTHING_TO_IMPORT  = 'nothing_to_import';
    const STATUS_IMPORTING          = 'importing';
    const STATUS_SUCCESS            = 'success';
    const STATUS_ERROR              = 'error';
    const STATUS_ALL_PUBLISHED      =  'published';

    const STATUS_NOT_PUBLISHED      = 0;
    const STATUS_PUBLISHED          = 1;

    /**
     * Get Id
     * @return string|null
     */
    public function getId();


    /**
     * Get Profile Id
     * @return string|null
     */
    public function getProfileId();

    /**
     * Get Batch group Id
     * @return string|null
     */
    public function getBatchGroupId();

    /**
     * Get Status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set Created at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Scheduled at
     * @return string|null
     */
    public function getScheduledAt();

    /**
     * Set Executed at
     * @return string|null
     */
    public function getExecutedAt();

    /**
     * get Finished at
     * @return string|null
     */
    public function getFinishedAt();

    /**
     * Get is Job Published to queue
     * @return bool
     */
    public function getIsPublished(): bool;

    /**
     * Set Id
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setId($id);


    /**
     * Set Profile Id
     * @param $profileId
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setProfileId($profileId);

    /**
     * Set Batch group Id
     * @param $batchGroupId
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setBatchGroupId($batchGroupId);

    /**
     * Set Status
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setStatus($status);

    /**
     * Set Created at
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Set Scheduled at
     * @param $scheduledAt
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setScheduledAt($scheduledAt);

    /**
     * Set Executed at
     * @param $executedAt
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setExecutedAt($executedAt);

    /**
     * Set Finished at
     * @param $finishedAt
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setFinishedAt($finishedAt);

    /**
     * @param bool $isPublished
     * @return \Baldwin\MedipimConnector\Api\Data\JobInterface
     */
    public function setIsPublished(bool $isPublished);
}
