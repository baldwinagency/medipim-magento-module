<?php

namespace Baldwin\MedipimConnector\Api\Data;

interface BrandInterface
{
    const BRAND_NAME = 'name';

    /**
     * Get Id
     * @return int|null
     */
    public function getId();

    /**
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\BrandInterface
     */
    public function setId($id);


    /**
     * Get batch data
     * @return string|null
     */
    public function getName();

    /**
     * @param $name
     * @return \Baldwin\MedipimConnector\Api\Data\BrandInterface
     */
    public function setName(string $name);
}
