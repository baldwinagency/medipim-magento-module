<?php

namespace Baldwin\MedipimConnector\Api\Data;

/**
 * Medipim Attribute Model
 * @package Baldwin\MedipimConnector\Api\Data
 */
interface AttributeInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
     const ID               = 'id';
     const MEDIPIM_ID       = 'medipim_id';
     const ATTRIBUTE_ID     = 'attribute_id';
     const ATTRIBUTE_TYPE   = 'type';
     const IS_MULTISELECT   = 'is_multi';
     const IS_LOCALIZED     = 'is_localized';
     const CREATED_AT       = 'created_at';
     const UPDATED_AT       = 'updated_at';

    /**
     * Get Id
     * @return string|null
     */
    public function getId();

    /**
     * Get Medipim Id
     * @return string|null
     */
    public function getMedipimId();

    /**
     * Get Eav Attribute Id
     * @return string|null
     */
    public function getAttributeId();

    /**
     * Get Attribute Type
     * @return string|null
     */
    public function getType();

    /**
     * Is attribute a multi select
     * @return boolean|null
     */
    public function getIsMulti();

    /**
     * Get Created At
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get Updated At
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set id
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setId($id);

    /**
     * Set Medipim Id
     * @param $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setMedipimId($medipimId);

    /**
     * Set Eav Attribute Id
     * @param $attributeId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setAttributeId($attributeId);

    /**
     * get Attribute type
     * @param $type
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setType($type);

    /**
     * Set attribute as multi select
     * @param $isMulti
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setIsMulti($isMulti);

    /**
     * Set if attribute is localized
     * @param $isLocalized
     * @return mixed
     */
    public function setIsLocalized($isLocalized);

    /**
     * Set Created at time
     * @param $createdAt
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Set Updated At time
     * @param $updatedAt
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Set Data from Medipim Api
     * @param $data
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     */
    public function setDataFromApi($data);
}
