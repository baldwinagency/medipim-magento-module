<?php

namespace Baldwin\MedipimConnector\Api\Data;

interface ImportProfileInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                       = 'profile_id';
    const IDENTIFIER               = 'identifier';
    const STORE_IDS                = 'store_ids';
    const BRAND_IDS                = 'brand_ids';
    const SUPPLIER_IDS             = 'supplier_ids';
    const CATEGORY_IDS             = 'category_ids';
    const ATTACHMENT               = 'attachment';
    const STATUS                   = 'status';
    const FREQUENCY                = 'frequency';
    const START_TIME               = 'start_time';
    const MANUAL_CONFIRMATION      = 'manual_confirmation';
    const MINIMUM_CONTENT_REQUIERD = 'min_content_required';
    const UPDATED_SINCE            = 'updated_since';
    const STATUS_ENABLED           = 1;
    const STATUS_DISABLED          = 0;


    const FREQUENCY_MANUAL         = 'manual';
    const FREQUENCY_HOUR           = 'hour';
    const FREQUENCY_DAY            = 'day';
    const FREQUENCY_WEEK           = 'week';
    const FREQUENCY_MONTH          = 'month';


    /**
     * Get Id
     * @return string|null
     */
    public function getId();

    /**
     * Get Identifier
     * @return string|null
     */
    public function getIdentifier();

    /**
     * Get Stores
     * @return mixed
     */
    public function getStoreIds();

    /**
     * Get Minium Content Required
     * @return string|null
     */
    public function getMinContentRequired();

    /**
     * Get Brands
     * @return string|null
     */
    public function getBrandIds();

    /**
     * Get Suppliers
     * @return string|null
     */
    public function getSupplierIds();

    /**
     * Get Categories
     * @return string|null
     */
    public function getCategoryIds();

    /**
     * Get Attachment
     * @return string|null
     */
    public function getAttachment();

    /**
     * Get Status
     * @return string|null
     */
    public function getStatus();

    /**
     * Get Frequency
     * @return string|null
     */
    public function getFrequency();

    /**
     * Get Start time
     * @return string|null
     */
    public function getStartTime();

    /**
     * Get manual confirmation
     * @return boolean|null
     */
    public function getManualConfirmation();

    /**
     * Get the timestamp of the last time this import profile ran
     * @return mixed
     */
    public function getUpdatedSince();

    /**
     * Set id
     * @param $id
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setId($id);


    /**
     * Get Identifier
     * @param $identifier
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setIdentifier($identifier);

    /**
     * Set Minium Content Required
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setMinContentRequired($miniumContentRequired);

    /**
     * Set Brands
     * @param $brands
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setBrandIds($brands);

    /**
     * Set Stores
     * @param $stores
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStoreIds($stores);

    /**
     * Set Suppliers
     * @param $suppliers
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setSupplierIds($suppliers);

    /**
     * Set Categories
     * @param $categories
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setCategoryIds($categories);

    /**
     * Set Attachment
     * @param $attachment
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setAttachment($attachment);

    /**
     * Set status
     * @param $status
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStatus($status);

    /**
     * Set frequency
     * @param $frequency
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setFrequency($frequency);

    /**
     * Set start time
     * @param $startTime
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setStartTime($startTime);

    /**
     * Set Manual confirmation
     * @param $confirmation
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setManualConfirmation($confirmation);

    /**
     * Set the timestamp of the last time this import profile ran
     * @param $updatedSince
     * @return \Baldwin\MedipimConnector\Api\Data\ImportProfileInterface
     */
    public function setUpdatedSince($updatedSince);
}
