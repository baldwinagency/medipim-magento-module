<?php

namespace Baldwin\MedipimConnector\Api;

use Baldwin\MedipimConnector\Api\Data\BatchInterface;

interface BatchRepositoryInterface
{
    /**
     * Save Batch.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\BatchInterface $batch
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(BatchInterface $batch);

    /**
     * Retrieve Batch.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\BatchInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Batch.
     *
     * @param int $batchGroupId
     * @return \Baldwin\MedipimConnector\Model\ResourceModel\Batch\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByBatchGroupId($batchGroupId);

    /**
     * Retrieve Batches by status.
     *
     * @param string $status
     * @return \Baldwin\MedipimConnector\Model\ResourceModel\Batch\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByStatus($status);


    public function deleteByBatchGroupId(string $batchGroupId);

    /**
     * Delete Batch
     * @param BatchInterface $batch
     * @return BatchInterface
     */
    public function delete(BatchInterface $batch);

    /**
     * @param BatchInterface $batch
     * @return mixed
     */
    public function reschedule(BatchInterface $batch);
}
