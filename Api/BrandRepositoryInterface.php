<?php

namespace Baldwin\MedipimConnector\Api;

interface BrandRepositoryInterface
{
    /**
     * Save Brand.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\BrandInterface $brand
     * @return \Baldwin\MedipimConnector\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\BrandInterface $brand);

    /**
     * Retrieve Brand.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);
}
