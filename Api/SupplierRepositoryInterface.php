<?php

namespace Baldwin\MedipimConnector\Api;

interface SupplierRepositoryInterface
{
    /**
     * Save Supplier.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\SupplierInterface $supplier
     * @return \Baldwin\MedipimConnector\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\SupplierInterface $supplier);

    /**
     * Retrieve Supplier.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\SupplierInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);
}
