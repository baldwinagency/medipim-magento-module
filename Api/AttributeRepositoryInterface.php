<?php

namespace Baldwin\MedipimConnector\Api;

interface AttributeRepositoryInterface
{
    /**
     * Save Attribute.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\AttributeInterface $attribute
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\AttributeInterface $attribute);

    /**
     * Retrieve Attribute.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Attribute.
     *
     * @param int $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByMedipimId($medipimId);

    /**
     * Retrieve Attribute.
     *
     * @param int $eavAttribute
     * @return \Baldwin\MedipimConnector\Api\Data\AttributeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByEavAttributeId($eavAttributeId);
}
