<?php

namespace Baldwin\MedipimConnector\Api;

interface CategoryRepositoryInterface
{
    /**
     * Save Category.
     *
     * @param \Baldwin\MedipimConnector\Api\Data\CategoryInterface $attribute
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Baldwin\MedipimConnector\Api\Data\CategoryInterface $attribute);

    /**
     * Retrieve Category.
     *
     * @param int $id
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Category by Medipim ID.
     *
     * @param int $medipimId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByMedipimId($medipimId);

    /**
     * Retrieve Category Medipim Parent ID.
     *
     * @param int $parentId
     * @return \Baldwin\MedipimConnector\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByMedipimParentId($parentId);
}
