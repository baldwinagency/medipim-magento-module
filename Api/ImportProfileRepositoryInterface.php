<?php

namespace Baldwin\MedipimConnector\Api;

use Baldwin\MedipimConnector\Api\Data\ImportProfileInterface;

interface ImportProfileRepositoryInterface
{
    /**
     * Save Import Profile.
     *
     * @param ImportProfileInterface $batch
     * @return ImportProfileInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ImportProfileInterface $importProfile);

    /**
     * Retrieve Import Profile.
     *
     * @param int $id
     * @return ImportProfileInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Delete Import Profile.
     *
     * @param ImportProfileInterface $batch
     * @return ImportProfileInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ImportProfileInterface $importProfile);

    /**
     * Schedule Import Profile.
     *
     * @param ImportProfileInterface $batch
     * @return ImportProfileInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function schedule(ImportProfileInterface $importProfile);

}
