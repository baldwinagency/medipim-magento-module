<?php

namespace Baldwin\MedipimConnector\Api;

use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Model\Job;

/**
 * Interface JobProcessorInterface
 * @package Baldwin\MedipimConnector\Api
 */
interface JobProcessorInterface
{
    /**
     * @param Baldwin\MedipimConnector\Api\Data\JobInterface $job
     * @return bool
     */
    public function executeJobSync(JobInterface $job): bool;

    /**
     * @param Baldwin\MedipimConnector\Api\Data\JobInterface $job
     * @return bool
     */
    public function executeJobImport(JobInterface $job);
}
