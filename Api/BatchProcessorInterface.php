<?php

namespace Baldwin\MedipimConnector\Api;

use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Model\Batch;

interface BatchProcessorInterface
{
    /**
     * @param array $status
     * @param string|null $batchGroupId
     */
    public function publishAllBatches($status = [Batch::SYNC_STATUS_PENDING], JobInterface $job);

    /**
     * @param string $bathId
     * @return bool
     */
    public function publishBatch(string $bathId): bool;

    /**
     * @param string $batchId
     * @return bool
     */
    public function executeSingleBatch(string $batchId): bool;
}
