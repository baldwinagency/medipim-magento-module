<?php

namespace Baldwin\MedipimConnector\Api;

use Baldwin\MedipimConnector\Api\Data\JobInterface;

interface JobRepositoryInterface
{
    /**
     * Save Job
     * @param JobInterface $job
     * @return JobInterface
     */
    public function save(JobInterface $job);

    /**
     * Retrieve Job
     * @param $id
     * @return JobInterface
     */
    public function getById($id);

    /**
     * Retrieve Job
     *
     * @param int $batchGroupId
     * @return JobInterface
     */
    public function getByBatchGroupId($batchGroupId);


    /**
     * Delete Job
     * @param JobInterface $job
     * @return JobInterface
     */
    public function delete(JobInterface $job);

    /**
     * Reschedule Job
     * @param JobInterface $job
     * @return JobInterface
     */
    public function reschedule(JobInterface $job);

}
