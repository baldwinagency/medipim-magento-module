ssh -T medipim@m2m.dev.medipim.be <<+
    cd /home/medipim/m2m.dev.medipim.be
    git clone repo-magento-env ${DEPLOY_ID}
    cd ${DEPLOY_ID}

    DEPLOY_ID=${DEPLOY_ID} MODULE_COMMIT=${MODULE_COMMIT} ./docker-deploys/testing/configure.sh
    docker-compose up -d
+
