ssh -T medipim@m2m.dev.medipim.be <<+
    cd /home/medipim/m2m.dev.medipim.be
    cd ${DEPLOY_ID}

    docker-compose down

    cd ..
    rm -rf ${DEPLOY_ID}
+
