<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Baldwin\MedipimConnector\Controller;

/**
 * Declarations of core registry keys used by the Customer module
 *
 */
class RegistryConstants
{
    /**
     * Registry key where current job ID is stored
     */
    const CURRENT_JOB_ID = 'current_job_id';
}
