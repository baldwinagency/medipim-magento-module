<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\ImportProfile;

use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Baldwin\MedipimConnector\Model\ImportProfile;
use Baldwin\MedipimConnector\Model\ImportProfileFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Save Import profile action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::save';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ImportProfileRepositoryInterface
     */
    private $importProfileRepository;

    /**
     * @var ImportProfileFactory
     */
    private $importProfileFactory;


    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        DataPersistorInterface $dataPersistor,
        ImportProfileFactory $importProfileFactory,
        ImportProfileRepositoryInterface $importProfileRepository
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->importProfileFactory = $importProfileFactory;
        $this->importProfileRepository = $importProfileRepository;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = ImportProfile::STATUS_ENABLED;
            }

            if (isset($data['category_ids']) && is_array($data['category_ids'])) {
                $data['category_ids'] = implode(",", $data['category_ids']);
            }

            if (isset($data['brand_ids']) && is_array($data['brand_ids'])) {
                $data['brand_ids'] = implode(",", $data['brand_ids']);
            }

            if (isset($data['supplier_ids']) && is_array($data['supplier_ids'])) {
                $data['supplier_ids'] = implode(",", $data['supplier_ids']);
            }

            if (isset($data['store_ids']) && is_array($data['store_ids'])) {
                $data['store_ids'] = implode(",", $data['store_ids']);
            }

            $model = $this->importProfileFactory->create();

            $id = $this->getRequest()->getParam('profile_id');
            if ($id) {
                try {
                    $model = $this->importProfileRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This page no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'medipim_importprofile_prepare_save',
                ['profile' => $model, 'request' => $this->getRequest()]
            );

            if (!$this->dataProcessor->validate($data)) {
                return $resultRedirect->setPath('*/*/edit', ['profile_id' => $model->getId(), '_current' => true]);
            }

            try {
                $this->importProfileRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the import profile.'));
                return $this->processResultRedirect($model, $resultRedirect, $data);
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the import profile.'));
            }

            $this->dataPersistor->set('medipim_importprofile', $data);
            return $resultRedirect->setPath('*/*/edit', ['profile_id' => $this->getRequest()->getParam('profile_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process result redirect
     *
     * @param \Magento\Cms\Api\Data\PageInterface $model
     * @param \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     * @param array $data
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    private function  processResultRedirect($model, $resultRedirect, $data)
    {
        if ($this->getRequest()->getParam('back', false) === 'duplicate') {
            $newProfile = $this->importProfileFactory->create(['data' => $data]);
            $newProfile->setId(null);
            $identifier = $model->getIdentifier() . '-' . uniqid();
            $newProfile->setIdentifier($identifier);
            $newProfile->setStatus(false);
            $this->importProfileRepository->save($newProfile);
            $this->messageManager->addSuccessMessage(__('You duplicated the import profile.'));
            return $resultRedirect->setPath(
                '*/*/edit',
                [
                    'profile_id' => $newProfile->getId(),
                    '_current' => true
                ]
            );
        }
        $this->dataPersistor->clear('medipim_importprofile');
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['profile_id' => $model->getId(), '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
