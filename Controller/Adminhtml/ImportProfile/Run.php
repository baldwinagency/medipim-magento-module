<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\ImportProfile;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface as ImportProfileRepository;

/**
 * Run Import profile action.
 */
class Run extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    private $importProfileRepository;

    public function __construct(
        Action\Context $context,
        ImportProfileRepository $importProfileRepository
    )
    {
        $this->importProfileRepository = $importProfileRepository;
        parent::__construct($context);
    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::importprofile_run';

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('profile_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $title = "";
            try {
                // init model and delete
                $importProfile = $this->importProfileRepository->getById($id);

                $title = $importProfile->getIdentifier();

                $this->importProfileRepository->schedule($importProfile);

                // display success message
                $this->messageManager->addSuccessMessage(__('The Import profile is scheduled.'));

                // go to grid
                $this->_eventManager->dispatch('adminhtml_medipim_importprofile_on_run', [
                    'title' => $title,
                    'status' => 'success'
                ]);

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_medipim_importprofile_on_run',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['page_id' => $id]);
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find an Import profile to run.'));

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
