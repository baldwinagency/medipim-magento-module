<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\ImportProfile;

use Baldwin\MedipimConnector\Model\ImportProfile;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;

/**
 * Edit Import Profile action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::save';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Baldwin\MedipimConnector\Model\ImportProfileFactory
     */
    private $importProfileFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Baldwin\MedipimConnector\Model\ImportProfileFactory $importProfileFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->importProfileFactory = $importProfileFactory;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Baldwin_MedipimConnector::import_profile')
            ->addBreadcrumb(__('Medipim'), __('Medipim'))
            ->addBreadcrumb(__('Manage Import Profiles'), __('Manage Import Profiles'));
        return $resultPage;
    }

    /**
     * Edit Import Profile
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('profile_id');
        $model = $this->importProfileFactory->create();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This page no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $model = $this->prepareData($model);

        $this->_coreRegistry->register('import_profile', $model);


        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Import Profile') : __('New Import Profile'),
            $id ? __('Edit Import Profile') : __('New Import Profile')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Import Profiles'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Import Profile'));

        return $resultPage;
    }

    private function prepareData(ImportProfile $model) {
        $data = $model->getData();
        if (isset($data['brand_ids']) && is_string($data['brand_ids'])) {
            $brandIds = $data['brand_ids'];
            $brandIds = explode(",", $brandIds);

            $parsedBrandIds = [];
            foreach($brandIds as $brandId) {
                $parsedBrandIds[] = ['brand_id' => $brandId];
            }

            $model->setData("brand_ids", null);
        }

        if (isset($data['supplier_ids']) && is_string($data['supplier_ids'])) {
            $supplierIds = $data['supplier_ids'];
            $supplierIds = explode(",", $supplierIds);

            $parsedSupplierIds = [];

            foreach($supplierIds as $supplierId) {
                $parsedSupplierIds[] = ['supplier_id' => $supplierId];
            }

            $model->setData("supplier_ids", null);
        }
        return $model;
    }
}
