<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\ImportProfile;

use Baldwin\MedipimConnector\Api\Data\ImportProfileInterface;
use Baldwin\MedipimConnector\Model\ImportProfile;
use Magento\Backend\App\Action\Context;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface as ImportProfileRepository;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Import Profile grid inline edit controller
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::save';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var ImportProfileRepository
     */
    protected $importProfileRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param ImportProfileRepository $importProfileRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        ImportProfileRepository $importProfileRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->importProfileRepository = $importProfileRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $profileId) {
            /* @var ImportProfile $importProfile */
            $importProfile = $this->importProfileRepository->getById($profileId);
            try {
                $profileData = $this->filterPost($postItems[$profileId]);
                $this->validatePost($profileData, $importProfile, $error, $messages);
                $extendedProfileData = $importProfile->getData();
                $this->setImportProfileData($importProfile, $extendedProfileData, $profileData);
                $this->importProfileRepository->save($importProfile);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithProfileId($importProfile, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithProfileId($importProfile, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithProfileId(
                    $importProfile,
                    __('Something went wrong while saving the Import Profile.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Filtering posted data.
     *
     * @param array $postData
     * @return array
     */
    protected function filterPost($postData = [])
    {
        $profileData = $this->dataProcessor->filter($postData);
        return $profileData;
    }

    /**
     * @param array $profileData
     * @param ImportProfile $importProfile
     * @param $error
     * @param array $messages
     */
    protected function validatePost(array $profileData, ImportProfile $importProfile, &$error, array &$messages)
    {
        if (!($this->dataProcessor->validate($profileData) && $this->dataProcessor->validateRequireEntry($profileData))) {
            $error = true;
            foreach ($this->messageManager->getMessages(true)->getItems() as $error) {
                $messages[] = $this->getErrorWithProfileId($importProfile, $error->getText());
            }
        }
    }

    /**
     * @param ImportProfileInterface $importProfile
     * @param $errorText
     * @return string
     */
    protected function getErrorWithProfileId(ImportProfileInterface $importProfile, $errorText)
    {
        return '[Profile ID: ' . $importProfile->getId() . '] ' . $errorText;
    }

    /**
     * @param ImportProfile $importProfile
     * @param array $extendedProfileData
     * @param array $profileData
     * @return $this
     */
    public function setImportProfileData(ImportProfile $importProfile, array $extendedProfileData, array $profileData)
    {
        $importProfile->setData(array_merge($importProfile->getData(), $extendedProfileData, $profileData));
        return $this;
    }
}
