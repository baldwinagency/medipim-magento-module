<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Baldwin\MedipimConnector\Model\Import\Brand\ImporterFactory;

/**
 * Class Update
 * @package Baldwin\MedipimConnector\Controller\Adminhtml\Brand
 */
class Update extends Action
{
    const BRAND_LISTING_DATA_SOURCE = 'medipim_brand_listing.medipim_brand_listing_data_source';

    protected $resultJsonFactory = false;

    private $brandImporterFactory;
    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        ImporterFactory $importerFactory
    ) {
        $this->brandImporterFactory = $importerFactory;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|mixed
     */
    public function execute()
    {
        $importer = $this->brandImporterFactory->create();
        $importer->execute();

        $response = ['dataSource' => self::BRAND_LISTING_DATA_SOURCE];

        $resultJson = $this->resultJsonFactory->create();

        $resultJson->setData($response);

        return $resultJson;

    }
}
