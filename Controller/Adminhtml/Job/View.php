<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Job;

use Baldwin\MedipimConnector\Model\Job;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action;
use Baldwin\MedipimConnector\Controller\RegistryConstants;

/**
 * View Job action.
 */
class View extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Baldwin\MedipimConnector\Model\JobFactory
     */
    private $jobFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Baldwin\MedipimConnector\Model\JobFactory $jobFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->jobFactory = $jobFactory;
        parent::__construct($context);
    }


    protected function initCurrentJob()
    {
        $jobId = (int)$this->getRequest()->getParam('id');

        if ($jobId) {
            $this->_coreRegistry->register(RegistryConstants::CURRENT_JOB_ID, $jobId);
        }

        return $jobId;
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Baldwin_MedipimConnector::job')
            ->addBreadcrumb(__('Medipim'), __('Medipim'))
            ->addBreadcrumb(__('Manage Jobs'), __('Manage Jobs'));
        return $resultPage;
    }


    public function execute()
    {


        // 1. Get ID and create model
        $id =  $this->initCurrentJob();
        $model = $this->jobFactory->create();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This page no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $model = $this->prepareData($model);

        $this->_coreRegistry->register('job', $model);


        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('View Import Job') : __('View Import Job'),
            $id ? __('View Import Job') : __('View Import Job')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Import Jobs'));
        $resultPage->getConfig()->getTitle()
            ->prepend( __('View Import Job: ID ') . $model->getId());

        return $resultPage;
    }

    private function prepareData(Job $model) {
        $data = $model->getData();

        return $model;
    }
}
