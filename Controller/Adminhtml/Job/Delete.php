<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Job;

use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
/**
 * Delete Job action.
 */
class Delete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    private $jobRepository;

    public function __construct(
        Action\Context $context,
        JobRepositoryInterface $jobRepository
    )
    {
        $this->jobRepository = $jobRepository;
        parent::__construct($context);
    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::job_delete';

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $title = "";
            try {
                // init model and delete
                $job = $this->jobRepository->getById($id);

                $title = $job->getIdentifier();

                $this->jobRepository->delete($job);

                // display success message
                $this->messageManager->addSuccessMessage(__('The Job has been deleted.'));

                // go to grid
                $this->_eventManager->dispatch('adminhtml_medipim_job_on_delete', [
                    'title' => $title,
                    'status' => 'success'
                ]);

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_medipim_job_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/index');
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Job to delete.'));

        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
