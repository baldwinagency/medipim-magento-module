<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Batch;

use Baldwin\MedipimConnector\Api\BatchRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Baldwin\MedipimConnector\Model\Batch;

/**
 * Run Batch action.
 */
class Run extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    private $batchRepository;

    public function __construct(
        Action\Context $context,
        BatchRepositoryInterface $batchRepository
    )
    {
        $this->batchRepository = $batchRepository;
        parent::__construct($context);
    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::batch_run';

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('batch_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $title = "";
            try {
                // init model and delete
                $batch = $this->batchRepository->getById($id);

                $this->batchRepository->reschedule($batch);

                // display success message
                $this->messageManager->addSuccessMessage(__('The Batch is scheduled'));

                // go to grid
                $this->_eventManager->dispatch('adminhtml_medipim_batch_on_run', [
                    'title' => $title,
                    'status' => 'success'
                ]);

                return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_medipim_batch_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/index');
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Batch to delete.'));

        // go to grid
        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }
}
