<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Supplier;

use Baldwin\MedipimConnector\Model\Import\Supplier\ImporterFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class Index
 * @package Baldwin\MedipimConnector\Controller\Adminhtml\Job
 */
class Update extends Action
{
    const SUPPLIER_LISTING_DATA_SOURCE = 'medipim_supplier_listing.medipim_supplier_listing_data_source';

    /**
     * @var ImporterFactory
     */
    protected $supplierImporterFactory;

    /**
     * @var bool|JsonFactory
     */
    protected $resultJsonFactory = false;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        ImporterFactory $importerFactory
    ) {
        $this->supplierImporterFactory = $importerFactory;
        $this->resultJsonFactory       = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Json|ResultInterface
     */
    public function execute()
    {
        $importer = $this->supplierImporterFactory->create();
        $importer->execute();

        $response = ['dataSource' => self::SUPPLIER_LISTING_DATA_SOURCE];

        $resultJson = $this->resultJsonFactory->create();

        $resultJson->setData($response);

        return $resultJson;
    }
}
