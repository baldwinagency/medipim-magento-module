<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\Category;

use Baldwin\MedipimConnector\Api\Data\CategoryInterface;
use Baldwin\MedipimConnector\Model\ImportProfile;
use Magento\Backend\App\Action\Context;
use Baldwin\MedipimConnector\Api\CategoryRepositoryInterface as CategoryRepository;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Import Profile grid inline edit controller
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Baldwin_MedipimConnector::save';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    protected $categoryRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param PostDataProcessor $dataProcessor
     * @param ImportProfileRepository $importProfileRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        CategoryRepository $categoryRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->categoryRepository = $categoryRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $categoryId) {
            /* @var ImportProfile $importProfile */
            $category = $this->categoryRepository->getById($categoryId);
            try {
                $categoryData = $this->filterPost($postItems[$categoryId]);
                $this->validatePost($categoryData, $category, $error, $messages);

                $extendedProfileData = $category->getData();
                $this->setImportProfileData($category, $extendedProfileData, $categoryData);

                $this->categoryRepository->save($category);

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithCategoryId($category, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithCategoryId($category, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithCategoryId(
                    $importProfile,
                    __('Something went wrong while saving the Category.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Filtering posted data.
     *
     * @param array $postData
     * @return array
     */
    protected function filterPost($postData = [])
    {
        $profileData = $this->dataProcessor->filter($postData);
        return $profileData;
    }

    /**
     * @param array $categoryData
     * @param CategoryInterface $category
     * @param $error
     * @param array $messages
     */
    protected function validatePost(array $categoryData, CategoryInterface $category, &$error, array &$messages)
    {
        if (!($this->dataProcessor->validate($categoryData) && $this->dataProcessor->validateRequireEntry($categoryData))) {
            $error = true;
            foreach ($this->messageManager->getMessages(true)->getItems() as $error) {
                $messages[] = $this->getErrorWithCategoryId($category, $error->getText());
            }
        }
    }

    /**
     * @param CategoryInterface $category
     * @param $errorText
     * @return string
     */
    protected function getErrorWithCategoryId(CategoryInterface $category, $errorText)
    {
        return '[Category ID: ' . $category->getId() . '] ' . $errorText;
    }

    /**
     * @param CategoryInterface $category
     * @param array $extendedProfileData
     * @param array $categoryData
     * @return $this
     */
    public function setImportProfileData(CategoryInterface $category, array $extendedProfileData, array $categoryData)
    {
        $category->setData(array_merge($category->getData(), $extendedProfileData, $categoryData));
        return $this;
    }
}
