<?php

namespace Baldwin\MedipimConnector\Controller\Adminhtml\System\Config\Import;

use Baldwin\MedipimConnector\Cron\SyncCategories;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Cron\Model\Schedule;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Cron\Model\ResourceModel\Schedule\Collection as CronScheduleCollection;

/**
 * Class Categories
 * @package Baldwin\MedipimConnector\Controller\Adminhtml\System\Config\Import
 */
class Categories extends Action
{
    const CRONJOB_NAME = 'baldwin_medipimconnector_importcategories';

    const DEBUG = false;

    private $resultJsonFactory;
    private $dateTime;
    private $cronScheduleCollection;
    private $import;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        DateTime $dateTime,
        CronScheduleCollection $cronScheduleCollection,
        SyncCategories $import
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->dateTime = $dateTime;
        $this->import = $import;
        $this->cronScheduleCollection = $cronScheduleCollection;
    }

    public function execute()
    {
        $success = false;
        $message = '';

        try {
            if (!self::DEBUG) {
                $scheduleId = $this->scheduleNewFullImportJob();

                $success = true;
                $message = __("Medipim Categories import will start shortly (schedule id: %1)", $scheduleId);

            } else {
                $this->import->execute();
            }
        } catch (\Exception $ex) {
            $message = "ERROR: {$ex->getMessage()}";
        }

        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData([
            'valid'   => (int) $success,
            'message' => $message,
        ]);
    }

    private function scheduleNewFullImportJob()
    {
        $createdAtTime   = $this->dateTime->gmtTimestamp();
        $scheduledAtTime = $createdAtTime;

        $schedule = $this->cronScheduleCollection->getNewEmptyItem();
        $schedule
            ->setJobCode(self::CRONJOB_NAME)
            ->setStatus(Schedule::STATUS_PENDING)
            ->setCreatedAt(strftime('%Y-%m-%d %H:%M:%S', $createdAtTime))
            ->setScheduledAt(strftime('%Y-%m-%d %H:%M', $scheduledAtTime))
            ->save();

        return $schedule->getScheduleId();
    }
}
