<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Model\Import\Brand\ImporterFactory;

/**
 * Class SyncBrands
 * @package Baldwin\MedipimConnector\Cron
 */
class SyncBrands
{
    private $importerFactory;

    public function __construct(
        ImporterFactory $importerFactory
    ) {
        $this->importerFactory = $importerFactory;
    }

    /**
     * Function that fetches the medipim brands and tries to import them in Magento.
     *
     */
    public function execute()
    {
        $importer = $this->importerFactory->create();
        $importer->execute();
    }
}
