<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Model\Import\Category\ImporterFactory;

/**
 * Class SyncProductAttributes
 * @package Baldwin\MedipimConnector\Cron
 */
class SyncCategories
{
    private $importerFactory;

    public function __construct(
        ImporterFactory $importerFactory
    ) {
        $this->importerFactory = $importerFactory;
    }

    /**
     * Function that fetches the medipim categories and tries to import them in Magento.
     *
     */
    public function execute()
    {
        $importer = $this->importerFactory->create();
        $importer->runImport();
    }
}
