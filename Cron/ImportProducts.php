<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Model\Import\Product\ImporterFactory;
use Baldwin\MedipimConnector\Model\Import\Product\Importer;

/**
 * Class SyncProductAttributes
 * @package Baldwin\MedipimConnector\Cron
 */
class ImportProducts
{
    private $importerFactory;

    public function __construct(
        ImporterFactory $importerFactory
    ) {
        $this->importerFactory = $importerFactory;
    }

    /**
     * Function that fetches the medipim categories and tries to import them in Magento.
     *
     */
    public function execute()
    {
        /* @var Importer $importer */
        $importer = $this->importerFactory->create();
        $importer->fullBatchImport();
    }
}
