<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Api\Data\JobInterface;
use Baldwin\MedipimConnector\Api\JobProcessorInterface;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Baldwin\MedipimConnector\Model\ImportProfile;
use Baldwin\MedipimConnector\Model\Job;
use Baldwin\MedipimConnector\Model\ResourceModel\Job\CollectionFactory as JobCollectionFactory;
use Baldwin\MedipimConnector\Model\ResourceModel\ImportProfile\CollectionFactory as ImportProfileCollectionFactory;
use Magento\Framework\MessageQueue\PublisherInterface;
use \Magento\Framework\Reflection\DataObjectProcessor;
use Baldwin\MedipimConnector\Api\Data\JobInterfaceFactory;

/**
 * Class JobScheduler
 * @package Baldwin\MedipimConnector\Cron
 */
class JobPublisher
{
    const TOPIC_NAME_SYNC   = 'medipim.job.sync';
    const TOPIC_NAME_IMPORT = 'medipim.job.import';

    private $jobProcessor;
    private $jobCollectionFactory;
    private $importProfileCollectionFactory;
    private $publisher;
    private $importProfileRepository;
    private $dataObjectProcessor;
    private $jobInterfaceFactory;
    private $jobRepository;

    public function __construct(
        JobProcessorInterface $jobProcessor,
        JobCollectionFactory $jobCollectionFactory,
        ImportProfileCollectionFactory $importProfileCollectionFactory,
        ImportProfileRepositoryInterface $importProfileRepository,
        PublisherInterface $publisher,
        DataObjectProcessor $dataObjectProcessor,
        JobInterfaceFactory $jobInterfaceFactory,
        JobRepositoryInterface $jobRepository
    ) {
        $this->jobProcessor                    = $jobProcessor;
        $this->jobCollectionFactory            = $jobCollectionFactory;
        $this->publisher                       = $publisher;
        $this->importProfileCollectionFactory  = $importProfileCollectionFactory;
        $this->importProfileRepository         = $importProfileRepository;
        $this->dataObjectProcessor             = $dataObjectProcessor;
        $this->jobInterfaceFactory             = $jobInterfaceFactory;
        $this->jobRepository                   = $jobRepository;
    }

    /**
     * This function creates new jobs from import profiles
     */
    public function creator()
    {
        $importProfiles = $this->importProfileCollectionFactory->create()
            ->addAttributeToSelect("*")
            ->addFieldToFilter("status", ['eq' => ImportProfile::STATUS_ENABLED]);

        foreach ($importProfiles as $importProfile) {
            if ($this->shouldScheduleImportProfile($importProfile)) {
                $this->importProfileRepository->schedule($importProfile);
            }
        }
    }

    /**
     * This functions loops over the created jobs and publishes them to the queue.
     */
    public function publisher()
    {
        $jobs = $this->jobCollectionFactory->create()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter("is_published", ['eq' => Job::STATUS_NOT_PUBLISHED]);

        foreach ($jobs as $job) {
            $status = $job->getStatus();

            $topic = null;
            switch($status) {
                case Job::STATUS_PENDING:
                    $topic = self::TOPIC_NAME_SYNC;
                    break;
                case Job::STATUS_READY_FOR_IMPORT:
                    $topic = self::TOPIC_NAME_IMPORT;
                    break;
            }

            if ($topic !== null) {
                $this->publishJob($topic, $job);
            }
        }
    }

    /**
     * This function determines whether an import profiled should be scheduled, based on the frequency and last updated time.
     * @param ImportProfile $importProfile
     * @return bool
     */
    private function shouldScheduleImportProfile(ImportProfile $importProfile): bool
    {
        $jobs = $this->jobCollectionFactory->create()
            ->addAttributetoSelect("*")
            ->addFieldToFilter("profile_id",['eq' => $importProfile->getId()])
            ->addFieldToFilter("status",['in' => [Job::STATUS_SYNCING ,Job::STATUS_PENDING, Job::STATUS_IMPORTING, Job::STATUS_READY_FOR_IMPORT]]);

        if ($jobs->getSize() > 0) {
            return false;
        }

        if ($importProfile->getUpdatedSince() === null) {
            return true;
        }

        $lastImport = new \DateTime($importProfile->getUpdatedSince());

        $today = new \DateTime();

        $interval = $lastImport->diff($today);

        $frequency = $importProfile->getFrequency();

        $result = null;

        switch ($frequency) {
            case ImportProfile::FREQUENCY_HOUR:
                if ($interval->h >= 1) {
                    $result = true;
                }
                break;
            case ImportProfile::FREQUENCY_DAY:
                if ($interval->d >= 1) {
                    $result = true;
                }
                break;
            case ImportProfile::FREQUENCY_WEEK:
                if ($interval->days  >= 7) {
                    $result = true;
                }
                break;
            case ImportProfile::FREQUENCY_MONTH:
                if ($interval->m  >= 1) {
                    $result = true;
                }
                break;
            case ImportProfile::FREQUENCY_MANUAL:
                $result = false;
                break;
        }

        return $result;
    }

    /**
     * @param Job $job
     */
    private function publishJob(string $topic, Job $job)
    {
        $data = $this->dataObjectProcessor->buildOutputDataArray( $job,JobInterface::class);
        $interface = $this->jobInterfaceFactory->create(['data' => $data]);

        $this->publisher->publish($topic, $interface);
        $job->setIsPublished(Job::STATUS_PUBLISHED);
        $this->jobRepository->save($job);
    }
}
