<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Model\Import\Attribute\ImporterFactory;

/**
 * Class SyncProductAttributes
 * @package Baldwin\MedipimConnector\Cron
 */
class SyncProductAttributes
{
    private $importerFactory;

    public function __construct(
        ImporterFactory $importerFactory
    ) {
        $this->importerFactory = $importerFactory;
    }

    /**
     * Function that fetches the medipim attributes and tries to import them in Magento.
     *
     */
    public function execute()
    {
        $importer = $this->importerFactory->create();
        $importer->runImport();
    }
}
