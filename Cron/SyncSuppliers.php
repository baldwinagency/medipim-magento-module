<?php

namespace Baldwin\MedipimConnector\Cron;

use Baldwin\MedipimConnector\Model\Import\Supplier\ImporterFactory;

/**
 * Class SyncBrands
 * @package Baldwin\MedipimConnector\Cron
 */
class SyncSuppliers
{
    private $importerFactory;

    public function __construct(
        ImporterFactory $importerFactory
    ) {
        $this->importerFactory = $importerFactory;
    }

    /**
     * Function that fetches the medipim suppliers and tries to import them in Magento.
     *
     */
    public function execute()
    {
        $importer = $this->importerFactory->create();
        $importer->execute();
    }
}
