<?php

namespace Baldwin\MedipimConnector\Observer\Adminhtml;

use Baldwin\MedipimConnector\Model\Config\Source\MedipimLanguages;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Registry;

class StoreObserver implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Baldwin\MedipimConnector\Model\Config\Source\WebaLanguages
     */
    protected $languages;

    /**
     * Constructor
     *
     * @param Registry $registry
     * @param MedipimLanguages $languages
     */
    public function __construct(
        Registry $registry,
        MedipimLanguages $languages
    ) {
        $this->_coreRegistry = $registry;
        $this->languages = $languages;
    }

    /**
     * Add weba_code field to store fieldset
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $form = $observer->getEvent()->getBlock()->getForm();

        // Fetch the medipim languages and strip out the empty value
        $languagesSource = $this->languages->toOptionArray();
        array_unshift($languagesSource, ['value' => '', 'label' => ' ']);

        // Fetch the current storeModel
        $storeModel = $this->_coreRegistry->registry('store_data');

        // Add the medipim_code attribute to the store fieldset
        $fieldset = $form->getElement('store_fieldset');

        if ($fieldset !== null) {
            $fieldset->addField(
                'store_medipim_code',
                'select',
                [
                    'name'      => 'store[medipim_code]',
                    'label'     => __('Medipim Language Code'),
                    'title'     => __('Medipim Language Code'),
                    'values'    => $languagesSource,
                    'value'     => $storeModel->getData('medipim_code')
                ],
                'store_code'
            );
        }
    }
}
