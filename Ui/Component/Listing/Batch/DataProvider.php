<?php

namespace Baldwin\MedipimConnector\Ui\Component\Listing\Batch;

use Baldwin\MedipimConnector\Model\ResourceModel\Batch\Grid\CollectionFactory;

/**
 * Custom DataProvider for customer addresses listing
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface $request ,
     */
    private $request;


    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        array $meta = [],
        array $data = []
    )
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
    }

    /**
     * Add country key for default billing/shipping blocks on customer addresses tab
     *
     * @return array
     */
    public function getData(): array
    {
        $collection = $this->getCollection();
        $data['items'] = [];
        if ($this->request->getParam('batch_group_id')) {
            $collection->addFieldToFilter('batch_group_id', $this->request->getParam('batch_group_id'));
            $data = $collection->toArray();
        }

        return $data;
    }
}
