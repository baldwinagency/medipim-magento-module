<?php

namespace Baldwin\MedipimConnector\Ui\Component\Listing\Column\Job;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;
class Profile extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $profileId = $item['profile_id'];
                if (isset($item['profile'])) {
                    $html = "<a href='" . $this->context->getUrl('medipim_connector/importprofile/edit',['profile_id'=>$profileId]) . "'>";
                    $html .= $item['profile'];
                    $html .= "</a>";

                    $item['profile'] = $html;
                }
            }
        }

        return $dataSource;
    }
}