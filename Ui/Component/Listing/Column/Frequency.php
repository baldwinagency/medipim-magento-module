<?php

namespace Baldwin\MedipimConnector\Ui\Component\Listing\Column;

use Baldwin\MedipimConnector\Model\ImportProfile;

class Frequency implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => ImportProfile::FREQUENCY_MANUAL, 'label' => __('None (manual run only)')],
            ['value' => ImportProfile::FREQUENCY_HOUR, 'label' => __('Every hour')],
            ['value' => ImportProfile::FREQUENCY_DAY, 'label' => __('Every day')],
            ['value' => ImportProfile::FREQUENCY_WEEK, 'label' => __('Every monday')],
            ['value' => ImportProfile::FREQUENCY_MONTH, 'label' => __('Every 1st of the month')]
        ];
    }
}
