<?php

namespace Baldwin\MedipimConnector\Ui\Component\Listing\Column\Category;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class MagentoCategoryPath extends Column
{
    const ROOT_CAT = 1;
    const DEFAULT_CAT = 2;

    private $categoryCollectionFactory;

    private $categoryRepository;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        CollectionFactory $categoryCollectionFactory,
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryRepository        = $categoryRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        /* @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
        $collection = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect("path");

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $entityId = $item['entity_id'];
                $model = $collection->getItemById($entityId);

                $pathIds = $model->getData("path");

                if($pathIds !== null) {
                    $pathIds = explode("/", $pathIds);
                    $item['catalog_category'] = $this->calculatePath($pathIds);
                }

            }
        }
        return $dataSource;
    }
    private function calculatePath($pathIds) {
        $paths = [];
        if (($key = array_search(self::ROOT_CAT, $pathIds)) !== false) {
            unset($pathIds[$key]);
        }

        if (($key = array_search(self::DEFAULT_CAT, $pathIds)) !== false) {
            unset($pathIds[$key]);
        }
        foreach ($pathIds as $id) {
            $categoryModel = $this->categoryRepository->get($id);
            $paths[] = $categoryModel->getName();
        }
        return implode("/", $paths);
    }
}
