<?php

namespace Baldwin\MedipimConnector\Ui\Component\Listing\Column\Category;

use Baldwin\MedipimConnector\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class MedipimCategoryPath extends Column
{
    private $categoryCollectionFactory;

    private $categoryRepository;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        CollectionFactory $categoryCollectionFactory,
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryRepository        = $categoryRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        /* @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
        $collection = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect("medipim_ids");

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $entityId = $item['entity_id'];
                $model = $collection->getItemById($entityId);

                $medipimIds = $model->getData("medipim_ids");

                if($medipimIds !== null) {
                    $medipimIds = explode(",", $medipimIds);
                    $paths = [];
                    foreach ($medipimIds as $id ) {
                        $medipimCat = $this->categoryRepository->getByMedipimId($id);
                        $paths[] = $medipimCat->getPath();
                    }
                    $item['medipim_categories'] = implode(",", $paths);
                    if (isset($item['medipim_categories'])) {

                    }

                }

            }
        }

        return $dataSource;
    }
}
