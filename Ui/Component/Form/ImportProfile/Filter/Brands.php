<?php

namespace Baldwin\MedipimConnector\Ui\Component\Form\ImportProfile\Filter;

use Baldwin\MedipimConnector\Model\ResourceModel\Brand\CollectionFactory;
use Baldwin\MedipimConnector\Helper\Import\Attribute;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\Serializer\Json;

class Brands implements \Magento\Framework\Option\ArrayInterface
{

    const MEDIPIM_BRANDS = "medipim_brands";

    /**
     * @var Attribute
     */
    private $attributeHelper;

    /**
     * @var array
     */
    private $values;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var CacheInterface
     */
    private $cache;

    private $brandsCollectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory,
        Attribute $attributeHelper,
        Json $serializer,
        CacheInterface $cache
    ) {
        $this->attributeHelper = $attributeHelper;
        $this->brandsCollectionFactory = $collectionFactory;
        $this->serializer = $serializer;
        $this->cache = $cache;
    }



    public function toOptionArray() {
        return $this->getBrands();
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function getBrands($key = 'value', $label = 'label')
    {
        $this->values = $this->cache->load(self::MEDIPIM_BRANDS);
        if ($this->values === false) {
            $brands = $this->brandsCollectionFactory->create();

            $values = [];

            foreach ($brands as $brand) {
                $value = $brand->getId();
                $name = $brand->getName();
                $values[] = [$key => $value, $label => $name];
            }

            $this->values = $values;

            $data = $this->serializer->serialize($values);
            $tags = [];

            $this->cache->save($data, self::MEDIPIM_BRANDS, $tags, 60 * 15);
        } else {
            $this->values = $this->serializer->unserialize($this->values);
        }

        return $this->values;
    }


}
