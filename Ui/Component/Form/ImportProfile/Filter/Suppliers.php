<?php

namespace Baldwin\MedipimConnector\Ui\Component\Form\ImportProfile\Filter;

use Baldwin\MedipimConnector\Model\ConnectorInterface;
use Baldwin\MedipimConnector\Helper\Import\Attribute;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Baldwin\MedipimConnector\Model\ResourceModel\Supplier\CollectionFactory;

class Suppliers implements \Magento\Framework\Option\ArrayInterface
{
    const MEDIPIM_SUPPLIERS = 'medipim_suppliers';

    private $attributeHelper;

    private $values;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        ConnectorInterface $connector,
        Attribute $attributeHelper,
        Json $serializer,
        CacheInterface $cache,
        CollectionFactory $collectionFactory
    )
    {
        $this->connector = $connector;
        $this->attributeHelper = $attributeHelper;
        $this->serializer = $serializer;
        $this->cache = $cache;
        $this->collectionFactory = $collectionFactory;
    }


    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return $this->getSuppliers();
    }

    public function getSuppliers($key = 'value', $label = 'label')
    {
        $this->values = $this->cache->load(self::MEDIPIM_SUPPLIERS);

        if ($this->values === false) {
            $brands = $this->collectionFactory->create();

            $values = [];

            foreach ($brands as $brand) {
                $value = $brand->getId();
                $name = $brand->getName();
                $values[] = [$key => $value, $label => $name];
            }

            $this->values = $values;

            $data = $this->serializer->serialize($values);
            $tags = [];

            $this->cache->save($data, self::MEDIPIM_SUPPLIERS, $tags, 60 * 15);
        } else {
            $this->values = $this->serializer->unserialize($this->values);
        }

        return $this->values;
    }
}
