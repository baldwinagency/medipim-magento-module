<?php

namespace Baldwin\MedipimConnector\Ui\Component\Form\ImportProfile\Filter;

use Baldwin\MedipimConnector\Model\ResourceModel\CategoryFactory as CategoryResourceFactory;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\Collection;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\CollectionFactory;

class Categories implements \Magento\Framework\Option\ArrayInterface
{

    const TREE_ROOT_ID = 0;

    private $collectionFactory;

    private $values;

    private $categoryResourceFactory;

    private $addedNodes;

    private $categoriesTree;

    public function __construct(
        CollectionFactory $collectionFactory,
        CategoryResourceFactory $categoryResourceFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->values = [];
        $this->addedNodes = [];
        $this->categoryResourceFactory = $categoryResourceFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {

        if($this->categoriesTree === null) {
            /* @var Collection $categories */
            $categories = $this->collectionFactory
                ->create()
                ->addAttributeToSort("level", "asc");

            $categoryById = [
                self::TREE_ROOT_ID => [
                    'value' => self::TREE_ROOT_ID
                ],
            ];

            foreach ($categories as $category) {
                $parentId = $category->getMedipimParentId();

                if ($parentId === null) {
                    $parentId = 0;
                }

                foreach ([$category->getMedipimId(), $parentId] as $categoryId) {
                    if (!isset($categoryById[$categoryId])) {
                        $categoryById[$categoryId] = ['value' => $categoryId];
                    }
                }

                $categoryById[$category->getId()]['is_active'] = 1;
                $categoryById[$category->getId()]['label'] = $category->getName();
                $categoryById[$parentId]['optgroup'][] = &$categoryById[$category->getId()];
            }

            $this->categoriesTree = $categoryById[self::TREE_ROOT_ID]['optgroup'];
        }
        return $this->categoriesTree;
    }
}
