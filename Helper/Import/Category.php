<?php

namespace Baldwin\MedipimConnector\Helper\Import;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filter\FilterManager;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoresCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Baldwin\MedipimConnector\Model\ResourceModel\Category\CollectionFactory as MedipimCatCollectionFactory;

/**
 * Class Category
 * @package Baldwin\MedipimConnector\Helper\Import
 */
class Category extends AbstractHelper
{
    /**
     * @var FilterManager
     */
    private $filter;

    private $magentoCategoryCollectionFactory;

    private $medipimCategoryCollectionFactory;

    private $cachedCategories;

    private $cachedMedipimCategories;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        StoresCollectionFactory $storesCollectionFactory,
        FilterManager $filter,
        CollectionFactory $magentoCategoryCollectionFactory,
        MedipimCatCollectionFactory $medipimCategoryCollectionFactory
    )
    {
        $this->filter                           = $filter;
        $this->magentoCategoryCollectionFactory = $magentoCategoryCollectionFactory;
        $this->medipimCategoryCollectionFactory = $medipimCategoryCollectionFactory;
        $this->cachedCategories                 = [];
        $this->cachedMedipimCategories          = [];
        parent::__construct($context, $storeManager, $storesCollectionFactory);
    }

    /**
     * Helper function to get url key from the category name
     * @param $catName
     * @return string
     */
    public function getImportCategoryUrlKeyFromName($catName)
    {
        $formattedName = $this->filter->translitUrl($catName);
        return $formattedName;
    }

    /**
     * Helper function to get url path from the category path
     * @param $categoryEscaped
     * @return string
     */
    public function createUrlPathFromCategoryPath($categoryEscaped)
    {
        $categoryUrlPath = [];
        for ($i = 0; $i < count($categoryEscaped); ++$i) {
            $categoryUrlPath[] = $this->getImportCategoryUrlKeyFromName($categoryEscaped[$i]);
        }

        return implode('/', $categoryUrlPath);
    }

    public function createUrlPathFromMedipimCategoryIds($parentIds, $medipimLang = "en")
    {
        $categoryUrlPath = [];
        for ($i = 0; $i < count($parentIds); ++$i) {
            $name = $this->getImportCategoryNameFromId($parentIds[$i], $medipimLang);
            $categoryUrlPath[] = $this->getImportCategoryUrlKeyFromName($name);
        }

        return implode('/', $categoryUrlPath);
    }
    
    public function getImportCategoryNameFromId($id, $medipimLang)
    {
        if (!in_array($id, $this->cachedMedipimCategories, true)) {
            $category = $this->medipimCategoryCollectionFactory->create()
                ->addAttributeToSelect("name")
                ->addAttributeToFilter("medipim_id", $id)
                ->getFirstItem();

            $this->cachedCategories[$id] = $category;
        }
        $category = $this->cachedCategories[$id];

        return $category->getName($medipimLang);
    }

    public function createNamePathFromCategoryUrlPath($categoryPath)
    {
        $rootPath = 'Root Catalog';
        $categoryUrlPath = explode("/", $categoryPath);
        foreach ($categoryUrlPath as $path) {
            if (!in_array($path, $this->cachedCategories, true)) {
                $category = $this->magentoCategoryCollectionFactory->create()
                    ->addAttributeToSelect("name")
                    ->addAttributeToFilter("entity_id", $path)
                    ->getFirstItem();

                $this->cachedCategories[$path] = $category;
            }
            $category = $this->cachedCategories[$path];
            $name = $category->getName();
            $namePath[] = $name;
        }
        //the first is root category so we will remove this from the array.
        if (($key = array_search($rootPath, $namePath)) !== false) {
            unset($namePath[$key]);
        }

        return implode("/", $namePath);
    }

}
