<?php


namespace Baldwin\MedipimConnector\Helper\Import;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoresCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
/**
 * Class Attribute
 * @package Baldwin\MedipimConnector\Helper\Import
 */
class Attribute extends AbstractHelper
{
    private $attributeRepository;

    private $optionIds = [];
    private $attributes = [];

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        StoresCollectionFactory $storesCollectionFactory,
        AttributeRepositoryInterface $attributeRepository
    )
    {
        $this->attributeRepository = $attributeRepository;
        parent::__construct($context, $storeManager, $storesCollectionFactory);
    }

    public function camelCaseToString(string $string)
    {
        $string[0] = strtolower($string[0]);
        return preg_replace_callback('/([A-Z])/', function ($string) {
            $result = " " . ucfirst(strtolower($string[1]));
            return $result;
        }, $string);
    }

    public function stringToCamelCase($string, array $noStrip = [])
    {
        // non-alpha and non-numeric characters become spaces
        $string = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $string);
        $string = trim($string);
        // uppercase the first character of each word
        $string = ucwords($string);
        $string = str_replace(" ", "", $string);
        $string = lcfirst($string);

        return $string;
    }

    public function snakeToString(string $string)
    {
        return str_replace('_', ' ', ucwords($string, '_'));
    }

    public function camelToSnake(string $string)
    {
        if ( preg_match ( '/[A-Z]/', $string ) === 0 ) {
            return $string;
        }

        $pattern = '/([a-z])([A-Z])/';
        $r = strtolower ( preg_replace_callback ( $pattern, function ($a) {
                return $a[1] . "_" . strtolower ( $a[2] );
            }, $string )
        );

        return $r;
    }

    public function intPriceToFloat(int $price)
    {
        return $price / 100;
    }

    public function getStoreViewValueOfAttribute(string $language, ? array $value)
    {
        $result = null;
        if (is_array($value)) {
            if (array_key_exists($language, $value)) {
                $result = $value[$language];
            }
        }

        return $result;
    }

    public function getAttributeOptionValueId(AttributeInterface $attribute, $adminValue): ? string
    {
        $attrCode = $attribute->getAttributeCode();

        if (!isset($this->optionIds[$attrCode])) {
            $this->optionIds[$attrCode] = [];
        }

        if (!isset($this->optionIds[$attrCode][$adminValue])) {
            $optionId = $attribute->setStoreId(0)->getSource()->getOptionId($adminValue);

            $this->optionIds[$attrCode][$adminValue] = $optionId;
        } else {
            $optionId = $this->optionIds[$attrCode][$adminValue];
        }

        return $optionId;
    }
}
