<?php


namespace Baldwin\MedipimConnector\Helper\Import;

use Magento\Framework\App\Helper\AbstractHelper as MagentoAbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoresCollectionFactory;

/**
 * Class AbstractHelper
 * @package Baldwin\MedipimConnector\Helper\Import
 */
class AbstractHelper extends MagentoAbstractHelper
{
    const MEDIPIM_CODE = 'medipim_code';
    const DEFAULT_MEDIPIM_LANGUAGE = 'en';

    protected $storeManager;
    protected $storesCollectionFactory;
    private $stores;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        StoresCollectionFactory $storesCollectionFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->storesCollectionFactory = $storesCollectionFactory;
        $this->stores = [];
        parent::__construct($context);
    }

    public function getMedipimLanguageCodeByStoreId($storeId)
    {
        return $this->storeManager->getStore($storeId)->getData(self::MEDIPIM_CODE);
    }

    public function getStoreIdByMedipimLanguage($language)
    {
        if (in_array($language, $this->stores, true)) {
            return $this->stores[$language];
        } else {
            $storesCollection = $this->storesCollectionFactory->create()
                ->addFieldToSelect(self::MEDIPIM_CODE)
                ->addFieldToSelect("store_id");

            foreach ($storesCollection as $store) {
                if ($language === $store->getData(self::MEDIPIM_CODE)) {
                    if (isset($this->stores[$language])) {
                        $this->stores[$language][] = $store->getId();
                    } else {
                        $this->stores[$language] = [$store->getId()];
                    }
                }
            }

            return $this->stores[$language];
        }
    }

    public function getDefaultMedpimLanguage()
    {
        return self::DEFAULT_MEDIPIM_LANGUAGE;
    }
}
