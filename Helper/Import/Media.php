<?php

namespace Baldwin\MedipimConnector\Helper\Import;

use Baldwin\MedipimConnector\Helper\Import\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoresCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use \GuzzleHttp\Client;
use Baldwin\MedipimConnector\Helper\Logger;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\Filesystem\Io\File;
/**
 * Class Media
 * @package Baldwin\MedipimConnector\Helper
 */
class Media extends AbstractHelper
{
    const FOLDER_PERMISSIONS = 0755;
    /**
     * @var \GuzzleHttp\ClientFactory
     */
    private $guzzleClient;

    /**
     * @var \Baldwin\MedipimConnector\Helper\Logger
     */
    private $logger;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $file;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        StoresCollectionFactory $storesCollectionFactory,
        \GuzzleHttp\Client $client,
        Logger $logger,
        Filesystem $filesystem,
        File $file
    )
    {
        $this->guzzleClient = $client;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->file = $file;
        parent::__construct($context, $storeManager, $storesCollectionFactory);
    }

    public function downloadImage($remotePath, $localPath, $filename)
    {
        try {
            $path = $localPath . $filename;
            if (!$this->fileExists($path)) {
                /* @var Client $client */
                $this->createDirectory($localPath);
                $response = $this->guzzleClient->get(
                    $remotePath,
                    ['save_to' => $path]
                );
            }

            return $path;
        } catch (\Exception $e) {
            $this->logger->logMessage($e);
            // Log the error or something
            return false;
        }
    }

    public function fileExists($path): bool
    {
        return $this->file->fileExists($path);
    }

    public function createDirectory($path)
    {
        $this->file->checkAndCreateFolder($path, self::FOLDER_PERMISSIONS);
    }

    public function removeDirectory($path): bool
    {
        $this->file->rmdir($path);
    }

    public function getMediaDirectoryAbsolutePath()
    {
        $mediapath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        return $mediapath;
    }

    public function getMediaDirectoryRelativePath()
    {
        $mediapath = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getRelativePath();
        return $mediapath;
    }

    public function getImageFileNameOfUrl(string $url)
    {
        $urlArray = explode('/', $url);
        $imageName = end($urlArray);

        return $imageName;
    }

}
