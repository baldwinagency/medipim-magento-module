<?php

namespace Baldwin\MedipimConnector\Helper\Catalog;

use \Baldwin\MedipimConnector\Helper\Cdn as CdnHelper;
use Baldwin\MedipimConnector\Helper\Cdn;
use Magento\Catalog\Model\Product\ImageFactory;
use Magento\Catalog\Model\View\Asset\PlaceholderFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\ConfigInterface;
use Baldwin\MedipimConnector\Model\ConfigInterface as MedipimConfigInterface;
use Baldwin\MedipimConnector\Model\Config\Source\ImportImageType;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Image extends \Magento\Catalog\Helper\Image
{
    CONST CDN_MEDIA_TYPE = 'cdn_media_type';
    const DEFAULT_CDN_IMAGE_TYPE = 'packshots';
    CONST CDN_IMAGE_TYPE_FALLBACK_PRODUCTSHOT = 'productshots';
    const CDN_IMAGE_TYPE_FALLBACK_FRONTAL = 'frontals';

    private $cdnData = null;
    private $medipimConfig;
    private $cdnHelper;
    protected $_cndImageType;
    protected $_width;
    protected $_height;
    private $productCollectionFactory;

    public function __construct(
        Context $context,
        ImageFactory $productImageFactory,
        Repository $assetRepo,
        ConfigInterface $viewConfig,
        PlaceholderFactory $placeholderFactory = null,
        CdnHelper $cdnHelper,
        MedipimConfigInterface $medipimConfig,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->cdnHelper     = $cdnHelper;
        $this->medipimConfig = $medipimConfig;
        $this->productCollectionFactory = $productCollectionFactory;
        parent::__construct(
            $context,
            $productImageFactory,
            $assetRepo,
            $viewConfig,
            $placeholderFactory
        );
    }

    public function setCdnImageType($type)
    {
        $this->_cndImageType = $type;
        return $this;
    }

    public function setDestinationSubdir($subdir)
    {
        $this->_getModel()->setDestinationSubdir($subdir);
        return $this;

    }
    public function setWidth($width)
    {
        $this->_getModel()->setWidth($width);
        return $this;
    }

    public function setHeight($height)
    {
        $this->_getModel()->setHeight($height);
        return $this;
    }

    /**
     * Retrieve image width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->getAttribute('width') ?: $this->_getModel()->getWidth();
    }

    /**
     * Retrieve image height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->getAttribute('height') ?: $this->_getModel()->getHeight() ?: $this->getAttribute('width');
    }

    public function getCdnImageType()
    {
        return $this->_cndImageType ?: self::DEFAULT_CDN_IMAGE_TYPE;
    }


    /**
     * {@inheritdoc}
     */
    protected function _reset()
    {
        $this->cdnData = null;
        return parent::_reset();
    }

    /**
     * {@inheritdoc}
     */
    public function init($product, $imageId, $attributes = [])
    {
        if ($product !== null && $this->medipimConfig->getImportImageType() === ImportImageType::IMPORT_TYPE_CDN) {
            $cdnDataProduct = $this->productCollectionFactory->create()
                ->addAttributeToSelect(Cdn::CDN_IMAGE_DATA_ATTRIBUTE_CODE)
                ->addIdFilter($product->getData("entity_id"))
                ->getFirstItem();

            $product->setData(Cdn::CDN_IMAGE_DATA_ATTRIBUTE_CODE, $cdnDataProduct->getData(Cdn::CDN_IMAGE_DATA_ATTRIBUTE_CODE));
            parent::init($product, $imageId, $attributes);
            // Add CDN data (if available on product)
            if (!$this->cdnData) {
                $cdnData = $this->cdnHelper->getProductCdnImageLinks($this->getProduct());
                if ($cdnData) {
                    $this->cdnData = $cdnData;
                }
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl()
    {
        if ($this->medipimConfig->getImportImageType() === ImportImageType::IMPORT_TYPE_CDN) {
            // Check the CDN data
            if ($this->cdnData) {
                $checkCdnDataFor = null;

                // If we have width dimension
                $imageWidth = $this->getWidth();
                if ($imageWidth) {
                    $format = $this->cdnHelper->getImageFormat(
                        $this->getWidth(), $this->getHeight()
                    );

                    // Check if we got a link for the requested format
                    // (+ fallback if format should not exist)
                    $checkCdnDataFor = [
                        $format,
                        $this->cdnHelper->getCdnImageFormatFallback($this->getWidth())
                    ];
                } else {
                    // If we have no width we fallback to the base_image
                    $checkCdnDataFor = [CdnHelper::CDN_DATA_HUGE_IMAGE];
                }

                // Fetch the 'url' attribute from the CDN data
                $cdnMediaType = $this->getCdnImageType();
                if(!empty($this->cdnData[$cdnMediaType])) {
                    $cdnData = $this->cdnData[$cdnMediaType];
                    $cdnData = reset($cdnData); //we take the first picture.
                    if (array_key_exists('formats', $cdnData)) {
                        $imageIdCdnData = $cdnData['formats'];
                        $url = $this->cdnHelper->getImageUrlFromCdnData(
                            $imageIdCdnData,
                            $checkCdnDataFor
                        );

                        if ($url) {
                            return $url;
                        }
                    }
                } else {

                    //We set some fallbacks if no packshots are found
                    if ($this->getCdnImageType() !== self::CDN_IMAGE_TYPE_FALLBACK_PRODUCTSHOT
                        && $this->getCdnImageType() !== self::CDN_IMAGE_TYPE_FALLBACK_FRONTAL
                    ) {
                        $this->setCdnImageType(self::CDN_IMAGE_TYPE_FALLBACK_PRODUCTSHOT);
                        return $this->getUrl();
                    } else {
                        if ($this->getCdnImageType() !== self::CDN_IMAGE_TYPE_FALLBACK_FRONTAL) {
                            $this->setCdnImageType(self::CDN_IMAGE_TYPE_FALLBACK_FRONTAL);
                            return $this->getUrl();
                        }
                    }
                }
            }
        }
        // Fallback to default Magento functionality
        return parent::getUrl();
    }

    /**
     * {@inheritdoc}
     */
    protected function getAttribute($name)
    {
        if ($this->medipimConfig->getImportImageType() === ImportImageType::IMPORT_TYPE_CDN) {
            if ($name === self::CDN_MEDIA_TYPE) {
                return isset($this->attributes[self::CDN_MEDIA_TYPE]) ? $this->attributes[self::CDN_MEDIA_TYPE] : CdnHelper::CDN_DATA_DEFAULT;
            }
        }

        // Fallback to default Magento functionality
        return parent::getAttribute($name);
    }
}
