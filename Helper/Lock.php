<?php

namespace Baldwin\MedipimConnector\Helper;

use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Filesystem\directoryList;

class Lock
{
    const IMRORT_LOCK_DIR = '/sync/locks/';
    const IMPORT_LOCK_FILE = 'medipim_import.lock';

    private $lockFileHandle = null;

    private $file;
    private $directoryList;

    public function __construct(
        File $file,
        directoryList $directoryList
    )
    {
        $this->file          = $file;
        $this->directoryList = $directoryList;
    }

    public function createAndCheckLockFile()
    {
        $filePath = $this->directoryList->getRoot() . self::IMRORT_LOCK_DIR . self::IMPORT_LOCK_FILE;

        $this->file->checkAndCreateFolder($this->directoryList->getRoot() . self::IMRORT_LOCK_DIR);

        if(!$this->file->fileExists($filePath)) {
            fopen($filePath, 'w');
        }

        $this->lockFileHandle = fopen($filePath, 'r+');
        if ($this->lockFileHandle === false) {
            throw new \Exception("File {$filePath} cannot be opened!");
        }

        if (flock($this->lockFileHandle, LOCK_EX | LOCK_NB) !== true) {
            throw new \Exception("Lockfile {$filePath} is currently locked, aborting this process!");
        }
    }

    public function releaseLockFile()
    {
        flock($this->lockFileHandle, LOCK_UN);
        fclose($this->lockFileHandle);
    }
}
