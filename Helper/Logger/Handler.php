<?php

namespace Baldwin\MedipimConnector\Helper\Logger;

use Monolog\Logger;

/**
 * Class Handler
 * @package Baldwin\MedipimConnector\Helper\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $fileName = '/var/log/medipim_connector.log';
    protected $loggerType = Logger::INFO;
}
