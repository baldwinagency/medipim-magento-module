<?php

namespace Baldwin\MedipimConnector\Helper;

use Monolog\Logger as MonoLogger;

/**
 * Class Logger
 * @package Baldwin\MedipimConnector\Helper
 */
class Logger extends MonoLogger
{
    const TYPE_INFO = 'INFO';
    const TYPE_WARNING = 'WARN';
    const TYPE_ERROR = 'ERROR';

    protected $_logMessages = [];

    public function addMessage($msg, $type = self::TYPE_INFO)
    {
        $this->logMessage($msg, $type);

        $msg = sprintf(
            '%s - %5s - %-7s: %s',
            date('Y-m-d H:i:s'),
            $this->convert(memory_get_usage(true)),
            $type,
            $msg
        );

        echo "$msg\n";

        $this->_logMessages[] = [
            'type'     => $type,
            'message'  => $msg,
        ];
    }

    public function logMessage($msg, $type = self::TYPE_INFO)
    {
        switch ($type) {
            case self::TYPE_ERROR:
                $this->addError($msg);
                break;
            case self::TYPE_WARNING:
                $this->addNotice($msg);
                break;
            case self::TYPE_INFO:
            default:
                $this->addInfo($msg);
        }
    }

    public function getLogMessages($withTypes = [])
    {
        // Set function parameter defaults
        if (count($withTypes) === 0) {
            $withTypes = [
                self::TYPE_INFO,
                self::TYPE_WARNING,
                self::TYPE_ERROR
            ];
        }

        foreach ($this->_logMessages as $logMessage) {
            // if we find a logmessage with one of the withTypes, we return all messages
            if (in_array($logMessage['type'], $withTypes, true)) {
                return $this->getLogMessagesAsStringArray();
            }
        }
        return [];
    }

    protected function getLogMessagesAsStringArray()
    {
        $result = [];
        foreach ($this->_logMessages as $logMessage) {
            $result[] = $logMessage['message'];
        }
        return $result;
    }

    protected function convert($size)
    {
        $unit = ['B','KB','MB','GB','TB','PB'];
        return @round($size/pow(1024, ($i=floor(log($size, 1024))))).$unit[$i];
    }
}
