<?php

namespace Baldwin\MedipimConnector\Helper;

class Cdn extends \Magento\Framework\App\Helper\AbstractHelper
{
    CONST CDN_DATA_DEFAULT              = 'base_image';
    CONST CDN_DATA_MEDIA_GALLERY        = 'media_gallery';

    CONST CDN_DATA_MEDIUM_IMAGE         = 'medium'; //450X450
    CONST CDN_DATA_LARGE_IMAGE          = 'large'; //900X900
    CONST CDN_DATA_HUGE_IMAGE           = 'huge'; //150X1500

    CONST CDN_DATA_BASE_IMAGE           = 'base_image';
    CONST CDN_DATA_FRONTALS             = 'small_image';
    CONST CDN_DATA_ADDITIONAL_IMAGES    = 'thumbnail_image';
    const CDN_IMAGE_DATA_ATTRIBUTE_CODE = 'mp_cdn_image_links';

    public function getImageFormat($width, $height)
    {
        return $width.'x'.$height;
    }

    public function getCdnImageFormatFallback($width)
    {
        if ($width < 500) {
            return self::CDN_DATA_MEDIUM_IMAGE;
        }
        elseif ($width < 900) {
            return self::CDN_DATA_LARGE_IMAGE;
        }

        return self::CDN_DATA_HUGE_IMAGE;
    }

    public function getImageUrlFromCdnData($cdnData, $imageIds)
    {
        if ($imageIds && is_array($cdnData)) {
            // Make sure we are working with an array
            if (!is_array($imageIds)) {
                $imageIds = [$imageIds];
            }

            // Loop the $imageIds to see if the $imageId
            // contains the requested $attribute.
            // The first $imageId with containing the $attribute will be returned
            foreach($imageIds as $imageId) {
                // Make sure the $imageId exists in the $cdnData
                if (array_key_exists($imageId, $cdnData)) {
                    $imageIdData = $cdnData[$imageId];
                    return $imageIdData;
                }
            }
        }

        return null;
    }

    private function getCdnTypeData($cdnData, $type, $imageId)
    {
        if ($cdnData) {
            // Make sure $type exists in the $cdnData
            if (array_key_exists($type, $cdnData)) {
                // Make sure the $imageId exists for the $type
                if (array_key_exists($imageId, $cdnData[$type])) {
                    return $cdnData[$type][$imageId];
                }
            }
        }

        return null;
    }

    public function getProductCdnImageLinks($product)
    {
        $cdnData = $product->getData(self::CDN_IMAGE_DATA_ATTRIBUTE_CODE);
        if (!empty($cdnData)) {
            return json_decode($cdnData, true);
        }

        return null;
    }
}
