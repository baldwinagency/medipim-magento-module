<?php

namespace Baldwin\MedipimConnector\Block\Catalog\Product\View;

use \Baldwin\MedipimConnector\Helper\Cdn as CdnHelper;

class Gallery extends \Magento\Catalog\Block\Product\View\Gallery
{
    protected $dataObjectFactory;
    protected $cdnHelper;

    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        CdnHelper $cdnHelper,
        array $data = []
    ) {
        $this->dataObjectFactory = $dataObjectFactory;
        $this->cdnHelper = $cdnHelper;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $arrayUtils, $jsonEncoder, $data);
    }


    /**
     * {@inheritdoc}
     */
    public function getGalleryImages()
    {
        $cdnMediaGallery = $this->getCdnData();
        if (is_array($cdnMediaGallery)) {
            $images = $this->collectionFactory->create();

            // Loop the media_gallery values for the CDN data
            for($i=0;$i<count($cdnMediaGallery);++$i) {
                if ($cdnMediaGallery[$i] !== null && $cdnMediaGallery[$i] !== false) {
                    $image = new \Magento\Framework\DataObject();

                    // Urls needed for the gallery (mapped with their corresponding imageId)
                    $imageUrls = [
                        'small_image_url' => 'product_page_image_small',
                        'medium_image_url' => 'product_page_image_medium_no_frame',
                        'large_image_url' => 'product_page_image_large_no_frame',
                        'small_image_url' => 'mini_cart_product_thumbnail'
                    ];

                    foreach ($imageUrls as $urlType => $originalImageId) {
                        // Fetch the media attributes (so we know the width and height)
                        $mediaAttributes = $this->getConfigView()->getMediaAttributes(
                            'Magento_Catalog',
                            \Magento\Catalog\Helper\Image::MEDIA_TYPE_CONFIG_NODE,
                            $originalImageId
                        );

                        $checkCdnDataFor = null;

                        // If we have width dimension for the $orinalImageId
                        $imageWidth = (isset($mediaAttributes['width']) ? $mediaAttributes['width'] : null);
                        if ($imageWidth) {
                            // Get the image format (based on the $mediaAttributes dimensions)
                            $imageFormat = $this->cdnHelper->getImageFormat(
                                $mediaAttributes['width'],
                                $mediaAttributes['height']
                            );

                            // Check if we got a link for the requested format
                            // (+ fallback if format should not exist)
                            $checkCdnDataFor = [
                                $imageFormat,
                                $this->cdnHelper->getCdnImageFormatFallback($mediaAttributes['width'])
                            ];
                        } else {
                            // If we have no width we fallback to the base_image
                            $checkCdnDataFor = [CdnHelper::CDN_DATA_HUGE_IMAGE];
                        }

                        // Fetch the 'url' attribute from the CDN data
                        $url = $this->cdnHelper->getImageUrlFromCdnData(
                            $cdnMediaGallery[$i]['formats'],
                            $checkCdnDataFor
                        );

                        if ($url) {
                            $image->setData($urlType, $url);
                            continue;
                        }
                    }

                    $image->setIsMain($i === 0 ? true : false);
                    $image->setPosition($i);
                    $image->setMediaType('image');
                    $images->addItem($image);
                }
            }

            if (!empty($images)) {
                return $images;
            }
        }

        return parent::getGalleryImages();
    }

    /**
     * {@inheritdoc}
     */
    public function isMainImage($image)
    {
        if ($image->getIsMain()) {
            return true;
        }

        return parent::isMainImage($image);
    }

    /**
     * {@inheritdoc}
     */
    public function getVar($name, $module = null)
    {
        return parent::getVar($name, 'Magento_Catalog');
    }

    /**
     * Retrieve config view
     *
     * @return \Magento\Framework\Config\View
     */
    private function getConfigView()
    {
        if (!$this->configView) {
            $this->configView = $this->_viewConfig->getViewConfig();
        }
        return $this->configView;
    }

    private function getCdnData()
    {
        if (!$this->hasData('cdn_media_gallery_data')) {
            $cdnData = $this->getProduct()->getData(CdnHelper::CDN_IMAGE_DATA_ATTRIBUTE_CODE);
            if (!empty($cdnData)) {
                $cdnData = json_decode($cdnData, true);

                $cdnData = call_user_func_array('array_merge', $cdnData);

                $this->setData('cdn_media_gallery_data', $cdnData);
            }
        }

        return $this->getData('cdn_media_gallery_data');
    }
}
