<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field;

/**
 * Class MagentoAttributes
 * @package Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field
 */
class MagentoAttributes extends \Magento\Framework\View\Element\Html\Select
{
    private $magentoAttributesSource;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Baldwin\MedipimConnector\Model\Config\Source\MagentoAttributes $magentoAttributesSource,
        array $data = []
    ) {
        $this->magentoAttributesSource = $magentoAttributesSource;
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->magentoAttributesSource->toOptionArray());
        }
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
