<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field;

class YesNo extends \Magento\Framework\View\Element\Html\Select
{
    private $_yesnoSource;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Config\Model\Config\Source\Yesno $yesnoSource,
        array $data = []
    ) {
        $this->_yesnoSource = $yesnoSource;
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->_yesnoSource->toOptionArray());
        }
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}