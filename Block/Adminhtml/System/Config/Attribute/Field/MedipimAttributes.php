<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field;

/**
 * Class MedipimAttributes
 * @package Baldwin\MedipimConnector\Block\Adminhtml\Form\Field
 */
class MedipimAttributes extends \Magento\Framework\View\Element\Text
{
    private $medipimAttributesSource;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Baldwin\MedipimConnector\Model\Config\Source\MedipimAttributes $medipimAttributesSource,
        array $data = []
    ) {
        $this->medipimAttributesSource = $medipimAttributesSource;
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->medipimAttributesSource->toOptionArray());
        }
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
