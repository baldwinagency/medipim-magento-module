<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config;

class CategoriesManager implements \Magento\Framework\Option\ArrayInterface
{
    private $_options;
    private $_groupCollectionFactory;
    public function __construct(\Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory)
    {
        $this->_groupCollectionFactory = $groupCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = $this->_groupCollectionFactory->create()->loadData()->toOptionArray();
        }
        return $this->_options;
    }
}

