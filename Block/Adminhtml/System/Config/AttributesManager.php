<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config;
use Baldwin\MedipimConnector\Model\Connector\AttributeInterface;
use \Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field\MedipimAttributes;
use \Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field\MagentoAttributes;

class AttributesManager extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    protected $_yesnoRenderer;

    /**
     * @var MagentoAttributes
     */
    protected $_magentoAttributesRenderer;

    /**
     * @var MedipimAttributes
     */
    protected $_medipimAttributesRenderer;

    protected $_hardcodedAttributes = AttributeInterface::DEFAULT_MAGENTO_ATTRIBUTE_MAPPING;

    protected $_addAfter = false;

    protected $_template = "Baldwin_MedipimConnector::system/config/form/field/array.phtml";


    protected function _prepareToRender()
    {
        $this->addColumn('medipim_attribute',
            [
                'label' => __('Medipim Attribute'),
                'class' => 'medipim_attribute',

            ]
        );
        $this->addColumn('magento_attribute',
            [
                'label' => __('Magento Attribute'),
                'renderer'  => $this->getMagentoAttributesRenderer('magento_attribute')
            ]
        );
    }

    private function getYesNoRenderer($className = null)
    {
        if (!$this->_yesnoRenderer) {
            $this->_yesnoRenderer = $this->getLayout()->createBlock(
                'Baldwin\MedipimConnector\Block\Adminhtml\System\Config\Attribute\Field\YesNo',
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true,
                    ]
                ]
            );

            if ($className !== null) {
                $this->_yesnoRenderer->setClass($className);
            }
        }

        return $this->_yesnoRenderer;
    }

    /**
     * @param null $className
     * @return MagentoAttributes
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getMagentoAttributesRenderer($className = null)
    {
        if (!$this->_magentoAttributesRenderer) {
            $this->_magentoAttributesRenderer = $this->getLayout()->createBlock(
                MagentoAttributes::class,
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true,
                    ]
                ]
            );

            if ($className !== null) {
                $this->_magentoAttributesRenderer->setClass($className);
            }
        }

        return $this->_magentoAttributesRenderer;
    }

    /**
     * @param null $className
     * @return MedipimAttributes
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getMedipimAttributesRenderer($className = null)
    {
        if (!$this->_medipimAttributesRenderer) {
            $this->_medipimAttributesRenderer = $this->getLayout()->createBlock(
                MedipimAttributes::class,
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true,
                    ]
                ]
            );

            if ($className !== null) {
                $this->_medipimAttributesRenderer->setClass($className);
            }
        }

        return $this->_medipimAttributesRenderer;
    }



    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {

        $optionExtraAttr = [];

        $optionExtraAttr['option_' . $this->getMagentoAttributesRenderer()->calcOptionHash($row->getData('magento_attribute'))]
            = 'selected="selected"';


        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = $this->_renderValue($element);
        $html .= $this->_renderHint($element);

        return $this->_decorateRowHtml($element, $html);
    }

    protected function _renderValue(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = parent::_renderValue($element);


        $html .= "<script>
            require([
                'jquery'
            ], function($) {
                var hardcodedAttributes = ".json_encode($this->_hardcodedAttributes).";

                $(window).load(function(){
                    $('#row_medipim_attributes_attributes').find('.value tbody .medipim_attribute').each(function(){
                        var v = $(this).val();
                        var medipim_attr = $(this).closest('tr').find('.medipim_attribute');
                        var magento_attr = $(this).closest('tr').find('.magento_attribute');

                        medipim_attr.prop('readonly', 'readonly');
                        magento_attr.prop('disabled', 'readonly');

                        if (Object.keys(hardcodedAttributes).indexOf(v) !== -1) {
                            magento_attr.attr('data-hardcoded', 1);
                        }
                    });
                });

                $(document).on('change', '#row_medipim_attributes_attributes .value tbody .medipim_attribute', function(){
                    var v = $(this).val();
                    var medipim_attr = $(this).closest('tr').find('.medipim_attribute');
                    var magento_attr = $(this).closest('tr').find('.magento_attribute');
                    medipim_attr.prop('readonly', 'readonly');

                    if (Object.keys(hardcodedAttributes).indexOf(v) !== -1) {
                        magento_attr.attr('data-hardcoded', 1);
                    }
                    else {
                        magento_attr.removeAttr('disabled');
                        magento_attr.attr('data-hardcoded', 0);
                    }
                });
            });
        </script>";
        $html .= '</td>';

        return $html;
    }
}
