<?php


namespace Baldwin\MedipimConnector\Block\Adminhtml\System\Config;

use \Magento\Config\Block\System\Config\Form\Field;
use \Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class ImportCategories
 * @package Baldwin\MedipimImport\Block\Adminhtml\System\Config
 */
class ImportCategories extends Field
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if (!$this->getTemplate()) {
            $this->setTemplate('system/config/category/fetch.phtml');
        }
        return $this;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $originalData = $element->getOriginalData();
        $buttonLabel = !empty($originalData['button_label'])
            ? $originalData['button_label'] : 'Import products';

        $this->addData(
            [
                'button_label' => __($buttonLabel),
                'html_id' => $element->getHtmlId(),
                'ajax_url' => $this->_urlBuilder->getUrl('medipim_connector/system_config_import/categories'),
            ]
        );

        return $this->_toHtml();
    }
}
