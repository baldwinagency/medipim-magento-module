<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\ImportProfile\Edit;

use Magento\Backend\Block\Widget\Context;
use Baldwin\MedipimConnector\Api\ImportProfileRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ImportProfileRepositoryInterface
     */
    protected $importProfileRepository;

    /**
     * @param Context $context
     * @param ImportProfileRepositoryInterface $importProfileRepository
     */
    public function __construct(
        Context $context,
        ImportProfileRepositoryInterface $importProfileRepository
    ) {
        $this->context = $context;
        $this->importProfileRepository = $importProfileRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getProfileId()
    {
        try {
            return $this->importProfileRepository->getById(
                $this->context->getRequest()->getParam('profile_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
