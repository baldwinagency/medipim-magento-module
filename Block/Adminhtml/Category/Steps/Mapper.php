<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\Category\Steps;

use Magento\Ui\Block\Component\StepsWizard\StepAbstract;

class Mapper extends StepAbstract
{
    /**
     * {@inheritdoc}
     */
    public function getCaption()
    {
        // Step 2
        return __('Map Categories');
    }
}
