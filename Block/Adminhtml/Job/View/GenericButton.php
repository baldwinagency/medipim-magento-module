<?php

namespace Baldwin\MedipimConnector\Block\Adminhtml\Job\View;

use Baldwin\MedipimConnector\Api\JobRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var JobRepositoryInterface
     */
    protected $jobRepository;

    /**
     * @param Context $context
     * @param ImportProfileRepositoryInterface $importProfileRepository
     */
    public function __construct(
        Context $context,
        JobRepositoryInterface $jobRepository
    ) {
        $this->context = $context;
        $this->jobRepository = $jobRepository;
    }

    /**
     * Return Job ID
     *
     * @return int|null
     */
    public function getProfileId()
    {
        try {
            return $this->jobRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
