# Medipim Connector

Medipim Connector module for Magento2

## Getting Started

These instructions will guide you in enabling the Medipim Connector for your Magento2 store.

### Prerequisites

To access the Medipim API you will need an API key and secret. Get them [here](https://platform.medipim.be/nl/inloggen)


### Installing

Using composer:

```
$ composer require baldwin/medipim-magento-module
```

Enabling the module:
 `bin/magento module:enable Baldwin_MedipimConnector` and `bin/magento setup:upgrade`


### Configuration
Configure your api client and secret under `Stores - Configuration - Services`

## Unit tests
`php vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist vendor/baldwin/medipim-magento-module/test/Unit -v`

## Authors

* **Tristan Hofman** - *Initial work* - [baldwin agency](https://baldwin.agency)

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

php vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist app/code/Baldwin/MedipimConnector/Test/Unit


#Cronjobs

To import the attributes run:
`tools/n-98-magerun.phar sys:cron:run baldwin_medipimconnector_importattributes`

to import the categories run:
`PHP_IDE_CONFIG="serverName=baldwin" XDEBUG_CONFIG="remote_host=127.0.0.1 idekey=phpstorm" tools/n-98-magerun.phar sys:cron:run baldwin_medipimconnector_importcategories`
