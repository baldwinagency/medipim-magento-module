<?php

namespace Baldwin\MedipimConnector\Plugin\Cron;

class Schedule
{
    private $cronConfig;
    private $lockHelper;

    public function __construct(
        \Magento\Cron\Model\ConfigInterface $cronConfig,
        \Baldwin\MedipimConnector\Helper\Lock $lockHelper
    ){
        $this->cronConfig = $cronConfig;
        $this->lockHelper = $lockHelper;
    }

    public function aroundTrySchedule(
        $subject,
        $proceed
    ) {
        $jobCode = $subject->getJobCode();

        $result = $proceed();
        return $result;
    }

    public function aroundTryLockJob(
        $subject,
        $proceed
    ) {
        $jobConfig = $this->getJobConfig($subject->getJobCode());
        if (isset($jobConfig['group']) AND $jobConfig['group'] === 'index') {
            try {
                // Check if the import lock is currently locked (to see if the import is running)
                $this->lockHelper->createAndCheckLockFile();
                $this->lockHelper->releaseLockFile();
            }
            catch (\Exception $e) {
                $subject->setStatus(\Magento\Cron\Model\Schedule::STATUS_MISSED);
                throw new \Exception("Cronjob {$jobConfig['name']} should not run while import is running");
            }
        }

        $result = $proceed();
        return $result;
    }

    private function getJobConfig($jobCode)
    {
        foreach ($this->cronConfig->getJobs() as $jobGroupCode => $jobGroup) {
            foreach ($jobGroup as $job) {
                if ($job['name'] == $jobCode) {
                    $job['group'] = $jobGroupCode;
                    return $job;
                }
            }
        }

        return [];
    }
}
