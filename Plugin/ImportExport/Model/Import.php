<?php

namespace Baldwin\MedipimConnector\Plugin\ImportExport\Model;

class Import
{
    protected $logHelper;

    public function __construct(
        \Baldwin\MedipimConnector\Helper\Logger $logHelper
    ) {
        $this->logHelper = $logHelper;
    }

    public function aroundAddLogComment($subject, $proceed, $debugData)
    {
        $msg = null;
        if (!is_array($debugData)) {
            $debugData = [$debugData];
        }

        foreach($debugData as $phrase) {
            if ($phrase instanceof \Magento\Framework\Phrase) {
                $this->logHelper->addMessage('-- '.$phrase->__toString());
            }
        }

        $result = $proceed($debugData);
        return $result;
    }
}
