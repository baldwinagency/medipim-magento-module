<?php

namespace Baldwin\MedipimConnector\Plugin;

use Baldwin\MedipimConnector\Model\Config\Source\ImportImageType;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class CdnImageUrl
{
    protected $imageHelperFactory;
    protected $productFactory;
    protected $medipimConfig;
    protected $collectionFactory;

    public function __construct(
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Baldwin\MedipimConnector\Model\ConfigInterface $medipimConfig
    )
    {
        $this->productFactory     = $productFactory;
        $this->imageHelperFactory = $imageHelperFactory;
        $this->medipimConfig      = $medipimConfig;
        $this->collectionFactory  = $collectionFactory;
    }

    public function beforeToHtml(\Magento\Catalog\Block\Product\Image $subject)
    {
        if ($this->medipimConfig->getImportImageType() === ImportImageType::IMPORT_TYPE_CDN) {
            $imageType = $this->medipimConfig->getProductListImageType();
            $productId = $subject->getData("product_id");
            $product = $this->productFactory->create()->load($productId);

            $width = $subject->getWidth();
            $height = $subject->getHeight();

            $imageUrl = $this->imageHelperFactory->create()
                ->init($product, "product_list_image")
                ->setCdnImageType($imageType)
                ->setWidth($width)
                ->setHeight($height)
                ->setDestinationSubdir("small_image")
                ->getUrl();

            if ($imageUrl) {
                $subject->setData("image_url", $imageUrl);
            }
        }
    }

    public function aroundResolve(
        \Magento\CatalogGraphQl\Model\Resolver\Product\ProductImage\Url $subject,
        $proceed,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if ($this->medipimConfig->getImportImageType() === ImportImageType::IMPORT_TYPE_CDN) {
            $cdnImageType = $this->medipimConfig->getProductListImageType();

            if (!isset($value['image_type'])) {
                throw new LocalizedException(__('"image_type" value should be specified'));
            }

            if (!isset($value['model'])) {
                throw new LocalizedException(__('"model" value should be specified'));
            }

            /** @var Product $product */
            $product = $value['model'];

            $product = $this->collectionFactory->create()
                ->addAttributeToSelect(\Baldwin\MedipimConnector\Helper\Cdn::CDN_IMAGE_DATA_ATTRIBUTE_CODE)
                ->addIdFilter($product->getId())
                ->getFirstItem();

            $imageType = $value['image_type'];

            $imageUrl = $this->imageHelperFactory->create()
                ->init($product, $imageType)
                ->setWidth(500)
                ->setHeight(500)
                ->setCdnImageType($cdnImageType)
                ->setDestinationSubdir("small_image")
                ->getUrl();

            return $imageUrl;
        }

        return $proceed($field,$context, $info, $value, $args);
    }
}
