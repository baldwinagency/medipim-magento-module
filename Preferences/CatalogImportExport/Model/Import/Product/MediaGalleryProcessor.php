<?php

namespace Baldwin\MedipimConnector\Preferences\CatalogImportExport\Model\Import\Product;

use Magento\CatalogImportExport\Model\Import\Product\MediaGalleryProcessor as MagentoMediaGalleryProcessor;
use Magento\CatalogImportExport\Model\Import\Product\SkuProcessor;
use Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;


class MediaGalleryProcessor extends MagentoMediaGalleryProcessor
{

    /**
     * DB connection.
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * @var string
     */
    private $mediaGalleryTableName;

    public function __construct(
        SkuProcessor $skuProcessor,
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection,
        ResourceModelFactory $resourceModelFactory,
        ProcessingErrorAggregatorInterface $errorAggregator
    )
    {
        $this->connection = $resourceConnection->getConnection();;
        parent::__construct($skuProcessor, $metadataPool, $resourceConnection, $resourceModelFactory, $errorAggregator);
    }

    /**
     * Remove old media gallery items.
     *
     * @param array $oldMediaValues
     * @return void
     */
    public function removeOldMediaItems(array $oldMediaValues) {
        $this->connection->delete(
            $this->mediaGalleryTableName,
            $this->connection->quoteInto('value IN (?)', $oldMediaValues)
        );
    }
}
